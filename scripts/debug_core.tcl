## source after synth_design step
# Probe Configuring function
proc ConfigProbe {ilaName netName} {

   # increment the probe index
   create_debug_port ${ilaName} probe
   # determine the probe index
   set probeIndex ${ilaName}/probe[expr [llength [get_debug_ports ${ilaName}/probe*]] - 1]

   # get the list of netnames
   set probeNet [lsort -increasing -dictionary [get_nets ${netName}]]
   puts "ProbeNet $probeNet"

   # calculate the probe width
   set probeWidth [llength ${probeNet}]
   puts "probeWidth $probeWidth"

   # set the width of the probe
   set_property port_width ${probeWidth} [get_debug_ports ${probeIndex}]

   # connect the probe to the ila module
   connect_debug_port ${probeIndex} ${probeNet}

}
proc ConfigSingleProbe {ilaName netName} {

   # increment the probe index
   create_debug_port ${ilaName} probe
    #determine the probe index
   set probeIndex ${ilaName}/probe[expr [llength [get_debug_ports ${ilaName}/probe*]] - 1]
   puts "ProbeIndex $probeIndex"
   #puts $probeInde
   ## set the width of the probe
   set_property port_width 1 [get_debug_ports ${probeIndex}]

   # connect the probe to the ila module
   connect_debug_port ${probeIndex} [get_nets ${netName}]
#connect_debug_port u_ila_0/probe2 [get_nets [list s_axis_tx_tvalid]]
}

#source ./vivado_proc_v1.tcl
# https://gitlab.cern.ch/rce/pixelrce/blob/78b980d3085d636e368e9b3a49b86f62d46685a5/rce/fw-hsio2/firmware/modules/StdLib/build/vivado_proc_v1.tcl
#set_property CONTROL.TRIGGER_POSITION 1024 [get_hw_ilas -of_objects [get_hw_devices xc7k325t_0] -filter {CELL_NAME=~"u_ila_0"}]
#Create the debug core
create_debug_core u_ila_0 ila
#set debug core properties
set_property C_DATA_DEPTH 8192   [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false   [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false  [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER true [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 0   [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL true    [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU true  [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0]
#connect the probe ports in the debug core to the signals being probed in the design
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list user_clk ]]

set_property port_width 1 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list cfg_interrupt]]

create_debug_port u_ila_0 probe
set_property port_width 1 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list cfg_interrupt_rdy]]

set pdma_i "app/PIO/PIO_EP_inst/pci_dma_engine_inst"

#set state_dma "${pdma_i}/state_rd_wr"
#create_debug_port u_ila_0 probe
#set_property port_width 3 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list ${state_dma}[0] ${state_dma}[1] ${state_dma}[2]]]
ConfigProbe u_ila_0 ${pdma_i}/state_rd_wr[*]

ConfigProbe u_ila_0 s_axis_tx_tvalid
ConfigProbe u_ila_0 s_axis_tx_tready
#ConfigProbe u_ila_0 ${pdma_i}/dma_payload_not_zero

#ConfigProbe u_ila_0 ${pdma_i}/f_data_out[*]
#ConfigProbe u_ila_0 ${pdma_i}/f_data_in[*]

#ConfigProbe u_ila_0 ${pdma_i}/tlps2go*
#set tlps2go "${pdma_i}/tlps2go"
#create_debug_port u_ila_0 probe
#set_property port_width 6 [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list ${tlps2go}[0] ${tlps2go}[1] ${tlps2go}[2] ${tlps2go}[3] ${tlps2go}[4] ${tlps2go}[5] ]]

ConfigProbe u_ila_0 ${pdma_i}/num_full_tlps[*]
#set num_full_tlps "${pdma_i}/num_full_tlps"
#create_debug_port u_ila_0 probe
#set_property port_width 6 [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list ${num_full_tlps}[0] ${num_full_tlps}[1] ${num_full_tlps}[2] ${num_full_tlps}[3] ${num_full_tlps}[4] ${num_full_tlps}[5] ]]

ConfigProbe u_ila_0 ${pdma_i}/state_irq[*]
#set cfg_interrupt_r "${pdma_i}/cfg_interrupt_r"
#ConfigSingleProbe u_ila_0 ${cfg_interrupt_r}
ConfigProbe u_ila_0 tx_buf_av[*]

ConfigProbe u_ila_0 ${pdma_i}/tlps2go[*]
#ConfigProbe u_ila_0 ${pdma_i}/qw2go_r[*]
#ConfigProbe u_ila_0 app/PIO/PIO_EP_inst/dma_data[*]
ConfigProbe u_ila_0 ${pdma_i}/tlp_req_r
ConfigProbe u_ila_0 ${pdma_i}/fifo_irq_rd_i
ConfigProbe u_ila_0 ${pdma_i}/prog_empty_ch0
ConfigProbe u_ila_0 ${pdma_i}/fifo_empty_i
ConfigProbe u_ila_0 ${pdma_i}/fifo_full_i
ConfigProbe u_ila_0 ${pdma_i}/prog_full_ch0
ConfigProbe u_ila_0 ${pdma_i}/dmach0_en_i
ConfigProbe u_ila_0 ${pdma_i}/streame_i
ConfigProbe u_ila_0 ${pdma_i}/data_valid_ch0

set datap_i "app/PIO/PIO_EP_inst/data_producer_inst"
ConfigProbe u_ila_0 ${datap_i}/cnt_data_i[*]

set dmatx_i "app/PIO/PIO_EP_inst/EP_DMA_TX_inst"
ConfigProbe u_ila_0 ${dmatx_i}/rd_len[*]
ConfigProbe u_ila_0 ${dmatx_i}/dma_tlp_compl_done_r

#Report Instance Areas:
#+------+-----------------------------------------------------------------------------------------------+-----------------------------+------+
#|      |Instance                                                                                       |Module                       |Cells |
#+------+-----------------------------------------------------------------------------------------------+-----------------------------+------+
#|1     |top                                                                                            |                             |  2598|
#|2     |  pcie_7x_0_support_i                                                                          |pcie_7x_0_support            |   495|
#|3     |    pipe_clock_i                                                                               |pcie_7x_0_pipe_clock         |    26|
#|4     |  app                                                                                          |pcie_app_7x                  |  2063|
#|5     |    PIO                                                                                        |PIO                          |  2063|
#|6     |      PIO_EP_inst                                                                              |PIO_EP                       |  2058|
#|7     |        EP_DMA_TX_inst                                                                         |PIO_DMA_TX_ENGINE            |   322|
#|8     |        pci_dma_engine_inst                                                                    |pci_dma_engine               |   600|
#|9     |          dma_fifo_0                                                                           |dma_fifo                     |   297|
#|2     |  app                       |pcie_app_7x          |   728|
#|3     |    PIO                     |PIO                  |   728|
#|4     |      PIO_EP_inst           |PIO_EP               |   724|
#|5     |        EP_DMA_TX_inst      |PIO_DMA_TX_ENGINE    |   175|
#|6     |        EP_REGS_inst        |PIO_EP_SHAPI_REGS    |   212|
#|7     |        EP_RX_inst          |PIO_RX_ENGINE        |   330|
#|8     |        pci_dma_engine_inst |pci_dma_engine       |     7|
# ...
#
#|41    |        data_producer_inst                                                                     |data_producer                |    92|
#|42    |        EP_REGS_inst                                                                           |PIO_EP_SHAPI_REGS            |   756|
#|43    |        EP_RX_inst                                                                             |PIO_RX_ENGINE                |   230|
#|44    |      PIO_TO_inst                                                                              |PIO_TO_CTRL                  |     3|
#|45    |  system_clocks_inst                                                                           |system_clocks                |    17|
#+------+-----------------------------------------------------------------------------------------------+-----------------------------+------+

#set_property port_width 1 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list m_axis_h2c_tready_0]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list m_axis_h2c_tvalid_0]]
#create_debug_port u_ila_0 probe
#set_property port_width 1 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list m_axis_h2c_tlast_0]]

#create_debug_port u_ila_0 probe

#set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0]
#Optionally, create more probe ports, set their width,
# and connect them to the nets you want to debug
#Implement design
