# vim: set ts=4 sw=4 tw=0 et :
###############################################################################
#
# project_implement.tcl: Tcl script for creating the VIVADO project
# Usage:
# Open Vivado IDE 202x.x
# Menu Tools->Run Tcl Script-> (this file)
################################################################################

set top_module atca_mimo_v2_adc
# Set the reference directory to where the script is
set origin_dir [file dirname [info script]]

cd $origin_dir
source $origin_dir/part.tcl

################################################################################
# define paths
################################################################################

set path_rtl ../src/hdl
set path_ip  ../src/ip
set path_sdc ../src/constraints

# Generate Ip Cores for this project.
# (make sure the 'part' is the same as  in .tcl files
#
source  $path_ip/Int64toDouble_lat1.tcl
source  $path_ip/Int64toFloat_lat1.tcl
source  $path_ip/Float2Double_lat0.tcl
source  $path_ip/DoubleMult_lat1.tcl
source  $path_ip/DoubleAccum_lat2.tcl
source  $path_ip/Double2Single_lat0.tcl
source  $path_ip/FpSingleSub_lat1.tcl
source  $path_ip/xdma_id0032.tcl

################################################################################
# setup the project
################################################################################

# set part xc7k325tfbg676-2

# Create project
create_project vivado_project "../vivado_project" -force -part $part

#
################################################################################
# read files:
# 1. RTL design sources
# 2. IP database files
# 3. constraints
################################################################################

add_files                             $path_rtl
#add_files                             $path_rtl/opencores_i2c_slave

#set_property top top [current_fileset]
set_property top_file {$path_rtl/$top_module.sv} [current_fileset]

read_ip    $path_ip/Int64toDouble_lat1/Int64toDouble_lat1.xci
read_ip    $path_ip/Int64toFloat_lat1/Int64toFloat_lat1.xci
read_ip    $path_ip/Float2Double_lat0/Float2Double_lat0.xci
read_ip    $path_ip/DoubleMult_lat1/DoubleMult_lat1.xci
read_ip    $path_ip/DoubleAccum_lat2/DoubleAccum_lat2.xci
read_ip    $path_ip/Double2Single_lat0/Double2Single_lat0.xci
read_ip    $path_ip/FpSingleSub_lat1/FpSingleSub_lat1.xci
read_ip    $path_ip/xdma_id0032/xdma_id0032.xci

#generate_target  all [get_ips] -force
#https://www.xilinx.com/support/answers/58526.html
#set_property generate_synth_checkpoint false [get_files $path_ip/xdma_0/xdma_0.xci]
#generate_target  {synthesis implementation instantiation_template} [get_ips]
#generate_target  {synthesis instantiation_template} [get_ips]
#generate_target  synthesis [get_ips]

read_xdc   $path_sdc/atca_mimo_v2_adc_x4g2.xdc
read_xdc   $path_sdc/atca_adc_channels.xdc
read_xdc   $path_sdc/atca_adc_channels_clk_route.xdc

# Optional: to implement put on Tcl Console
# update_compile_order -fileset sources_1
# launch_runs impl_1 -to_step write_bitstream -jobs 4
#
close_project
puts "INFO: Project created: vivado_project"
#exit
