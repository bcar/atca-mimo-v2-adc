###############################################################################
#
# create_mcs.tcl: Tcl script for creating the mcs programming file
#
# Usage:
# vivado -mode batch -source create_mcs.tcl
# Trenz Module has a SPI Memory type s25fl256sxxxxxx0-spi-x1_x2_x4
################################################################################
#
set bit_file atca_mimo_v2_adc
# set bit_file atcav2xdma
set path_impl ../vivado_project/vivado_project.runs/impl_1
# set path_impl ../binaries

set prog_file atca_mimo_v2_adc

# Set the reference directory to where the script is
set origin_dir [file dirname [info script]]

cd $origin_dir
set path_bin ../binaries

#SPIx4
write_cfgmem -force -format MCS -size 256 -interface SPIx4 -loadbit "up 0x0 \
    $path_impl/${bit_file}.bit" "$path_bin/${prog_file}.mcs"
exit
