# https://gitlab.cern.ch/rce/pixelrce/blob/78b980d3085d636e368e9b3a49b86f62d46685a5/rce/fw-hsio2/firmware/modules/StdLib/build/vivado_proc_v1.tcl
#
#connect the probe ports in the debug core to the signals being probed in the design
source vivado_debug_core_inc.tcl

set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets adc_data_clk]
#connect_debug_port u_ila_0/clk [get_nets [list main_clk_100]]

set padc_i  "adc_data_producer_inst"

set_property port_width 1 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets acq_on_r]

ConfigProbe u_ila_0 clk_50_cnt_i[*]
ConfigProbe u_ila_0 adc_start_conv_n_i
ConfigProbe u_ila_0 s_axis_c2h_tvalid_1
ConfigProbe u_ila_0 s_axis_c2h_tready_1
ConfigProbe u_ila_0 s_axis_c2h_tdata_1[*]
ConfigProbe u_ila_0 ${padc_i}/data_valid_c2h1_r
ConfigProbe u_ila_0 ${padc_i}/cnt_decim_c2h_1_r
ConfigProbe u_ila_0 ${padc_i}/fifo_ready_c2h_1
#ConfigProbe u_ila_0 adc_data_hold_i
#ConfigProbe u_ila_0 ${padc_i}/adc_data_p[*]
#ConfigProbe u_ila_0 ${padc_i}/adc_clk_p[*]
#ConfigProbe u_ila_0 ${padc_i}/adc_data_p[*]
#ConfigProbe u_ila_0 ${padc_i}/adc_integral_4[*]
#ConfigProbe u_ila_0 ${padc_i}/integral_double_ar4[*]
#ConfigProbe u_ila_0 ${padc_i}/integral_mult_ar4[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_inp_0[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_inp_1[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_0[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_1[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_0_7[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_1_7[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_0_8[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_1_8[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_0_9[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt_1_9[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt32_0[*]
#ConfigProbe u_ila_0 ${padc_i}/accum_rslt32_1[*]
#ConfigProbe u_ila_0 ${padc_i}/integral_valid_r
#ConfigProbe u_ila_0 ${padc_i}/accum_in_vld_0_r
#ConfigProbe u_ila_0 ${padc_i}/accum_in_vld_1_r
#ConfigProbe u_ila_0 ${padc_i}/accum_out_vld_0
#ConfigProbe u_ila_0 ${padc_i}/accum_out_vld_1
#ConfigProbe u_ila_0 ${padc_i}/singleInt64valid[0]
#ConfigProbe u_ila_0 ${padc_i}/multDoublevalid[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_0[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_1[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_2[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_3[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_4[*]
#ConfigProbe u_ila_0 ${padc_i}/float_val_5[*]
#ConfigProbe u_ila_0 ${padc_i}/last_accum_F_r[*]
#ConfigProbe u_ila_0 ${padc_i}/last_accum_dF_r[*]
#ConfigProbe u_ila_0 ${padc_i}/sub_rdy_0
#ConfigProbe u_ila_0 ${padc_i}/sub_rdy_1
#ConfigProbe u_ila_0 ${padc_i}/sub_rslt_0[*]
#ConfigProbe u_ila_0 ${padc_i}/sub_rslt_1[*]
ConfigProbe u_ila_0 ${padc_i}/aresetn_r

#set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0]


