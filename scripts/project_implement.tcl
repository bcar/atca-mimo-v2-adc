###############################################################################
#
# project_implement_all.tcl: Tcl script for creating the VIVADO project
#
# Usage:
# vivado -mode batch -source project_implement_all.tcl
# See https://github.com/Digilent/digilent-vivado-scripts
################################################################################
set DEBUG_CORE false
set WRITE_MCS true
# increase effort
set EXPLORE false
set REPORTING true

set top_file atca_mimo_v2_adc
set prog_file atcav2xdma

# Set the reference directory to where the script is
set origin_dir [file dirname [info script]]

cd $origin_dir
#
################################################################################
# install UltraFast Design Methodology from TCL Store
#################################################################################

tclapp::install -quiet ultrafast

#
################################################################################
# define paths
################################################################################

set path_rtl ../src/hdl
set path_ip  ../src/ip
set path_sdc ../src/constraints

set path_out ../output
set path_bin ../binaries

file mkdir $path_out
file mkdir $path_bin
################################################################################
# setup the project
################################################################################

source $origin_dir/part.tcl
#set part xc7k325tfbg676-2
puts "Part is: $part"

## Create project
create_project -in_memory -part $part

################################################################################
# read files:
# 1. RTL design sources
# 2. IP database files
# 3. constraints
################################################################################
read_ip    $path_ip/DoubleAccum_lat2/DoubleAccum_lat2.xci
read_ip    $path_ip/xdma_id0032/xdma_id0032.xci
read_ip    $path_ip/DoubleMult_lat1/DoubleMult_lat1.xci
read_ip    $path_ip/Double2Single_lat0/Double2Single_lat0.xci
read_ip    $path_ip/Int64toDouble_lat1/Int64toDouble_lat1.xci
read_ip    $path_ip/Int64toFloat_lat1/Int64toFloat_lat1.xci
read_ip    $path_ip/FpSingleSub_lat1/FpSingleSub_lat1.xci
read_ip    $path_ip/Float2Double_lat0/Float2Double_lat0.xci

#https://support.xilinx.com/s/article/54810
#set_property is_locked false [get_files $path_ip/DoubleAccum_lat1/DoubleAccum_lat1.xci]
#generate_target synthesis [get_ips DoubleAccum_lat1]

#generate_target  {synthesis} [get_ips DoubleAccum_lat1] -force
##generate_target  all [get_ips] -force
#https://www.xilinx.com/support/answers/58526.html
#generate_target  {synthesis implementation instantiation_template} [get_ips]
#generate_target  synthesis [get_ips]
# Create a DCP for the IPs
#synth_ip [get_ips]
#generate_target  {all} [get_files $path_ip/FpSingleMult_lat1/FpSingleMult_lat1.xci]
#  https://support.xilinx.com/s/article/59335
# Create a DCP for the IP
#synth_ip [get_files $path_ip/FpSingleMult_lat1/FpSingleMult_lat1.xci]
# Query all the files for this IP (optional)
#get_files -all -of_objects [get_files $path_ip/FpSingleMult_lat1/FpSingleMult_lat1.xci]
#
add_files              $path_rtl

set_property top_file {$path_rtl/$top_file.sv} [current_fileset]
##generate_target  all [get_ips] -force
#https://www.xilinx.com/support/answers/58526.html
#generate_target  {synthesis implementation instantiation_template} [get_ips]
#generate_target  {synthesis instantiation_template} [get_ips]
# Create a DCP for the IPs
#synth_ip [get_ips]

read_xdc   $path_sdc/atca_mimo_v2_adc_x4g2.xdc
read_xdc   $path_sdc/atca_adc_channels.xdc
read_xdc   $path_sdc/atca_adc_channels_clk_route.xdc

# Optional: to implement put on Tcl Console
################################################################################
# run synthesis
# report utilization and timing estimates
# write checkpoint design (open_checkpoint filename)
################################################################################

set_param general.maxThreads 16

auto_detect_xpm
synth_design -top atca_mimo_v2_adc -flatten_hierarchy rebuilt -bufg 16
#synth_design -top atca_mimo_v2_adc -bufg 24
# synth_design -top red_pitaya_top -flatten_hierarchy none -bufg 16 -keep_equivalent_registers

if {$REPORTING == true} {
    write_checkpoint         -force   $path_out/post_synth
    report_timing_summary    -file    $path_out/post_synth_timing_summary.rpt
    report_power             -file    $path_out/post_synth_power.rpt
}

################################################################################
# insert debug core
#
################################################################################
if {$DEBUG_CORE == true} {
    source atca_debug_signals.tcl
}

#####################################################################
# run placement and logic optimization
# report utilization and timing estimates
# write checkpoint design
#####################################################################

if {$EXPLORE == true} {
    opt_design  -directive Explore
} else {
    opt_design
}

power_opt_design

# place_design
if {$EXPLORE == true} {
    place_design  -directive Explore
} else {
    place_design
}

if {$EXPLORE == true} {
    phys_opt_design  -directive Explore
} else {
    phys_opt_design
}
if {$REPORTING == true} {
    write_checkpoint       -force $path_out/post_place
    report_timing_summary  -file  $path_out/post_place_timing_summary.rpt
}

#####################################################################
# run router
# report actual utilization and timing,
# write checkpoint design
# run drc, write verilog and xdc out
################################################################################

if {$EXPLORE == true} {
    route_design  -directive Explore
} else {
    route_design
}
report_timing_summary  -file  $path_out/post_route_timing_summary.rpt

if {$REPORTING == true} {
    write_checkpoint         -force   $path_out/post_route
    report_timing            -file    $path_out/post_route_timing.rpt \
        -sort_by group -max_paths 100 -path_type summary
    report_clock_utilization -file    $path_out/clock_util.rpt
    report_utilization       -file    $path_out/post_route_util.rpt
    report_power             -file    $path_out/post_route_power.rpt
    report_io                -file    $path_out/post_imp_io.rpt
    write_verilog            -force   $path_out/bft_impl_netlist.v
    write_xdc -no_fixed_only -force   $path_out/bft_impl.xdc

    xilinx::ultrafast::report_io_reg -verbose -file $path_out/post_route_iob.rpt
}

report_drc               -file    $path_out/post_imp_drc.rpt

################################################################################
# generate a bitstream and debug probes
################################################################################

if {$DEBUG_CORE == true} {
    write_debug_probes -force            $path_bin/${prog_file}.ltx
}

write_bitstream -force            $path_bin/${prog_file}.bit

close_project

if {${WRITE_MCS} == true} {
    #SPIx4
write_cfgmem -force -format MCS -size 256 -interface SPIx4 -loadbit "up 0x0 \
    $path_bin/${prog_file}.bit" "$path_bin/${prog_file}.mcs"
}

exit
