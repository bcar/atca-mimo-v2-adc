#!/bin/bash
XPATH=/home/xilinx/Vivado/2023.1/bin
#XILINXD_LICENSE_FILE=/home/xilinx/vivado2016+IP.lic.dont.use
cd ../src/hdl
./update_ts.sh
cd ../../scripts

time $XPATH/vivado -mode batch -source ./project_implement.tcl

