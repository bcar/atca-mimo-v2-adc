###############################################################################
#
# project_implement_all.tcl: Tcl script for creating the VIVADO project
#
# Usage:
# source /home/Xilinx/Vivado/2019.1/settings64.sh
# vivado -mode batch -source project_implement_all.tcl
# See https://github.com/Digilent/digilent-vivado-scripts
################################################################################
#set DEBUG_CORE false
set DEBUG_CORE true
set WRITE_MCS true

set top_file atca_mimo_v2_adc
set prog_file atcav2

# Set the reference directory to where the script is
set origin_dir [file dirname [info script]]

cd $origin_dir
#
################################################################################
# install UltraFast Design Methodology from TCL Store
#################################################################################

tclapp::install -quiet ultrafast

#
################################################################################
# define paths
################################################################################

set path_rtl ../src/hdl
set path_ip  ../src/ip
set path_sdc ../src/constraints

set path_out ../out

file mkdir $path_out
################################################################################
# setup the project
################################################################################

set part xc7k325tfbg676-2

## Create project
create_project -in_memory -part $part

#auto_detect_xpm

################################################################################
# read files:
# 1. RTL design sources
# 2. IP database files
# 3. constraints
################################################################################

add_files              $path_rtl

set_property top_file {$path_rtl/$top_file} [current_fileset]
read_ip    $path_ip/pcie_7x_gen2_id30/pcie_7x_gen2_id30.xci
read_ip    $path_ip/dma_fifo/dma_fifo.xci

##generate_target  all [get_ips] -force
#https://www.xilinx.com/support/answers/58526.html
#generate_target  {synthesis implementation instantiation_template} [get_ips]
generate_target  {synthesis instantiation_template} [get_ips]

read_xdc   $path_sdc/atca_mimo_v2_adc_x4g2.xdc

#set_property used_in_synthesis false [get_files atca_adc_channels.xdc]
#set_property used_in_implementation true [get_files atca_adc_channels.xdc]

if {$WRITE_MCS == true} {
    read_xdc   $path_sdc/trenz_te0741_spi.xdc
}

# Optional: to implement put on Tcl Console
################################################################################
# run synthesis
# report utilization and timing estimates
# write checkpoint design (open_checkpoint filename)
################################################################################

set_param general.maxThreads 8

auto_detect_xpm
synth_design -top $top_file
#synth_design -top red_pitaya_top -flatten_hierarchy none -bufg 16 -keep_equivalent_registers

write_checkpoint         -force   $path_out/post_synth
report_timing_summary    -file    $path_out/post_synth_timing_summary.rpt
report_power             -file    $path_out/post_synth_power.rpt

################################################################################
# insert debug core
#
################################################################################
if {$DEBUG_CORE == true} {
    source debug_core.tcl
}

#####################################################################
# run placement and logic optimization
# report utilization and timing estimates
# write checkpoint design
#####################################################################
read_xdc   $path_sdc/atca_adc_channels.xdc

opt_design
power_opt_design

read_xdc   $path_sdc/atca_adc_channels_clk_route.xdc
place_design

phys_opt_design
write_checkpoint         -force   $path_out/post_place
report_timing_summary    -file    $path_out/post_place_timing_summary.rpt
#write_hwdef              -file    $path_sdk/red_pitaya.hwdef

#####################################################################
# run router
# report actual utilization and timing,
# write checkpoint design
# run drc, write verilog and xdc out
################################################################################

route_design
write_checkpoint         -force   $path_out/post_route
report_timing_summary    -file    $path_out/post_route_timing_summary.rpt
report_timing            -file    $path_out/post_route_timing.rpt -sort_by group -max_paths 100 -path_type summary
report_clock_utilization -file    $path_out/clock_util.rpt
report_utilization       -file    $path_out/post_route_util.rpt
report_power             -file    $path_out/post_route_power.rpt
report_drc               -file    $path_out/post_imp_drc.rpt
report_io                -file    $path_out/post_imp_io.rpt
#write_verilog            -force   $path_out/bft_impl_netlist.v
#write_xdc -no_fixed_only -force   $path_out/bft_impl.xdc

xilinx::ultrafast::report_io_reg -verbose -file $path_out/post_route_iob.rpt

################################################################################
# generate a bitstream and debug probes
################################################################################

if {$DEBUG_CORE == true} {
    write_debug_probes -force            $path_out/${prog_file}.ltx
}

write_bitstream -force            $path_out/${prog_file}.bit

close_project

if {${WRITE_MCS} == true} {
#SPIx4
write_cfgmem -force -format MCS -size 256 -interface SPIx4 -loadbit "up 0x0 $path_out/${prog_file}.bit" "$path_out/${prog_file}.mcs"

}
exit
