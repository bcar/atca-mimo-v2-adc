# Usage:
# source /home/Xilinx/Vivado/20xx.x/settings64.sh
# vivado -mode batch -source program_spi_flash_mem.tcl
#
#
# Set the reference directory to where the script is
# Set the reference directory to where the script is
set origin_dir [file dirname [info script]]

cd $origin_dir
set prog_file atcav2xdma
set mcs_file "../binaries/${prog_file}.mcs"

set fpga_device xc7k325t_*
set cfgParts "s25fl256sxxxxxx0-spi-x1_x2_x4"

open_hw_manager

# connect_hw_server -url localhost:3121
connect_hw_server -url 193.136.136.88:3121
#refresh_hw_server
# Change here to your's programmer's reference:
current_hw_target [get_hw_targets */xilinx_tcf/Digilent/201809*]
# set_property PARAM.FREQUENCY 10000000 [get_hw_targets */xilinx_tcf/Digilent/201809*]
set_property PARAM.FREQUENCY 10000000 [current_hw_target]
# current_hw_target [get_hw_targets */xilinx_tcf/Xilinx/000012929e2a01]
open_hw_target

# get first KIntex Fpga
current_hw_device [lindex [get_hw_devices ${fpga_device}] 0]
# refresh_hw_device -update_hw_probes false [lindex [get_hw_devices ${fpga_device}] 0]
refresh_hw_device -update_hw_probes false [current_hw_device]
create_hw_cfgmem -hw_device [current_hw_device] [lindex $cfgParts 0 ]
set cfgMem [current_hw_cfgmem]
set_property PROGRAM.BLANK_CHECK 0 $cfgMem
set_property PROGRAM.ERASE  1 $cfgMem
set_property PROGRAM.CFG_PROGRAM  1 $cfgMem
set_property PROGRAM.VERIFY  1 $cfgMem
set_property PROGRAM.CHECKSUM 0 $cfgMem
refresh_hw_device [lindex [get_hw_devices ${fpga_device}] 0]
set_property PROGRAM.ADDRESS_RANGE  {use_file} $cfgMem
set_property PROGRAM.FILES $mcs_file $cfgMem
set_property PROGRAM.PRM_FILE {} $cfgMem
set_property PROGRAM.UNUSED_PIN_TERMINATION {pull-none} $cfgMem
# set_property PROGRAM.BPI_RS_PINS {none} $cfgMem
create_hw_bitstream -hw_device [current_hw_device] [get_property PROGRAM.HW_CFGMEM_BITFILE [current_hw_device]]
program_hw_devices [current_hw_device]
refresh_hw_device [current_hw_device]
program_hw_cfgmem [current_hw_cfgmem]
boot_hw_device [current_hw_device]
# exit
