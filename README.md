# Firmware  project running in IPP-HGW on IPFN ATCA-MIMO-ISOL V2.1 Hardware Boards
## Xilinx Kintex 7 FPGA Module
 * The ATCA contains an FPGA plugin Module [Trenz TE0741](https://wiki.trenz-electronic.de/display/PD/TE0741)

## Getting started

1.  Prerequisites
 * The Master branch in currently using [Vivado Design Suite](https://www.xilinx.com/support/download.html),
 version 2022.1, which needs a valid license to compile projects using Xilinx Kintex-7 XC7K325T FPGA

2.  Checkout the latest release, and `cd atca-mimo-v2-adc` to the folder
3. Create and build the project in Vivado **Project Mode** (with GUI)

Open Vivado IDE and do:
```
 Menu Tools->Run Tcl Script-> "scripts/project_create.tcl"
 You will need to (re-)generate IP cores in used in the Project, at least 
 PCIe XDMA endpoint/DMA engine.

```
Project files will be generated in `vivado_project` folder

4. (optional) Build the project in Vivado **non-Project Mode** (CLI in Linux)

* Open a Linux console and Run:
```
 [~]source [...]/Xilinx/Vivado/2022.1/settings64.sh
 [~]time vivado -mode batch -source project_implement.tcl

```

 Generated files will be in `out` folder.

 The Project should compile in ~ 15 minutes on a Intel i7 4-core Machine.


## Program the FPGA (Kintex 7).
1. Program in **non-Project Mode**, for temporary testing.
```
 [~]vivado -mode batch -source scripts/program_fpga.tcl
```
* You may need to change the reference ID to your JTAG programmer. 
* This may be well connected a remote machine running the Xilinx Hardware [Server](https://www.xilinx.com/member/forms/download/xef-vivado.html?filename=Xilinx_HW_Server_Lin_2019.2_1106_2127.tar.gz).
(You may need use the same Vivado version!), or a remote Xilinx programmer

2. Program the FPGA configuration Memory and reboot FPGA.
* Run Vivado GUI
```
 source /opt/Xilix/Vivado_Lab/2021.1/settings.sh
 vivado_lab&
```
* Connect to remote Xilinx Program and add configuration memory
* Trenz Module has a SPI Memory type `s25fl256sxxxxxx0-spi-x1_x2_x4`
* Program SPI with default parameters and reboot

 ## Program the IPMC-Module (TI MSP430F5253 embedded processor)
 
* As the FPGA and the IPMC module now communicate closer, changes in the FPGA code
also need in some cases an update of the firmware in the IPMC-module. 
This can now be accomplished completely from remote. The JTAG cable connector of the TI USB-FET programmer needs to be connected to the IPMC-module plug on the ATCA_V2 board. The USB-A connector of the TI USB-FET programmer is connected to an USB-port of the Linux host computer of the ATCA test system (HGW CoDaC Lab) where also the programming software (MSPFlasher) is installed. When the programmer is connected correctly to the host the Linux command `lsusb | grep MSP` shows the following info:

Bus 002 Device 002: ID 2047:0014 Texas Instruments MSP-FET 

* Now the IPMC-module can be programmed: 
The software for the programmer is in the folder `/opt/ti/MSPFlasher_1.3.20` with the binary `MSP430Flasher` and two shell-scripts: `Program_IPMC_master.sh` and `Program_IPMC_slave` where
one is for the Master-version of the ATCA_V2 board (with an RTM connected) the other is for the Slave-version, normally used when in an ATCA-shelf are several ATCA boards (old V1 or new V2) where only one board can be the Master and the other are Slaves. The binary files `ipmcApi_3_master.hex` and `ipmcApi_3_slave.hex` which are uploaded in the shell-scripts to the IPMC-module also need be in the same folder 
`/opt/ti/MSPFlasher_1.3.20`  REM:this folder can be only written with sudo permissions)
To programm e.g. the ATCA_V2 Master use:  `sudo ./Program_IPMC_master.sh`

* The actual source code of the IPMC-module and documentation of how to use the TI Code Composer Studio is available in the Gitlab repository here:
https://gitlab.mpcdf.mpg.de/mmz/ipmc-atca

   

## Linux Sofware

* Driver code, C++ API and tools were now moved to a different repository:

[MPCDF Gitlab](https://gitlab.mpcdf.mpg.de/bcar/dma_ip_drivers)  
Forked from Xilinx github [xdma drivers](https://github.com/Xilinx/dma_ip_drivers) 

* Driver should compile in recent version >= 4.x.y kernels  
Go to `XDMA/linux-kernel/xdma`and run:
- `make`

 * Load driver in root mode with
    - `$ insmod adc_xdma.ko`
    - install driver permanently with `make install`
    - Check driver loading and troubleshoot with `dmesg`

### If you reprogram FPGA, reboot machine or reload driver with:
```
echo 1 > /sys/bus/pci/devices/0000\:07\:00.0/remove
# Youi may need to change the correct device number, check with 'lspci | grep Xilinx'
echo 1 > /sys/bus/pci/rescan
```
