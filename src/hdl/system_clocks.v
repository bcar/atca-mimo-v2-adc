//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Company: INSTITUTO DOS PLASMAS E FUSAO NUCLEAR
// Engineer:  BBC
//
// Project Name:   W7X ATCA V2 DAQ
// Design Name:    ATCA V2_DAQ FIRMWARE
// Module Name:    system_clocks
// Target Devices: kintex-7
// Tool Versions:  Vivado 2023.1
//
// Description:
//
// Copyright 2015 - 2021 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date   Wed May 22 17:21:15 WEST 2019
//
// Licensed under the EUPL, Version 1.1 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");

// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// http://ec.europa.eu/idabc/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//////////////////////////////////////////////////////////////////////////////////////
//
`timescale 1ns/1ps
module system_clocks (

    input adc_data_clk,

    output [4:0] clk_50_cnt,

    output adcs_word_sync_n,
    output adc_sreg_rst_n,
    output adcs_start_conv_n,
    output clk_2mhz_tte,       // sync with ADCs_word_sync but duty cycle = 50%
    output [31:0] clk_2_cnt  //
);

    reg [4:0] cnt_50_r  = 'h00;
    reg [31:0] cnt_2_r  = 32'h00;

    reg start_conv_en		= 1'b0;
    reg adc_sreg_rst_n_r	= 1'b1;
    reg clk_2Mhz_square_r	= 1'b1;

    reg word_sync_n= 1'b1;

    assign clk_2mhz_tte = clk_2Mhz_square_r;
    assign clk_50_cnt = cnt_50_r;
    assign clk_2_cnt = cnt_2_r;
    assign adc_sreg_rst_n = adc_sreg_rst_n_r;

    always @ (posedge adc_data_clk )
    begin
        cnt_50_r <= cnt_50_r + 1;
        case(cnt_50_r)
            5'd00: begin
                start_conv_en     <= 1'b1;
                clk_2Mhz_square_r <= 1'b1;
                cnt_2_r           <= cnt_2_r + 1;
            end
            5'd01:  start_conv_en       <= 1'b0; // Convert Pulse Width, t1 Min:15ns, Max:70
            5'd12:  clk_2Mhz_square_r   <= 1'b0;
            5'd21:  word_sync_n         <= 1'b0;
            5'd22:  word_sync_n         <= 1'b1;
            //5'd23:  adc_sreg_rst_n_r    <= 1'b0; // adc clk  30 ns async reset
            //5'd23:  adc_sreg_rst_n_r    <= 1'b1;
            5'd24:  cnt_50_r            <= 5'h00;   // counts 25 periods (500 ns)
            default: ;

        endcase
    end
    assign clk_2mhz_tte  = clk_2Mhz_square_r;
    assign adcs_word_sync_n = word_sync_n;

    //assign adcs_start_conv_n = start_conv_en; // delay 50ns
    /* makes a 20 ns pulse, low jitter */
    BUFGCE_1 BUFGCE_1_inst ( .O(adcs_start_conv_n), .CE(start_conv_en), .I(adc_data_clk));

endmodule // system_clocks
/* vim: set ts=8 sw=4 tw=0 noet : */