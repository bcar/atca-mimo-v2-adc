//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Company: INSTITUTO DOS PLASMAS E FUSAO NUCLEAR
// Engineer:  BBC
//
// Project Name:   W7X ATCA DAQ
// Design Name:    ATCA V2_DAQ FIRMWARE
// Module Name:    chop_gen
// Target Devices:
//
//Description:
//
// Copyright 2015 - 2019 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date   Wed May 22 17:21:15 WEST 2019
//
// Licensed under the EUPL, Version 1.1 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");

// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// http://ec.europa.eu/idabc/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
`include "atca_mimo_v2_config.vh"
module chop_gen #(
    parameter CHOP_DEFAULT = 1'b0,        // default for "Normal" modules
    parameter CHOP_DLAY    = `CHOP_DLAY,      // Delay N data samples for phase reconstruction
    //parameter HOLD_SAMPLES = `HOLD_SAMPLES, // Suppress N data samples and hold last good values during dechopping algorithm
    parameter HOLD_N_BITS = 6, // Suppress N data samples and hold last good values during dechopping algorithm
    
    parameter TCQ        = 1
)(
    input adc_data_clk,  // ADC data  clock domain (50/100)
    input adc_word_sync_n,
    //input reset_n,
    input chop_en,
    input [15:0] max_count,     // 16'd2000 -> 1kHz
    input [15:0] change_count,  // max_count / 2 -> 50 % d.c.
    input [HOLD_N_BITS-1:0] hold_samples,  // 0, 2 - 63 , do not use 1. Suppress N data samples and hold last good values during dechopping algorithm
    output chop_o,
    output chop_dly_o,
    output data_hold_o
);

   function  [15:0] hold_change_rst_count_f;  // integer 64 + 16 fractional bits
        input [15:0] change_cnt;
        input [HOLD_N_BITS-1:0] hold_val;
        reg [15:0] hold_ext;        
        begin
            hold_ext = {10'h00, hold_val};
            hold_change_rst_count_f = change_cnt + hold_ext - 16'h0001;
        end
   endfunction
      
    reg chop_r;
    assign chop_o = chop_r;

    reg [CHOP_DLAY-1:0] chop_dly = 0;
    assign chop_dly_o = chop_dly[CHOP_DLAY-1];

    reg hold_r;
    assign data_hold_o = hold_r;
    //reg [HOLD_MAX_SAMPLES-1:0] hold_dly;
    //assign data_hold_o = hold_dly[CHOP_DLAY-1];

    reg [15:0] chop_counter_r = 0;
    always @(posedge adc_data_clk or negedge chop_en)
        if(!chop_en) begin
            chop_counter_r <= 16'h0000;
            chop_r <= CHOP_DEFAULT;
            hold_r <= 1'b0;
            chop_dly <= {CHOP_DLAY{1'b0}}; //0;
            //hold_dly <= {CHOP_DLAY{1'b0}}; //0;
        end
        else begin
            if(!adc_word_sync_n) begin
                    chop_counter_r  <= #TCQ  chop_counter_r + 1;
                    chop_dly        <= #TCQ {chop_dly[CHOP_DLAY-2:0], chop_r};
                   // hold_dly        <= #TCQ {hold_dly[CHOP_DLAY-2:0], hold_r};
                    // case(chop_counter_r)
                    hold_r      <= #TCQ 1'b0;
                end
                else begin
                    if ( chop_counter_r == {10'h00, hold_samples})
                        hold_r      <= #TCQ 1'b0;
                    else if ( chop_counter_r == hold_change_rst_count_f(change_count, hold_samples) )
                        hold_r      <= #TCQ 1'b0;
                        
                    if ( chop_counter_r == (change_count-1) ) begin
                        chop_r      <= #TCQ !CHOP_DEFAULT;
                        if (hold_samples > 6'h01)
                            hold_r      <= #TCQ 1'b1;
                    end
//                    else if ( chop_counter_r == (change_count + HOLD_SAMPLES-1) )
                    else if ( chop_counter_r == (max_count - 1) ) begin
                        chop_counter_r  <= 16'h0000;
                        chop_r          <= #TCQ CHOP_DEFAULT;
                        if (hold_samples > 6'h01)
                            hold_r          <= #TCQ 1'b1;
                    end
                end
        end

endmodule //chop_gen
// vim: set ts=8 sw=4 tw=0 et :