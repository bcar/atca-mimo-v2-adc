//////////////////////////////////////////////////////////////////////////////////
// Company: IPFN-IST
// Engineer: BBC
//
// Create Date: 05/02/2019 07:21:01 PM
// Design Name: atca_mimo_v2_adc.sv
// Module Name:  atca_mimo_v2_adc (top file)
// Project Name: ATCA MIMO V2
// Target Devices: kintex-7
// Tool Versions:  Vivado 2023.1
// Description:
// Firmware  project running in IPP-HGW on IPFN ATCA-MIMO-ISOL V2.1 Hardware Boards
// Xilinx Kintex 7 FPGA Module
//
// Dependencies:
//
// Revision 2 - File Created
// Additional Comments:
//
// Copyright 2020 - 2023 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date  2020-11-09
//
// Licensed under the EUPL, Version 1.2 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//////////////////////////////////////////////////////////////////////////////////
//
// (c) Copyright 2012-2012 Xilinx, Inc. All rights reserved.
//
// This file contains confidential and// Master ATCA Shared clock output proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
//
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the marximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
//
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
//
//-----------------------------------------------------------------------------
//
// Version    : 4.1
//-----------------------------------------------------------------------------

`timescale 1ps / 1ps
`include "atca_mimo_v2_config.vh"
`include "control_word_bits.vh"

module atca_mimo_v2_adc #(
    parameter PL_LINK_CAP_MAX_LINK_WIDTH          = 4,            // 1- X1; 2 - X2; 4 - X4; 8 - X8
    parameter PL_SIM_FAST_LINK_TRAINING           = "FALSE",      // Simulation Speedup
    parameter PL_LINK_CAP_MAX_LINK_SPEED          = 2,             // 1- GEN1; 2 - GEN2; 4 - GEN3
    parameter C_DATA_WIDTH                        = 128,
    parameter EXT_PIPE_SIM                        = "FALSE",  // This Parameter has effect on selecting Enable External PIPE Interface in GUI.
    parameter C_ROOT_PORT                         = "FALSE",      // PCIe block is in root port mode
    parameter C_DEVICE_NUMBER                     = 0,            // Device number for Root Port configurations only

    parameter ADC_DATA_WIDTH   = `ADC_DATA_WIDTH,
    parameter FLOAT_WIDTH      = `FLOAT_WIDTH,

    parameter N_ADC_CHANNELS   = `N_ADC_MAX_CHANNELS,
    parameter N_INT_CHANNELS   = `N_INT_CHANNELS,
    parameter N_ILOCK_PARAMS   =  N_INT_CHANNELS + 2,
    parameter EO_VECT_WIDTH   = ADC_DATA_WIDTH * N_ADC_CHANNELS,
    parameter WO_VECT_WIDTH   = FLOAT_WIDTH    * N_ADC_CHANNELS,
    parameter ILCK_VECT_WIDTH = FLOAT_WIDTH    * N_ILOCK_PARAMS,
    parameter TCQ        = 1
    )
    (
    output [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0] pci_exp_txp,
    output [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0] pci_exp_txn,
    input [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0]  pci_exp_rxp,
    input [(PL_LINK_CAP_MAX_LINK_WIDTH - 1) : 0]  pci_exp_rxn,

    //TE0741 100Mhz clk and chip enables
    input   te0741_clk_100_p,
    input   te0741_clk_100_n,

    output  te0741_en_mgt,
    output  te0741_en_clk,

    output  te0741_led1,  // Red LED on Trenz Module. Should always blink
    output  te0741_led2,  // Green LED on Trenz Module

// ATCA CLK Signals
    input    atca_3a_r, //  (Sys reset, Not connected in AXI crates)
    output   atca_1a_d, // ATCA Master Shared clock output
    input    atca_1a_r, // ATCA Master Shared clock input

    output   atca_1b_d, // ATCA Shared trigger ouput (Active Low)
    input    atca_1b_r, // ATCA Shared trigger input (Active Low)

    // ADC interface
    output adcs_start_conv_p,
    output adcs_start_conv_n,

    output adcs_reset,      // Active low
    output adcs_power_down, // Chopp signal

    input [N_ADC_CHANNELS-1:0] adc_clk_p,
    input [N_ADC_CHANNELS-1:0] adc_clk_n,
    input [N_ADC_CHANNELS-1:0] adc_data_p,
    input [N_ADC_CHANNELS-1:0] adc_data_n,

    // RTM signals
    output  rtm_io_d5, // RTM ADC CLOCK OUT
    output  rtm_io_d6, // RTM CHOPPER ADC OUT
    output  rtm_io_d7, // RTM INTERLOCK OUT

    input  rtm_io_r1, // fpga MIO13 pin,  RTM 10 MHz CLOCK IN
    input  rtm_io_r4, // RTM TRIGGER  IN
    input  rtm_io_r8, // Free RTM  IN. Trasmitted to IPMC
    //  I2C IPMC interface signals
    inout           fpga_sda,
    input           fpga_scl,

    output  atca_led_d7,  // ATCA Front panel LED marked "Vref"
    
    // PCIEe
    input    sys_clk_p,
    input    sys_clk_n
    //input  sys_rst_n  // This board uses ATCA RX3 signal
 );

   //-----------------------------------------------------------------------------------------------------------------------

   // Local Parameters derived from user selection
   localparam integer       USER_CLK_FREQ         = ((PL_LINK_CAP_MAX_LINK_SPEED == 3'h4) ? 5 : 4);
   //localparam TCQ = 1;
   localparam C_S_AXI_ID_WIDTH = 4;
   localparam C_M_AXI_ID_WIDTH = 4;
   localparam C_M_AXI_DATA_WIDTH = 32;  //  LITE_AXI_DATA_WIDTH
   localparam C_S_AXI_DATA_WIDTH = C_M_AXI_DATA_WIDTH;
   localparam C_M_AXI_ADDR_WIDTH = 32;
   localparam C_S_AXI_ADDR_WIDTH = 10;
   localparam C_NUM_USR_IRQ  = 1;

    // Width o LITE AXI data bus for internal regosters 
    //parameter integer LITE_AXI_DATA_WIDTH    = 32,

   wire                        user_lnk_up;

   //----------PCIEe------------------------------------------------------------------------------------------------------//
   //  AXI Interface                                                                                                 //
   //----------------------------------------------------------------------------------------------------------------//
   wire                        axi_aclk;    //  125 Mhz
   wire                        axi_aresetn;

  //----------------------------------------------------------------------------------------------------------------//
  //    System(SYS) Interface                                                                                       //
  //----------------------------------------------------------------------------------------------------------------//
   //wire     sys_rst_n;
    wire                                    sys_clk;
    wire                                    sys_clk_gt;
    wire                                    sys_rst_n_c;

  // User Clock LED Heartbeat
     reg [25:0]                  axi_aclk_heartbeat;
     reg [C_NUM_USR_IRQ-1:0]             usr_irq_req = 0;
     wire [C_NUM_USR_IRQ-1:0]            usr_irq_ack;

//----------------------------------------------------------------------------------------------------------------//
//     AXI LITE Master
//----------------------------------------------------------------------------------------------------------------//
    //-- AXI Master Write Address Channel
    wire [C_M_AXI_ADDR_WIDTH-1:0] m_axil_awaddr;
   // wire [31:0] m_axil_awaddr;
    wire [2:0]  m_axil_awprot;
    wire    m_axil_awvalid;
    wire    m_axil_awready;

    //-- AXI Master Write Data Channel
    wire [C_M_AXI_DATA_WIDTH-1:0] m_axil_wdata;
    wire [3:0]  m_axil_wstrb;
    wire    m_axil_wvalid;
    wire    m_axil_wready;
    //-- AXI Master Write Response Channel
    wire    m_axil_bvalid;
    wire    m_axil_bready;
    //-- AXI Master Read Address Channel
    wire [C_M_AXI_ADDR_WIDTH-1:0]     m_axil_araddr;
    //wire [31:0] m_axil_araddr;
    wire [2:0]  m_axil_arprot;
    wire    m_axil_arvalid;
    wire    m_axil_arready;
    //-- AXI Master Read Data Channel
    wire [C_M_AXI_DATA_WIDTH-1:0] m_axil_rdata;
    wire [1:0]  m_axil_rresp;
    wire    m_axil_rvalid;
    wire    m_axil_rready;
    wire [1:0]  m_axil_bresp;

    wire [2:0]    msi_vector_width;
    wire          msi_enable;

    //-- AXI streaming ports 1 * H2C, 2 * C2H
    wire [C_DATA_WIDTH-1:0] m_axis_h2c_tdata_0;
    wire            m_axis_h2c_tlast_0;
    wire            m_axis_h2c_tvalid_0;
    wire            m_axis_h2c_tready_0;
    wire [C_DATA_WIDTH/8-1:0]   m_axis_h2c_tkeep_0;

    wire [C_DATA_WIDTH-1:0] s_axis_c2h_tdata_0, s_axis_c2h_tdata_1;
    wire s_axis_c2h_tlast_0, s_axis_c2h_tlast_1;
    (* keep = "true" *) wire s_axis_c2h_tvalid_0, s_axis_c2h_tvalid_1;
    (* keep = "true" *) wire s_axis_c2h_tready_0, s_axis_c2h_tready_1;
    wire [C_DATA_WIDTH/8-1:0] s_axis_c2h_tkeep_0, s_axis_c2h_tkeep_1;

    assign m_axis_h2c_tready_0 = 1'b1; // Allways Flush H2C data

   // PCIEe Ref clock buffer
   IBUFDS_GTE2 refclk_ibuf (.O(sys_clk), .ODIV2(), .I(sys_clk_p), .CEB(1'b0), .IB(sys_clk_n));

// ----- IPFN CODE ---------------------

    wire [C_M_AXI_DATA_WIDTH-1 :0] i2C_reg0;

    wire [3:0] slot_id = i2C_reg0[3:0];
    wire  atca_master = i2C_reg0[4]; // now connected to to IPMC i2c;
    wire  [EO_VECT_WIDTH-1 :0]   eo_offset_i;
    wire  [WO_VECT_WIDTH-1 :0]   wo_offset_i;
    wire  [ILCK_VECT_WIDTH-1 :0]  ilck_param_i;

    wire  [C_M_AXI_DATA_WIDTH-1:0] control_reg_i, chopp_period_i, channel_mask_i;

    wire rtm_r4_i, rtm_r1_i, rtm_r8_i;

    wire adc_word_sync_n_i, adc_start_conv_n_i, adc_chop_phase_i, adc_chop_phase_dly_i;
    (* keep = "true" *) wire adc_data_hold_i;
    wire atca_clk1b_i; // Shared trigger signal, active low

    // -------------- TE0741 and RTM clocks ----------
    wire te0741_clk_100_i;
    wire te0741_clk_100_locked_i, atca_clk_locked_i, rtm_clk10_locked_i;//, main_clk_locked_i;

    // -------------- system_clocks ----------
    (* keep = "true" *) wire  adc_data_clk,  clk_100_i, clk_200_i,  clk_2mhz_tte_i;
    wire [5:0] clk_100_cnt_i;
    (* keep = "true" *) wire [4:0] clk_50_cnt_i;
    wire [31:0] clk_2_cnt_i;

    (* keep = "true" *) reg  acq_on_r, acq_on_q;

    //-----------------------------I/O BUFFERS------------------------//

     wire  atca_rst_n;
     wire atca_hard_trig_rcv, atca_hard_trig_d;

     wire atca_1a_i, clk_10_atca_recv, clk_10_atca_drv;
     IBUF atca_rx_1a_buf ( .O(atca_1a_i), .I(atca_1a_r) );
     IBUF atca_rx_1b_buf ( .O(atca_hard_trig_rcv), .I(atca_1b_r) );
  // PCIe Reset buffer
     IBUF atca_rx_3a_buf ( .O(sys_rst_n_c), .I(atca_3a_r) );
    
     BUFG bufg_atca_1a_r ( .O(clk_10_atca_recv), .I(atca_1a_i) );

     // -------------- Trenz TE0741 enable MGTs, CLKs ----------
    OBUF obuf_te0741_en_mgt_inst (.O(te0741_en_mgt), .I(1'b1));
    OBUF obuf_te0741_en_clk_inst (.O(te0741_en_clk), .I(1'b1));

    // IDELAYCTRL: IDELAYE2/ODELAYE2 Tap Delay Value Control for  Kintex-7, clock fpga clk signal

    wire idelay_rdy_w;
//    (* IODELAY_GROUP = iodelay_group_adc *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL
    IDELAYCTRL IDELAYCTRL_adc (
        .RDY(idelay_rdy_w),       // 1-bit output: Ready output
        .REFCLK(clk_200_i), // 1-bit input: Reference clock input
        .RST(!sys_rst_n_c)        // 1-bit input: Active high reset input
    );
    wire [14:10] fifos_status_i;
    wire [4:0] fifos_status_cdc;

   // xpm_cdc_array_single: Single-bit Array Synchronizer
   // Xilinx Parameterized Macro, version 2019.2
    xpm_cdc_array_single #(
      .DEST_SYNC_FF(4),   // DECIMAL; range: 2-10
      .INIT_SYNC_FF(0),   // DECIMAL; 0=disable simulation init values, 1=enable simulation init values
      .SIM_ASSERT_CHK(0), // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      .SRC_INPUT_REG(1),  // DECIMAL; 0=do not register input, 1=register input
      .WIDTH(5)           // DECIMAL; range: 1-1024
   )
   xpm_cdc_array_single_cdc (
      .dest_out(fifos_status_cdc), // WIDTH-bit output: src_in synchronized to the destination clock domain. This
                           // output is registered.

      .dest_clk(axi_aclk), // 1-bit input: Clock signal for the destination clock domain.
      .src_clk(adc_data_clk),   // 1-bit input: optional; required when SRC_INPUT_REG = 1
      .src_in(fifos_status_i)      // WIDTH-bit input: Input single-bit array to be synchronized to destination clock
                           // domain. It is assumed that each bit of the array is unrelated to the others. This
                           // is reflected in the constraints applied to this macro. To transfer a binary value
                           // losslessly across the two clock domains, use the XPM_CDC_GRAY macro instead.
   );

    wire [C_M_AXI_DATA_WIDTH-1 :0] status_reg_i = {8'h00,  // bits 31:24
            rtm_r8_i, atca_master, msi_enable, idelay_rdy_w,  // bits 23:20
                            1'b0, atca_clk_locked_i, te0741_clk_100_locked_i, rtm_clk10_locked_i, // bits 19:16
            1'b0, fifos_status_cdc, acq_on_q, 1'b0,    // bits 15:8
        i2C_reg0[7:0]}; // bits 7:0 atca slot_id 
        
//--          Global clocks --------------------------------

/* ATCA IPP DAQ Clock Configuration for Simple RTM board

      INTERNAL WIRES
                                                                                      |
 ATCA Backplane:                                                                      |
                                                                                      |
        |---------->ATCA_RX_1A -----> (shared signal) -> (BUFG)-> ATCA_shared_clk ----->|
        |           ATCA_RX_2
        |           ATCA_TX_3
        |
        |<--------  ATCA_TX_1B <---------------------< ORed_trigger_n  ( master only)
        |<--------  ATCA_TX_1A <----------------|         ( master only)
                                                |
                                                |
                                                |
 RTM ->(RS485) -> RS485_RX_1 --->(BUFG)->-------|           RS485_RX_1_buf (10 Mhz)
                  RS485_RX_4 -------------------------->  |detect H-to-L| --->      (Ext TTE trigger, Activ Low)

            <--------RS485_TX[5]-----------------< ADC clk_2mhz_tte
            <--------RS485_TX[6]-----------------< CHOPPER ADC OUT
            <--------RS485_TX[7]-----------------< Interlock Signal OUT

                  Board is Master if programmed by IPMC
*/
    
    IBUFGDS #(
        .DIFF_TERM("FALSE"),       // Differential Termination
        .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("DEFAULT")
    ) IBUFGDS_100_osc (
        .O(te0741_clk_100_i),
        .I(te0741_clk_100_p),  
        .IB(te0741_clk_100_n) // Diff_n buffer input (connect directly to top-level port)
    );

    wire atca_10_clk, clk_10_rtm,  clk_10_te741, clk_50_te741_i;

    wire rtm_r1_buf;
    BUFG bufg_rtm_r1 (.O(rtm_r1_buf), .I(rtm_r1_i));
    external_clock external_rtm_clock_10_inst (
        .clk_10_ext(rtm_r1_buf),          // i
        .reset(!te0741_clk_100_locked_i), // i     // atca_master ?   
        .mmcm_locked(rtm_clk10_locked_i), // o
        .clk_10(clk_10_rtm)               // o 10 MHz
    );

    //-- Cloclk generated from the TE0741 Kintex Module
    internal_clock tr0741_clock_inst (
        .clk_100_int(te0741_clk_100_i), // i
        .reset(1'b0),               // i
        .mmcm_locked(te0741_clk_100_locked_i), // o
        .clk_5(),            // o
        .clk_10(clk_10_te741),          // o  - alternative clock 10 MHz when there is no RTM clk
        .clk_50(clk_50_te741_i),        // o
        .clk_200(clk_200_i)             // o   for IDELAYCTRLADC
    );

    //--   10 Mhz Clock Select RTM / INTERNAL from TE041 module
    BUFGMUX_CTRL bufgmux_10M_inst (
      .O(clk_10_atca_drv),      // 1-bit output: Clock output
      .I0(clk_10_te741),        // 1-bit input: Clock input (S=0) // Fall back to internal clock
      .I1(clk_10_rtm),          // 1-bit input: Clock input (S=1)
      .S(rtm_clk10_locked_i)    // 1-bit input: Clock select
    );
    
    wire clk_50_atca_i;
    atca_clock atca_clock_inst (
        .clk_10_atca(clk_10_atca_recv),     // i
        .reset(!te0741_clk_100_locked_i),   // i
        .mmcm_locked(atca_clk_locked_i),    // o
        .clk_50(clk_50_atca_i)              // o
    );
   
    //BUFG bufg_data_clk50 (.O(adc_data_clk), .I(clk_50_te741_i));
   
    //--   50 Mhz Clock Select: ATCA / INTERNAL from TE041 module
    BUFGMUX_CTRL bufgmux_50M_inst (
      .O(adc_data_clk),
      .I0(clk_50_te741_i),      // i : Clock input (S=0) //  Master falls back to internal clock
      .I1(clk_50_atca_i),       // i -  Clock input (S=1)
      .S(atca_clk_locked_i)     // i -  Clock select
    );
 
    system_clocks system_clocks_inst (
        .adc_data_clk(adc_data_clk),  // i
        .adcs_word_sync_n(adc_word_sync_n_i),   // o
        .adcs_start_conv_n(adc_start_conv_n_i), // o
        .adc_sreg_rst_n(),
        .clk_50_cnt(clk_50_cnt_i), // [4:0] o
        .clk_2_cnt(clk_2_cnt_i),   // [31:0] o
        .clk_2mhz_tte(clk_2mhz_tte_i) // o
    );

    chop_gen chop_gen_inst (
        .adc_data_clk(adc_data_clk), // i
        .adc_word_sync_n(adc_word_sync_n_i),
        //         .reset_n(commandREG[STREAME]),  TODO : sync chop...
        .chop_en(control_reg_i[`CHOP_ON_BIT]), // i
        .max_count(chopp_period_i[31:16]), // i
        .change_count(chopp_period_i[15:00]), // i for duty cycle
        .hold_samples(control_reg_i[5:0]),        
        .chop_o(adc_chop_phase_i),  // o
        .chop_dly_o(adc_chop_phase_dly_i),  // o
        .data_hold_o(adc_data_hold_i)  // o
    );

    reg [25:0] te0741_clk_cnt_r;
    reg te0741_1hz_r;
    always @(posedge te0741_clk_100_i)  //  Create a Clock Heartbeat 1 Hz
        if (te0741_clk_cnt_r == 26'd49_999_999) begin
                te0741_clk_cnt_r <= 0;
                te0741_1hz_r <= !te0741_1hz_r;
            end
         else
            te0741_clk_cnt_r <= te0741_clk_cnt_r + 1;

    assign te0741_led1 = te0741_1hz_r;  // Red. Always 1Hz
    assign te0741_led2 = (rtm_clk10_locked_i)? clk_2_cnt_i[20]: clk_2_cnt_i[18]; // Green LED 1'b1=ON

    //----  Front Panel LED. OFF on not PCIe Link, blinking otherwise, Fast = acquisition
    
    reg led_1Hz_r;
    reg [31:0] count_clk_50 = 0;
    //assign atca_led_d7 = led_1Hz_r;
     
    always @(posedge adc_data_clk or negedge sys_rst_n_c)
        if (!sys_rst_n_c) 
           count_clk_50 <= 0;
        else         
            if (count_clk_50 == 32'd24_999_999) begin
                count_clk_50 <= 0;
                led_1Hz_r <= !led_1Hz_r;
            end
            else
                count_clk_50 <= count_clk_50 + 1;          
    
    reg led_d7_r;
    assign atca_led_d7 = led_d7_r;
    always @(user_lnk_up, clk_2_cnt_i, te0741_1hz_r, te0741_clk_cnt_r, acq_on_r, led_1Hz_r)
      if (user_lnk_up)
          if (acq_on_r)
              led_d7_r = clk_2_cnt_i[18];
          else
              led_d7_r = te0741_1hz_r; // led_1Hz_r;  
      else
         led_d7_r = te0741_clk_cnt_r[22]; // Very fast, No PCIe link 
    //-----------------------------I/O BUFFERS------------------------//

    wire interlock_f, interlock_df;

    assign atca_hard_trig_d = rtm_r4_i;
    //assign hard_trigger_rcv = hard_trigger_drv ; //

    OBUF obuf_atca_clk1a_d_inst (.O(atca_1a_d), .I(clk_10_atca_drv));
    //OBUF obuf_atca_clk1a_d_inst (.O(atca_1a_d), .I(clk_200_i));   // Just for testing
    
    OBUF obuf_atca_clk1b_d_inst (.O(atca_1b_d), .I(atca_hard_trig_d));
//    OBUF obuf_atca_clk1b_d_inst (.O(atca_1b_d), .I(clk_5_te741));  // Just for testing

    IBUF u_rtm_r1 (.O(rtm_r1_i), .I(rtm_io_r1) );
    IBUF u_rtm_r4 (.O(rtm_r4_i), .I(rtm_io_r4) );
    IBUF u_rtm_r8 (.O(rtm_r8_i), .I(rtm_io_r8) );

    OBUF obuf_adc_rst_inst (.O(adcs_reset), .I(1'b1));
    OBUF obuf_chop_inst    (.O(adcs_power_down), .I(adc_chop_phase_i));
    OBUFDS #( .IOSTANDARD("DEFAULT"), .SLEW("SLOW")) 
        obufds_start_conv (.O(adcs_start_conv_p), .OB(adcs_start_conv_n), .I(adc_start_conv_n_i));

    wire clk_2mhz_tte_out = (acq_on_r)? clk_2mhz_tte_i :  1'b0;
    OBUF #( .DRIVE(16),  // Specify the output drive strength
        .IOSTANDARD("DEFAULT"), .SLEW("SLOW"))
    obuf_rtm_d5_inst (
        .O(rtm_io_d5),
        .I(clk_2mhz_tte_out)  // 2Mhz, 50% duty cycle
    );
    wire adc_chop_phase_out = (acq_on_r)? adc_chop_phase_i:  1'b0;
    wire interQLck_out = (control_reg_i[`ILCK_QF_OUT_EN_BIT])? interlock_df : adc_chop_phase_out;
    OBUF #( .DRIVE(16),
        .IOSTANDARD("DEFAULT"),
        .SLEW("SLOW")   )
    obuf_rtm_d6_inst (
        .O(rtm_io_d6),
        //.I(adc_chop_phase_out)
        .I(interQLck_out)
    );
    wire interLck_out = (control_reg_i[`ILCK_F_OUT_EN_BIT])? interlock_f : 0;
    OBUF obuf_ilck_inst (
        .O(rtm_io_d7),
        .I(interLck_out)
    );
    // End ----------------------------I/O BUFFERS------------------------//

    /* Synch signal with PCIe clk */
    always @(posedge axi_aclk) begin
//        user_reset_q  <= user_reset;
//        user_lnk_up_q <= user_lnk_up;
        acq_on_q <= acq_on_r;
    end

    reg [1:0] soft_trig_dly;
    reg [1:0] hard_trig_dly;

// ---------------- Trigger generation -------------------------------//
    always @(posedge adc_data_clk or negedge control_reg_i[`ACQE_BIT]) begin
        if (!control_reg_i[`ACQE_BIT])
        begin
            acq_on_r <= #TCQ  1'b0;
            soft_trig_dly <=  #TCQ 2'b11;
            hard_trig_dly <=  #TCQ 2'b00;
        end
        else
        begin
            soft_trig_dly <=  #TCQ  {soft_trig_dly[0], control_reg_i[`STRG_BIT]}; // delay pipe
            hard_trig_dly <=  #TCQ  {hard_trig_dly[0], atca_hard_trig_rcv};         // delay pipe

            if ((soft_trig_dly == 2'b01) || (hard_trig_dly == 2'b10)) // detect rising / falling edge
                acq_on_r <= #TCQ  1'b1;
        end
    end

  /***  XDMA Core Top Level Wrapper   */
  xdma_id0032 xdma_id0032_i
     (
      //---------------------------------------------------------------------------------------//
      //  PCI Express (pci_exp) Interface                                                      //
      //---------------------------------------------------------------------------------------//
      .sys_rst_n       ( sys_rst_n_c ), // i
      .sys_clk         ( sys_clk ),     // i

      // Tx
      .pci_exp_txn     ( pci_exp_txn ),
      .pci_exp_txp     ( pci_exp_txp ),

      // Rx
      .pci_exp_rxn     ( pci_exp_rxn ),
      .pci_exp_rxp     ( pci_exp_rxp ),

      //---------------------------------------------------------------------------------------//
      //  AXI streaming ports                                                                  //
      //---------------------------------------------------------------------------------------//
      .s_axis_c2h_tdata_0(s_axis_c2h_tdata_0),
      .s_axis_c2h_tlast_0(s_axis_c2h_tlast_0),
      .s_axis_c2h_tvalid_0(s_axis_c2h_tvalid_0),
      .s_axis_c2h_tready_0(s_axis_c2h_tready_0),
      .s_axis_c2h_tkeep_0(s_axis_c2h_tkeep_0),
      // Port Not used
      .m_axis_h2c_tdata_0(m_axis_h2c_tdata_0),
      .m_axis_h2c_tlast_0(m_axis_h2c_tlast_0),
      .m_axis_h2c_tvalid_0(m_axis_h2c_tvalid_0),
      .m_axis_h2c_tready_0(m_axis_h2c_tready_0),
      .m_axis_h2c_tkeep_0(m_axis_h2c_tkeep_0),

      .s_axis_c2h_tdata_1(s_axis_c2h_tdata_1),
      .s_axis_c2h_tlast_1(s_axis_c2h_tlast_1),
      .s_axis_c2h_tvalid_1(s_axis_c2h_tvalid_1),
      .s_axis_c2h_tready_1(s_axis_c2h_tready_1),
      .s_axis_c2h_tkeep_1(s_axis_c2h_tkeep_1),

      //---------------------------------------------------------------------------------------//
      //  AXI Master LITE interface                                                                   //
      //---------------------------------------------------------------------------------------//
      //-- AXI Master Write Address Channel
      .m_axil_awaddr    (m_axil_awaddr),
      .m_axil_awprot    (m_axil_awprot),
      .m_axil_awvalid   (m_axil_awvalid),
      .m_axil_awready   (m_axil_awready),
      //-- AXI Master Write Data Channel
      .m_axil_wdata     (m_axil_wdata),
      .m_axil_wstrb     (m_axil_wstrb),
      .m_axil_wvalid    (m_axil_wvalid),
      .m_axil_wready    (m_axil_wready),
      //-- AXI Master Write Response Channel
      .m_axil_bvalid    (m_axil_bvalid),
      .m_axil_bresp     (m_axil_bresp),
      .m_axil_bready    (m_axil_bready),
      //-- AXI Master Read Address Channel
      .m_axil_araddr    (m_axil_araddr),
      .m_axil_arprot    (m_axil_arprot),
      .m_axil_arvalid   (m_axil_arvalid),
      .m_axil_arready   (m_axil_arready),
      .m_axil_rdata     (m_axil_rdata),
      //-- AXI Master Read Data Channel
      .m_axil_rresp     (m_axil_rresp),
      .m_axil_rvalid    (m_axil_rvalid),
      .m_axil_rready    (m_axil_rready),

      .usr_irq_req       (usr_irq_req),
      .usr_irq_ack       (usr_irq_ack),
      .msi_enable        (msi_enable),
      .msi_vector_width  (msi_vector_width),

      //-- AXI Global
      .axi_aclk        ( axi_aclk ),        // o
      .axi_aresetn     ( axi_aresetn ),     // o

      .user_lnk_up     ( user_lnk_up )      // o
    );

//----            IPFN code         
// BAR0 register Space 10-bit address, 32-bit Data
    wire [C_M_AXI_DATA_WIDTH-1:0] tmp101_0_1;
    wire [C_M_AXI_DATA_WIDTH-1 :0] mcp9808_0_1, mcp9808_2_3, mcp9808_4_5, mcp9808_6_7;

    wire  dev_hard_reset_i;
    
    shapi_regs # (
        .C_S_AXI_DATA_WIDTH(C_S_AXI_DATA_WIDTH),
        .C_S_AXI_ADDR_WIDTH(C_S_AXI_ADDR_WIDTH)
    ) shapi_regs_inst (
           .S_AXI_ACLK(axi_aclk),
          .S_AXI_ARESETN(axi_aresetn),
          .S_AXI_AWADDR(m_axil_awaddr[C_S_AXI_ADDR_WIDTH-1:0]),
          //.S_AXI_AWPROT(s_axil_awprot), // Not used
          .S_AXI_AWVALID(m_axil_awvalid),
          .S_AXI_AWREADY(m_axil_awready),
          .S_AXI_WDATA(m_axil_wdata),
          .S_AXI_WSTRB(m_axil_wstrb),
          .S_AXI_WVALID(m_axil_wvalid),
          .S_AXI_WREADY(m_axil_wready),
          .S_AXI_BRESP(m_axil_bresp),
          .S_AXI_BVALID(m_axil_bvalid),
          .S_AXI_BREADY(m_axil_bready),
          .S_AXI_ARADDR(m_axil_araddr[9:0]),
          //.S_AXI_ARPROT(s_axil_arprot), // Not used
          .S_AXI_ARVALID(m_axil_arvalid),
          .S_AXI_ARREADY(m_axil_arready),
          .S_AXI_RDATA(m_axil_rdata),
          .S_AXI_RRESP(m_axil_rresp),
          .S_AXI_RVALID(m_axil_rvalid),
          .S_AXI_RREADY(m_axil_rready),
          
           .dev_hard_reset(dev_hard_reset_i),
           .status_reg(status_reg_i),   // i
           .control_reg(control_reg_i), // o
           .eo_offset(eo_offset_i),  // o
           .wo_offset(wo_offset_i),  // o
           .ilck_param(ilck_param_i),  // o
           .chopp_period(chopp_period_i),  // o
           .channel_mask(channel_mask_i),  // o
           .i2C_reg0(i2C_reg0),       // i
           .tmp101_0_1(tmp101_0_1),   // [31:0] i
           .mcp9808_0_1(mcp9808_0_1), //i
           .mcp9808_2_3(mcp9808_2_3), //i
           .mcp9808_4_5(mcp9808_4_5), //i
           .mcp9808_6_7(mcp9808_6_7) //i
           //.i2C_reg1(i2C_reg1)        // o

    );
 // ---            ADC data acquisition and packeting ---------
  adc_data_producer #(
    .C_S_AXI_DATA_WIDTH(C_M_AXI_DATA_WIDTH),
    .C_STREAM_DATA_WIDTH(C_DATA_WIDTH)
  ) adc_data_producer_inst (
      .axi_aclk(axi_aclk),
      .axi_aresetn(axi_aresetn),

      .adc_data_clk(adc_data_clk),  // i
      .adc_clk_cnt(clk_50_cnt_i),  // i [4:0]

      .adc_clk_p(adc_clk_p),
      .adc_clk_n(adc_clk_n),
      .adc_data_p(adc_data_p),
      .adc_data_n(adc_data_n),
      .word_sync_n(adc_word_sync_n_i),

      .adc_chop_phase_dly(adc_chop_phase_dly_i),
      .adc_data_hold(adc_data_hold_i),

      .eo_offset(eo_offset_i),
      .wo_offset(wo_offset_i),
      .ilck_param(ilck_param_i),

      .control_reg(control_reg_i),
      .channel_mask(channel_mask_i),
      .acq_on(acq_on_r),
      .fifos_status(fifos_status_i),  // o [14:10]
      .interlock_f(interlock_f),
      .interlock_df(interlock_df),

      // AXI streaming ports
      .m_axis_tdata_0(s_axis_c2h_tdata_0),
      .m_axis_tlast_0(s_axis_c2h_tlast_0),
      .m_axis_tvalid_0(s_axis_c2h_tvalid_0),
      .m_axis_tready_0(s_axis_c2h_tready_0),
      .m_axis_tkeep_0(s_axis_c2h_tkeep_0),

      .m_axis_tdata_1(s_axis_c2h_tdata_1),
      .m_axis_tlast_1(s_axis_c2h_tlast_1),
      .m_axis_tvalid_1(s_axis_c2h_tvalid_1),
      .m_axis_tkeep_1(s_axis_c2h_tkeep_1),
      .m_axis_tready_1(s_axis_c2h_tready_1)
  );

    //wire ipmcReg_w = {rtm_r8_i, atca_master, msi_enable, idelay_rdy_w,
    //  1'b0, atca_clk_locked_i, te0741_clk_100_locked_i, rtm_clk10_locked_i};

    reg [7:0] ipmcReg20_r;
    always @(posedge axi_aclk)
        ipmcReg20_r <= {rtm_r8_i, atca_master, msi_enable, idelay_rdy_w,
                1'b0, atca_clk_locked_i, te0741_clk_100_locked_i, rtm_clk10_locked_i};

// ----   I2C Slave from opencores.org  
    i2cSlave u_i2cSlave(
      .clk(axi_aclk),
      .rst(!axi_aresetn),
      .sda(fpga_sda),
      .scl(fpga_scl),
      .outReg0(i2C_reg0[7:0]),   // o  bit 4: atca_master [3:0] i2c addr : 0x00 . slot_id
      .outReg1(i2C_reg0[15:8]),  // ipmc DEV_FW_PATCH
      .outReg2(i2C_reg0[23:16]), // ipmc DEV_FW_MINOR
      .outReg3(i2C_reg0[31:24]), // ipmc DEV_FW_MAJOR

  .tmp101_0(tmp101_0_1[15:0]), //o  addr: 0x04
  .tmp101_1(tmp101_0_1[31:16]),

  .mcp9808_0(mcp9808_0_1[15:0]), //o [15:0]  addr: 0x08
  .mcp9808_1(mcp9808_0_1[31:16]),
  .mcp9808_2(mcp9808_2_3[15:0]), //o  addr: 0x0C
  .mcp9808_3(mcp9808_2_3[31:16]),
  .mcp9808_4(mcp9808_4_5[15:0]), //o  addr: 0x10
  .mcp9808_5(mcp9808_4_5[31:16]),
  .mcp9808_6(mcp9808_6_7[15:0]), //o  addr: 0x14
  .mcp9808_7(mcp9808_6_7[31:16]),

  .inReg20(ipmcReg20_r), // i2C_reg1[7:0] i [7:0]  i2c addr: 0x20
  .inReg21(`I2C_MAGIC),  // i2C_reg1[15:8]
  .inReg22(8'h02),
  .inReg23({dev_hard_reset_i, 7'h0}) // i2C_reg1[31:24]

 );

endmodule
// vim: set ts=8 sw=4 tw=0 et :
