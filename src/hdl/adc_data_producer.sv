/* vim: set ts=4 sw=4 tw=0 et : */
// sw is shiftwidth, sts is softtabstop, sr is shiftround, and noet is noexpandtab 
//set ai means set autoindent
//////////////////////////////////////////////////////////////////////////////////
// Company: IPFN-IST
// Engineer: BBC
//
// Create Date: 05/02/2021 07:21:01 PM
// Design Name:
// Module Name: adc_data_producer
// Project Name:
// Target Devices: kintex-7
// Tool Versions:  Vivado 2022.1
// Description: Creates data packages for xdma engine in 32 /16 data format
// also computes Intergal of adc signal, the "F" function algorithm an
// derivative.
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:wo_offset_r
// Copyright 2015 - 2022 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date  2017-11-09
//
// Licensed under the EUPL, Version 1.2 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//////////////////////////////////////////////////////////////////////////////////
`timescale 1ns / 1ps
`include "atca_mimo_v2_config.vh"
`include "control_word_bits.vh"

module adc_data_producer #(
    parameter PKT_SAMPLES_WIDTH = 12,
    parameter DMA_FIFO_DEPTH = 32768,   // Max Value 32768

    //    parameter RT_PKT_DECIM = 200,
    parameter C_S_AXI_DATA_WIDTH    = 32,    // AXI Lite Master  data width
    parameter C_STREAM_DATA_WIDTH    = 128,         // Streaming DMA C2H  data width
    parameter KEEP_WIDTH = C_STREAM_DATA_WIDTH / 8,              // TSTRB width

    parameter ADC_DATA_WIDTH   = `ADC_DATA_WIDTH,
    parameter FLOAT_WIDTH      = `FLOAT_WIDTH,

    parameter N_ADC_CHANNELS   = `N_ADC_MAX_CHANNELS,
    parameter N_INT_CHANNELS   = `N_INT_CHANNELS,
    parameter N_ILOCK_PARAMS   =  N_INT_CHANNELS + 2,

    parameter EO_VECT_WIDTH   = ADC_DATA_WIDTH * N_ADC_CHANNELS,
    parameter WO_WIDTH        = FLOAT_WIDTH,
    parameter WO_VECT_WIDTH   = WO_WIDTH  * N_ADC_CHANNELS,
    parameter ILCK_WIDTH      = FLOAT_WIDTH,
    parameter ILCK_VECT_WIDTH = ILCK_WIDTH    * N_ILOCK_PARAMS,
    parameter TCQ        = 1
)(
    input axi_aclk,
    input axi_aresetn,

    // ADC data clk (50Mhz)
    input adc_data_clk,
    input [4:0] adc_clk_cnt, // counts 0->24 in each adc period for channel mux

    //ADC module input data/clk
    input [N_ADC_CHANNELS-1:0] adc_clk_p,
    input [N_ADC_CHANNELS-1:0] adc_clk_n,
    input [N_ADC_CHANNELS-1:0] adc_data_p,
    input [N_ADC_CHANNELS-1:0] adc_data_n,

    input word_sync_n,

    input adc_chop_phase_dly,

    input adc_data_hold,   // Suppress  data samples and hold for chopping spike removal

    input [EO_VECT_WIDTH-1:0] eo_offset,
    input [WO_VECT_WIDTH-1:0] wo_offset,
    input [ILCK_VECT_WIDTH-1:0] ilck_param,

    input [C_S_AXI_DATA_WIDTH-1 :0]  control_reg,
    output [14:10] fifos_status,  // Status of the FIFO and Accumulators

    input [N_ADC_CHANNELS-1:0]  channel_mask,

// Control interface
    input  acq_on,       // start on trigger
    output interlock_f,  // to RTM output pin
    output interlock_df, // to RTM output pin

  // Two AXI C2H xdma streaming ports
    output m_axis_tvalid_0,
    input  m_axis_tready_0,
    output [C_STREAM_DATA_WIDTH-1:0] m_axis_tdata_0,
    output [KEEP_WIDTH-1:0]   m_axis_tkeep_0,
    output m_axis_tlast_0,

    output m_axis_tvalid_1,
    input  m_axis_tready_1,
    output [C_STREAM_DATA_WIDTH-1:0] m_axis_tdata_1,
    output [KEEP_WIDTH-1:0]   m_axis_tkeep_1,
    output m_axis_tlast_1
);

    /***    ADC data/clock internal delays on FPGA. May need to be tuned. First element in array is Channel 0 **/
    //localparam integer IDELAY_IVAL_ARRAY [31:0] = {10, 10, 10, 28, 10, 10, 10, 10,
    //localparam integer IDELAY_IVAL_ARRAY [31:0] = {30, 30, 30, 30, 30, 30, 30, 30,
        //10, 10, 10, 10, 10, 10, 10, 10,
    localparam integer IDELAY_IVAL_ARRAY[N_ADC_CHANNELS] = '{18, 18, 18, 31, 12, 13, 13, 13,
        6, 7, 8, 13, 13, 6, 6, 7,
        6, 10, 6, 13,  6, 6, 14, 13,
        7, 6, 5, 13, 13, 13, 22, 22};

    /*  Choose here what are the 8 channels to use in the Interlock calculus */
    localparam integer INTEGER_CHANNEL_ARRAY[N_INT_CHANNELS] = '{0,2,4,6,8,10,    12,14};

/* Version 1 IPP W7-X Interlock Input ADC Channels channel count starting at 1
 .data_in_ch1(c_data[1]),  // 1-QXD31CE101 (compensatio n coil 1), in url coda-station count channel number: 0
 .data_in_ch2(c_data[3]),  // 1-QXD31CE201 (compensation coil 2), in url coda-station count channel number: 2
 .data_in_ch3(c_data[5]),  // 1-QXD31CE301 (compensation coil 3), in url coda-station count channel number: 4
 .data_in_ch4(c_data[7]),  // 1-QXD31CE401 (compensation coil 4), in url coda-station count channel number: 6
 .data_in_ch5(c_data[9]),  // 1-QXD31CE001 (diamagnetic loop), in url coda-station count channel number: 8
 .data_in_ch6(c_data[11]), // 1-QXR40CE001 -  Rogowski coil, in url coda-station count channel number: 10
*/
    localparam LONG_INTEGER_WIDTH = 64;
    localparam FRACTIONAL_WIDTH   = 16;
    localparam INTEGRAL_WIDTH     = LONG_INTEGER_WIDTH + FRACTIONAL_WIDTH;
    localparam WO_EXTEND_BITS     = INTEGRAL_WIDTH - FRACTIONAL_WIDTH - 20;
    localparam FLOAT_SIGN_BIT     = FLOAT_WIDTH - 1;
    localparam DFLOAT_WIDTH       = 2 * FLOAT_WIDTH;

/*********** Function Declarations ***************/
`include "adc_data_functions.vh"

    wire acq_en = control_reg[`ACQE_BIT];
    reg [C_S_AXI_DATA_WIDTH-1:0] channel_mask_aclk;

    reg data_32bit;
    always @ (posedge adc_data_clk) begin
        data_32bit         <= control_reg[`DMA_DATA_32_BIT];
        channel_mask_aclk  <= channel_mask;
    end
    /*Interlock signal is (invert of) sign bit of result value*/
    reg    interlock_F_r, interlock_dF_r;
    assign interlock_f = interlock_F_r; // interlock must be a synchronous regitered signal to avoid spikes
    assign interlock_df = interlock_dF_r;

    // (* keep = "true" *) 
    reg [FLOAT_WIDTH-1:0] float_val_0, float_val_1;
    //(* keep = "true" *) 
    reg [FLOAT_WIDTH-1:0] float_val_2, float_val_3;
    //(* keep = "true" *) reg [FLOAT_WIDTH-1:0] float_val_4, float_val_5; // Debug variables

    reg [63:0] cnt_sample_c2h0_r;
    reg [15:0] cnt_pckt_c2h0_r;
    reg [9:0] cnt_decim_c2h_1_r;
    //reg [63:0] cnt_time_c2h1_r;
    reg [63:0] cnt_pckt_c2h1_r;
    //reg [(PKT_SAMPLES_WIDTH-1):0] cnt_sample_c2h1_r ;

    //(* keep = "true" *)
    wire  fifo_ready_c2h_0, fifo_ready_c2h_1;

 //----------------------------- ADC data array ------------------------//
    wire [ADC_DATA_WIDTH-1 : 0] adc_par_data[0: N_ADC_CHANNELS-1];
    //wire [15:0] wr_data_count, rd_data_count;
    wire [15:0] wr_data_count;
    wire [ADC_DATA_WIDTH-1 : 0] adc_18_data[0: N_ADC_CHANNELS-1];
    wire [15:0] adc_16_data[0: N_ADC_CHANNELS-1];
    wire [15:0] adc_16_data_c31 = adc_16_data[31];  // Last of input channels

    genvar k;
    generate
        for (k = 0; k < N_ADC_CHANNELS; k = k + 1)
        begin:
            //$display("Delay %d = %d", k, IDELAY_IVAL_ARRAY[k]);
            adc_chan_gen adc_chan_ddr #
            (.IDELAY_VAL(IDELAY_IVAL_ARRAY[k]))
            channel_inst (
                .adc_data_clk(adc_data_clk),
                .word_sync_n(word_sync_n),
                .serial_clock_p(adc_clk_p[k]),
                .serial_clock_n(adc_clk_n[k]),
                .serial_data_p(adc_data_p[k]),
                .serial_data_n(adc_data_n[k]),
                .parallel_data(adc_par_data[k])   // o [17:0]
            );

            assign adc_18_data[k] = (channel_mask_aclk[k])? adc_par_data[k] : {(ADC_DATA_WIDTH+2){1'b0}};
            assign adc_16_data[k] = (channel_mask_aclk[k])? adc_16_msb_f(adc_par_data[k]):
                                                            16'h0000;
        end
    endgenerate

    wire signed [ADC_DATA_WIDTH+1 : 0] adc_eo_data[0: N_ADC_CHANNELS-1];
    wire signed [ADC_DATA_WIDTH+1 : 0] adc_dechopp[0: N_ADC_CHANNELS-1];

    reg  signed [ADC_DATA_WIDTH+1 : 0] last_adc_dechopp[0:N_ADC_CHANNELS-1]; // for data hold in Integral calc

    generate
    for (k = 0; k < N_ADC_CHANNELS; k = k+1)
        begin: adc_dechop_gen
            assign adc_eo_data[k] =  adc_eorec_f(adc_18_data[k],
                eo_offset[(ADC_DATA_WIDTH * (k+1) - 1) : (ADC_DATA_WIDTH * k)]);
        
            assign adc_dechopp[k] = adc_dechop_f(adc_eo_data[k], adc_chop_phase_dly);
        end
    endgenerate


    reg  data_vld_c2h_0_r = 1'b0;
    wire big_endian = control_reg[`ENDIAN_DMA_BIT];
    reg [C_STREAM_DATA_WIDTH-1:0] data_c2h_0_end_r, data_c2h_1_end_r; //registered. Used to correct BIG/Little Endian Data

    //wire [C_DATA_WIDTH-1 : 0] data_c2h_1;
    reg  [C_STREAM_DATA_WIDTH-1 : 0] data_c2h_0_r;
    wire fifo_prog_full_c2h_0, fifo_prog_full_c2h1_i;
    reg [KEEP_WIDTH-1:0] s_axis_tkeep_c = {KEEP_WIDTH{1'b1}};
    reg [KEEP_WIDTH-1:0] s_axis_tstrb_c = {KEEP_WIDTH{1'b0}};

    reg s_axis_tlast_r, s_axis_tlast_c2h1_r = 0;
    reg [4:0] status_data_r = 5'b00000;
    assign fifos_status = status_data_r;

    wire [3:0] accum_overflow;        // Accummulator Overflow / Input Overflow conditions

    reg [4:0] int_channel_idx_r = 5'd0;

    wire [DFLOAT_WIDTH-1:0] integral_double_ar[0: N_INT_CHANNELS-1];
    wire [FLOAT_WIDTH-1:0] integral_single_ar[0: N_ADC_CHANNELS-1];
 
    reg [FLOAT_WIDTH-1:0] integral_single_decim;
    wire [N_ADC_CHANNELS-1:0] singleInt64valid;
/****    Fifo input size is 32k * 8/16 Bytes. Here multiplexing 32 bit data channels. (64/128 Bytes/sample   */
    generate
    if (C_STREAM_DATA_WIDTH == 128)
        always @ (posedge adc_data_clk or negedge acq_on)
          if (!acq_on) begin
              data_vld_c2h_0_r   <= #TCQ 'h00; // check this async
              data_c2h_0_end_r   <= #TCQ 'h00;
              cnt_sample_c2h0_r  <= #TCQ 'h00;
              status_data_r      <= #TCQ 'h00;
              cnt_pckt_c2h0_r    <= #TCQ 'h00;//{PKT_SAMPLES_WIDTH {1'b0}}; // Counts sample num in each packet
              s_axis_tlast_r     <= #TCQ 0;
              int_channel_idx_r  <= #TCQ 5'd0;
          end
          else begin
              data_c2h_0_end_r <= #TCQ big_endian_128_f(data_c2h_0_r, big_endian);
              // | - reduction operator
              if(|accum_overflow != 1'b0)        //Check Accumulators Overflow / Input Overflow conditions
                  status_data_r[3:0]  <= #TCQ (status_data_r[3:0] | accum_overflow); // Store any condition

              if(fifo_prog_full_c2h_0)
                  status_data_r[4]  <= #TCQ 1'b1;  // Latch FIFO full condition during acquisition
              if(singleInt64valid[0])
                    integral_single_decim <= integral_single_ar[int_channel_idx_r];                    
              case (adc_clk_cnt) // one cycle per sample. This case may start at clk_100_cnt != 0!
                  5'h01: begin

                        /******  ADC data 16/32 bit MUX ******/
                        if (data_32bit) begin // Send 4 channels per word + count data
                            data_c2h_0_r <= #TCQ {adc_18_data[3], 6'h0, cnt_sample_c2h0_r[31:24],
                                adc_18_data[2], 6'h0, cnt_sample_c2h0_r[23:16],
                                adc_18_data[1], 6'h0, cnt_sample_c2h0_r[15:8],
                                adc_18_data[0], 6'h0, cnt_sample_c2h0_r[7:0]};
                        end
                        else begin      // Send 8 channels per word
                            data_c2h_0_r <= #TCQ {adc_16_data[7], adc_16_data[6], adc_16_data[5], adc_16_data[4],
                                adc_16_data[3], adc_16_data[2], adc_16_data[1], adc_16_data[0]};
                        end
                  end
                  5'h02: begin
                        if( fifo_ready_c2h_0 && !fifo_prog_full_c2h_0)
                              data_vld_c2h_0_r      <= #TCQ 1'b1; //Start fifo writing samples

                        if (data_32bit) begin
                            data_c2h_0_r <= #TCQ {adc_18_data[7], 6'h0, cnt_sample_c2h0_r[63:56],
                                adc_18_data[6], 6'h0, cnt_sample_c2h0_r[55:48],
                                adc_18_data[5], 6'h0, cnt_sample_c2h0_r[47:40],
                                adc_18_data[4], 6'h0, cnt_sample_c2h0_r[39:32]};
                        end
                        else begin
                            data_c2h_0_r <= #TCQ {adc_16_data[15], adc_16_data[14], adc_16_data[13],
                                adc_16_data[12], adc_16_data[11], adc_16_data[10], adc_16_data[9], adc_16_data[8]};
                        end
                  end
                  5'h03: begin
                        if (data_32bit) begin
                            data_c2h_0_r <= #TCQ {adc_18_data[11], 6'h00, float_val_0[31:24],
                                adc_18_data[10], 6'h00, float_val_0[23:16],
                                adc_18_data[9],  6'h00, float_val_0[15:8],
                                adc_18_data[8],  6'h00, float_val_0[7:0]};
                        end
                        else begin
                            data_c2h_0_r <= #TCQ {adc_16_data[23], adc_16_data[22], adc_16_data[21], adc_16_data[20],
                                adc_16_data[19], adc_16_data[18], adc_16_data[17], adc_16_data[16]};
                        end
                  end
                  5'h04: begin
                        if (data_32bit) begin
                            data_c2h_0_r <= #TCQ {adc_18_data[15], 6'h00, float_val_1[31:24],
                                adc_18_data[14], 6'h00, float_val_1[23:16],
                                adc_18_data[13], 6'h00, float_val_1[15:8],
                                adc_18_data[12], 6'h00, float_val_1[7:0]};
                        end
                        else begin
                            data_c2h_0_r[63:0] <= #TCQ {adc_16_data[27], adc_16_data[26], adc_16_data[25], adc_16_data[24]};
                            if (control_reg[`CHOP_ON_BIT])
                                data_c2h_0_r[127:64] <= #TCQ {adc_16_data_c31[15:1],  adc_chop_phase_dly,
                                    adc_16_data[30], adc_16_data[29], adc_16_data[28]};
                            else
                                data_c2h_0_r[127:64] <= #TCQ {adc_16_data[31], adc_16_data[30], adc_16_data[29],
                                    adc_16_data[28]};
                        end
                  end
                  5'h05: begin
                      //       FIFO input data has 1-cycle latency, for endianess, so still input 16-bit data
                        if (data_32bit)
                            data_c2h_0_r <= #TCQ {adc_18_data[19], 6'h00, float_val_2[31:24],
                                adc_18_data[18], 6'h00, float_val_2[23:16],
                                adc_18_data[17], 6'h00, float_val_2[15:8],
                                adc_18_data[16], 6'h00, float_val_2[7:0]};
                        else
                            data_c2h_0_r <= #TCQ 'h00;
                  end
                5'h06: begin
                        if (data_32bit)
                            data_c2h_0_r <= #TCQ {adc_18_data[23], 6'h00, float_val_3[31:24],
                                adc_18_data[22], 6'h00, float_val_3[23:16],
                                adc_18_data[21], 6'h00, float_val_3[15:8],
                                adc_18_data[20], 6'h00, float_val_3[7:0]};
                        else    // If 16 bit-mode, end packet here
                            data_vld_c2h_0_r      <= #TCQ 1'b0;
                  end
               5'h07: begin
                        if (data_32bit) begin
                            data_c2h_0_r <= #TCQ {adc_18_data[27], 6'h0, integral_single_decim[31:24], 
                                adc_18_data[26], 6'h0, integral_single_decim[23:16],
                                adc_18_data[25], 6'h0, integral_single_decim[15:8], 
                                adc_18_data[24], 6'h0, integral_single_decim[7:0]};
                        end
                    end
                5'h08: begin
                        if (data_32bit) begin
                            data_c2h_0_r <= #TCQ {adc_18_data[31], 6'h0,interlock_dF_r, interlock_F_r,
                                accum_overflow, fifo_prog_full_c2h_0, adc_chop_phase_dly,
                                adc_18_data[30], 9'h000, int_channel_idx_r,
                                adc_18_data[29], 6'h0, wr_data_count[15:8],
                                adc_18_data[28], 6'h0, wr_data_count[7:0]};
                        end
                    end
                5'h09: begin
                    if(data_vld_c2h_0_r)  // only if fifo writing is ON
                        cnt_pckt_c2h0_r     <= #TCQ cnt_pckt_c2h0_r + 1;
                    if (&cnt_pckt_c2h0_r[9:0]) // 128kB packet (Does xdma really cares about it?)
                        s_axis_tlast_r      <= #TCQ 1'b1;
                    end
                 5'h0A: begin
                    data_vld_c2h_0_r  <= #TCQ 1'b0;
                    s_axis_tlast_r    <= #TCQ 1'b0;
                    cnt_sample_c2h0_r <= #TCQ cnt_sample_c2h0_r + 1;
                    int_channel_idx_r <= #TCQ int_channel_idx_r + 1;
                    
                    data_c2h_0_r <= #TCQ { 64'h00, 32'h00, integral_single_decim};
                end
                default: begin
                    data_vld_c2h_0_r <= #TCQ 1'b0;
                    s_axis_tlast_r   <= #TCQ 1'b0;
                end
            endcase
        end // always
    endgenerate  //128/64 bit

    wire m_axis_tready_downstream = (m_axis_tready_0 || !acq_en); // Flush data fifo on acq disabled

    wire [C_STREAM_DATA_WIDTH-1:0] inter_c2h_tdata;
    wire [C_STREAM_DATA_WIDTH/8-1:0] inter_c2h_tkeep;
    
    wire inter_c2h_tvalid, inter_c2h_tready, inter_c2h_tlast;

/*******
    wire user_rstn_sync;
    xpm_cdc_async_rst#(
    //Common module parameters
        .DEST_SYNC_FF(6),   //integer; range:2-10 .
        .INIT_SYNC_FF(0),    //integer; 0=disable simulation init values,1=enablesimulationinitvalues .
        .RST_ACTIVE_HIGH(0)  //integer;0= active low reset, 1= active high reset
    ) xpm_cdc_async_rst_inst(
        .src_arst(user_resetn),
        .dest_clk(adc_data_clk),
        .dest_arst(user_rstn_sync)
    );
*/

/***

DOWNSTREAM AXI FIFO
*  xpm_fifo_axis: AXI Stream FIFO
   // Xilinx Parameterized Macro, version 2019.2
---------------------------------------------------------------------------------------------------------------------+
// | USE_ADV_FEATURES     | String             | Default value = 1000.                                                   |
// |---------------------------------------------------------------------------------------------------------------------|
// | Enables almost_empty_axis, rd_data_count_axis, prog_empty_axis, almost_full_axis, wr_data_count_axis,               |
// | prog_full_axis sideband signals.                                                                                    |
// |                                                                                                                     |
// |   Setting USE_ADV_FEATURES[1] to 1 enables prog_full flag;    Default value of this bit is 0                        |
// |   Setting USE_ADV_FEATURES[2]  to 1 enables wr_data_count;     Default value of this bit is 0                       |
// |   Setting USE_ADV_FEATURES[3]  to 1 enables almost_full flag;  Default value of this bit is 0                       |
// |   Setting USE_ADV_FEATURES[9]  to 1 enables prog_empty flag;   Default value of this bit is 0                       |
// |   Setting USE_ADV_FEATURES[10] to 1 enables rd_data_count;     Default value of this bit is 0                       |
// |   Setting USE_ADV_FEATURES[11] to 1 enables almost_empty flag; Default value of this bit is 0
// 8 * 64kB data fifo , Prog full = 20 * 8 B
//
*/



   xpm_fifo_axis #(
      // .CDC_SYNC_STAGES(2),            // DECIMAL Range: 2 - 8. Default value = 2.
      .CLOCKING_MODE("common_clk"), // String
      .ECC_MODE("no_ecc"),            // String
      .FIFO_DEPTH(DMA_FIFO_DEPTH),              // DECIMAL 32768 (Max DEPTH* num of Bits= 4194304 bit)
      .FIFO_MEMORY_TYPE("auto"),      // String
      .PACKET_FIFO("false"),          // String
      //.PROG_EMPTY_THRESH(d32736),         // DECIMAL
      //.PROG_FULL_THRESH(32736),       // DECIMAL 8- 32763
      //.RD_DATA_COUNT_WIDTH(16),        // DECIMAL
      //.RELATED_CLOCKS(0),             // DECIMAL
      .SIM_ASSERT_CHK(0),             // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      .TDATA_WIDTH(C_STREAM_DATA_WIDTH),               // DECIMAL Defines the width of the TDATA port, s_axis_tdata and m_axis_tdata
      .TDEST_WIDTH(1),                // DECIMAL
      .TID_WIDTH(1),                  // DECIMAL
      .TUSER_WIDTH(1)                // DECIMAL
//      .USE_ADV_FEATURES("1000"),      // String
 //     .WR_DATA_COUNT_WIDTH(16)         // DECIMAL
   )
   xpm_fifo_axis_c2h0_i (
      .almost_empty_axis(),   // 1-bit output: Almost Empty : When asserted, this signal
                                               // indicates that only one more read can be performed before the
                                               // FIFO goes to empty.

      .almost_full_axis(),     // 1-bit output: Almost Full: When asserted, this signal
                                               // indicates that only one more write can be performed before
                                               // the FIFO is full.

      .dbiterr_axis(),             // 1-bit output: Double Bit Error- Indicates that the ECC
                                               // decoder detected a double-bit error and data in the FIFO core
                                               // is corrupted.

      .m_axis_tdata(m_axis_tdata_0),             // TDATA_WIDTH-bit output: TDATA: The primary payload that is
                                               // used to provide the data that is passing across the
                                               // interface. The width of the data payload is an integer number
                                               // of bytes.

      .m_axis_tdest(),             // TDEST_WIDTH-bit output: TDEST: Provides routing information
                                               // for the data stream.

      .m_axis_tid(),                 // TID_WIDTH-bit output: TID: The data stream identifier that
                                               // indicates different streams of data.

      .m_axis_tkeep(m_axis_tkeep_0),             // TDATA_WIDTH/8-bit output: TKEEP: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as part of the data stream. Associated bytes
                                               // that have the TKEEP byte qualifier deasserted are null bytes
                                               // and can be removed from the data stream. For a 64-bit DATA,
                                               // bit 0 corresponds to the least significant byte on DATA, and
                                               // bit 7 corresponds to the most significant byte. For example:
                                               // KEEP[0] = 1b, DATA[7:0] is not a NULL byte KEEP[7] = 0b,
                                               // DATA[63:56] is a NULL byte

      .m_axis_tlast(m_axis_tlast_0),             // 1-bit output: TLAST: Indicates the boundary of a packet.
      .m_axis_tstrb(),             // TDATA_WIDTH/8-bit output: TSTRB: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as a data byte or a position byte. For a 64-bit
                                               // DATA, bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 7 corresponds to the most significant byte. For
                                               // example: STROBE[0] = 1b, DATA[7:0] is valid STROBE[7] = 0b,
                                               // DATA[63:56] is not valid

      .m_axis_tuser(),             // TUSER_WIDTH-bit output: TUSER: The user-defined sideband
                                               // information that can be transmitted alongside the data
                                               // stream.

      .m_axis_tvalid(m_axis_tvalid_0),           // 1-bit output: TVALID: Indicates that the master is driving a
                                               // valid transfer. A transfer takes place when both TVALID and
                                               // TREADY are asserted

      .prog_empty_axis(),       // 1-bit output: Programmable Empty- This signal is asserted
                                               // when the number of words in the FIFO is less than or equal to
                                               // the programmable empty threshold value. It is de-asserted
                                               // when the number of words in the FIFO exceeds the programmable
                                               // empty threshold value.

      .prog_full_axis(),         // 1-bit output: Programmable Full: This signal is asserted when
                                               // the number of words in the FIFO is greater than or equal to
                                               // the programmable full threshold value. It is de-asserted when
                                               // the number of words in the FIFO is less than the programmable
                                               // full threshold value.

      .rd_data_count_axis(), // RD_DATA_COUNT_WIDTH-bit output: Read Data Count- This bus
                                               // indicates the number of words available for reading in the
                                               // FIFO.

      .s_axis_tready(inter_c2h_tready),           // 1-bit output: TREADY: Indicates that the slave can accept a
                                               // transfer in the current cycle.

      .sbiterr_axis(),             // 1-bit output: Single Bit Error- Indicates that the ECC
                                               // decoder detected and fixed a single-bit error.

      .wr_data_count_axis(), // WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus
                                               // indicates the number of words written into the FIFO.

      .injectdbiterr_axis(1'b0), // 1-bit input: Double Bit Error Injection- Injects a double bit
                                               // error if the ECC feature is used.

      .injectsbiterr_axis(1'b0), // 1-bit input: Single Bit Error Injection- Injects a single bit
                                               // error if the ECC feature is used.

      .m_aclk(),                         // 1-bit input: Master Interface Clock: All signals on master
                                               // interface are sampled on the rising edge of this clock.

      .m_axis_tready(m_axis_tready_downstream),  //  1-bit input: TREADY: Indicates that the slave can accept a
                                               // transfer in the current cycle.

      .s_aclk(axi_aclk),                         // 1-bit input: Slave Interface Clock: All signals on slave
                                               // interface are sampled on the rising edge of this clock.

      .s_aresetn(axi_aresetn),                   // 1-bit input: Active low asynchronous reset.
      .s_axis_tdata(inter_c2h_tdata),             // TDATA_WIDTH-bit input: TDATA: The primary payload that is
                                               // used to provide the data that is passing across the
                                               // interface. The width of the data payload is an integer number
                                               // of bytes.

      .s_axis_tdest(1'b0),             // TDEST_WIDTH-bit input: TDEST: Provides routing information
                                               // for the data stream.

      .s_axis_tid(1'b0),                 // TID_WIDTH-bit input: TID: The data stream identifier that
                                               // indicates different streams of data.

      .s_axis_tkeep(inter_c2h_tkeep),             // TDATA_WIDTH/8-bit input: TKEEP: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as part of the data stream. Associated bytes
                                               // that have the TKEEP byte qualifier deasserted are null bytes
                                               // and can be removed from the data stream. For a 64-bit DATA,
                                               // bit 0 corresponds to the least significant byte on DATA, and
                                               // bit 7 corresponds to the most significant byte. For example:
                                               // KEEP[0] = 1b, DATA[7:0] is not a NULL byte KEEP[7] = 0b,
                                               // DATA[63:56] is a NULL byte

      .s_axis_tlast(inter_c2h_tlast),             // 1-bit input: TLAST: Indicates the boundary of a packet.

      .s_axis_tstrb(s_axis_tstrb_c),             // TDATA_WIDTH/8-bit input: TSTRB: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as a data byte or a position byte. For a 64-bit
                                               // DATA, bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 7 corresponds to the most significant byte. For
                                               // example: STROBE[0] = 1b, DATA[7:0] is valid STROBE[7] = 0b,
                                               // DATA[63:56] is not valid

      .s_axis_tuser(1'b0),             // TUSER_WIDTH-bit input: TUSER: The user-defined sideband
                                               // information that can be transmitted alongside the data
                                               // stream.

      .s_axis_tvalid(inter_c2h_tvalid)            // 1-bit input: TVALID: Indicates that the master is driving a
                                               // valid transfer. A transfer takes place when both TVALID and
                                               // TREADY are asserted

   );


   // End of xpm_fifo_axis_inst instantiation

    //wire m_axis_tready_adc_fifo = (inter_c2h_tready || !acq_en); // Flush upstream fifo on acq disabled  
     
   // UPSTREAM 32k sample FIFO 
   xpm_fifo_axis #(
      .CDC_SYNC_STAGES(3),            // DECIMAL Range: 2 - 8. Default value = 2.
      .CLOCKING_MODE("independent_clock"), // String
      .ECC_MODE("no_ecc"),            // String
      .FIFO_DEPTH(DMA_FIFO_DEPTH),              // DECIMAL 32768 (Max DEPTH* num of Bits= 4194304 bit)
      .FIFO_MEMORY_TYPE("auto"),      // String
      .PACKET_FIFO("false"),          // String
       //.PROG_EMPTY_THRESH(32736),         // DECIMAL
      .PROG_FULL_THRESH(32736),       // DECIMAL 8- 32763
       //.RD_DATA_COUNT_WIDTH(16),        // DECIMAL
      .RELATED_CLOCKS(0),             // DECIMAL
      .SIM_ASSERT_CHK(0),             // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      .TDATA_WIDTH(C_STREAM_DATA_WIDTH),               // DECIMAL Defines the width of the TDATA port, s_axis_tdata and m_axis_tdata
      .TDEST_WIDTH(1),                 // DECIMAL
      .TID_WIDTH(1),                   // DECIMAL
      .TUSER_WIDTH(1),                 // DECIMAL
       .USE_ADV_FEATURES("1006"),      // String USE_ADV_FEATURES[1] to 1 enables prog_full flag;
                                       // USE_ADV_FEATURES[2]  to 1 enables wr_data_count;
      .WR_DATA_COUNT_WIDTH(16)         // DECIMAL
   )
   xpm_fifo_axis_adc_0_i (
      .almost_empty_axis(),   // 1-bit output: Almost Empty : When asserted, this signal
                                               // indicates that only one more read can be performed before the
                                               // FIFO goes to empty.

      .almost_full_axis(),     // 1-bit output: Almost Full: When asserted, this signal
                                               // indicates that only one more write can be performed before
                                               // the FIFO is full.

      .dbiterr_axis(),             // 1-bit output: Double Bit Error- Indicates that the ECC
                                               // decoder detected a double-bit error and data in the FIFO core
                                               // is corrupted.

      .m_axis_tdata(inter_c2h_tdata),             // TDATA_WIDTH-bit output: TDATA: The primary payload that is
                                               // used to provide the data that is passing across the
                                               // interface. The width of the data payload is an integer number
                                               // of bytes.

      .m_axis_tdest(),             // TDEST_WIDTH-bit output: TDEST: Provides routing information
                                               // for the data stream.

      .m_axis_tid(),                 // TID_WIDTH-bit output: TID: The data stream identifier that
                                               // indicates different streams of data.

      .m_axis_tkeep(inter_c2h_tkeep),             // TDATA_WIDTH/8-bit output: TKEEP: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as part of the data stream. Associated bytes
                                               // that have the TKEEP byte qualifier deasserted are null bytes
                                               // and can be removed from the data stream. For a 64-bit DATA,
                                               // bit 0 corresponds to the least significant byte on DATA, and
                                               // bit 7 corresponds to the most significant byte. For example:
                                               // KEEP[0] = 1b, DATA[7:0] is not a NULL byte KEEP[7] = 0b,
                                               // DATA[63:56] is a NULL byte

      .m_axis_tlast(inter_c2h_tlast),             // 1-bit output: TLAST: Indicates the boundary of a packet.
      .m_axis_tstrb(),             // TDATA_WIDTH/8-bit output: TSTRB: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as a data byte or a position byte. For a 64-bit
                                               // DATA, bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 7 corresponds to the most significant byte. For
                                               // example: STROBE[0] = 1b, DATA[7:0] is valid STROBE[7] = 0b,
                                               // DATA[63:56] is not valid

      .m_axis_tuser(),             // TUSER_WIDTH-bit output: TUSER: The user-defined sideband
                                               // information that can be transmitted alongside the data
                                               // stream.

      .m_axis_tvalid(inter_c2h_tvalid),           // 1-bit output: TVALID: Indicates that the master is driving a
                                               // valid transfer. A transfer takes place when both TVALID and
                                               // TREADY are asserted

      .prog_empty_axis(),       // 1-bit output: Programmable Empty- This signal is asserted
                                               // when the number of words in the FIFO is less than or equal to
                                               // the programmable empty threshold value. It is de-asserted
                                               // when the number of words in the FIFO exceeds the programmable
                                               // empty threshold value.

      .prog_full_axis(fifo_prog_full_c2h_0),         // 1-bit output: Programmable Full: This signal is asserted when
                                               // the number of words in the FIFO is greater than or equal to
                                               // the programmable full threshold value. It is de-asserted when
                                               // the number of words in the FIFO is less than the programmable
                                               // full threshold value.

      .rd_data_count_axis(), // RD_DATA_COUNT_WIDTH-bit output: Read Data Count- rd_data_count This bus
                                               // indicates the number of words available for reading in the
                                               // FIFO.

      .s_axis_tready(fifo_ready_c2h_0),           // 1-bit output: TREADY: Indicates that the slave can accept a
                                               // transfer in the current cycle.

      .sbiterr_axis(),             // 1-bit output: Single Bit Error- Indicates that the ECC
                                               // decoder detected and fixed a single-bit error.

      .wr_data_count_axis(wr_data_count), // WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus
                                               // indicates the number of words written into the FIFO.

      .injectdbiterr_axis(1'b0), // 1-bit input: Double Bit Error Injection- Injects a double bit
                                               // error if the ECC feature is used.

      .injectsbiterr_axis(1'b0), // 1-bit input: Single Bit Error Injection- Injects a single bit
                                               // error if the ECC feature is used.

      .m_aclk(axi_aclk),                         // 1-bit input: Master Interface Clock: All signals on master
                                               // interface are sampled on the rising edge of this clock.

      .m_axis_tready(inter_c2h_tready ),           //  1-bit input: TREADY: Indicates that the slave can accept a
                                               // transfer in the current cycle.

      .s_aclk(adc_data_clk),                         // 1-bit input: Slave Interface Clock: All signals on slave
                                               // interface are sampled on the rising edge of this clock.

      .s_aresetn(axi_aresetn),                   // 1-bit input: Active low asynchronous reset.
      .s_axis_tdata(data_c2h_0_end_r),             // TDATA_WIDTH-bit input: TDATA: The primary payload that is
                                               // used to provide the data that is passing across the
                                               // interface. The width of the data payload is an integer number
                                               // of bytes.

      .s_axis_tdest(1'b0),             // TDEST_WIDTH-bit input: TDEST: Provides routing information
                                               // for the data stream.

      .s_axis_tid(1'b0),                 // TID_WIDTH-bit input: TID: The data stream identifier that
                                               // indicates different streams of data.

      .s_axis_tkeep(s_axis_tkeep_c),             // TDATA_WIDTH/8-bit input: TKEEP: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as part of the data stream. Associated bytes
                                               // that have the TKEEP byte qualifier deasserted are null bytes
                                               // and can be removed from the data stream. For a 64-bit DATA,
                                               // bit 0 corresponds to the least significant byte on DATA, and
                                               // bit 7 corresponds to the most significant byte. For example:
                                               // KEEP[0] = 1b, DATA[7:0] is not a NULL byte KEEP[7] = 0b,
                                               // DATA[63:56] is a NULL byte

      .s_axis_tlast(s_axis_tlast_r),             // 1-bit input: TLAST: Indicates the boundary of a packet.

      .s_axis_tstrb(s_axis_tstrb_c),             // TDATA_WIDTH/8-bit input: TSTRB: The byte qualifier that
                                               // indicates whether the content of the associated byte of TDATA
                                               // is processed as a data byte or a position byte. For a 64-bit
                                               // DATA, bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 0 corresponds to the least significant byte on
                                               // DATA, and bit 7 corresponds to the most significant byte. For
                                               // example: STROBE[0] = 1b, DATA[7:0] is valid STROBE[7] = 0b,
                                               // DATA[63:56] is not valid

      .s_axis_tuser(1'b0),             // TUSER_WIDTH-bit input: TUSER: The user-defined sideband
                                               // information that can be transmitted alongside the data
                                               // stream.

      .s_axis_tvalid(data_vld_c2h_0_r)            // 1-bit input: TVALID: Indicates that the master is driving a
                                               // valid transfer. A transfer takes place when both TVALID and
                                               // TREADY are asserted

   );
   // End of xpm_fifo_axis_inst instantiation

    wire [DFLOAT_WIDTH-1:0] integral_double_mult_ar[0 : N_INT_CHANNELS-1];
    //(* keep = "true" *) wire [DFLOAT_WIDTH-1:0] integral_mult_ar4 = integral_double_mult_ar[4];
    //assign float_val_2 = integral_float32_ar[0];

    wire [WO_WIDTH-1 : 0] wo_off_ar[0:(N_ADC_CHANNELS-1)]; // array
    integer ch; // used in 'for loops'
    generate
        begin
            //ch = 0;  // no compiles
            for (k = 0; k < N_ADC_CHANNELS; k = k + 1)
                assign wo_off_ar[k] =  wo_offset[(WO_WIDTH*(k+1)-1):(WO_WIDTH * k)] ;
        end
    endgenerate

    wire [ILCK_WIDTH-1 : 0] ilck_par_ar[0 : N_ILOCK_PARAMS-1];  // array
    wire [DFLOAT_WIDTH-1:0]  ilck_par64_ar[0:N_INT_CHANNELS-1]; // array for coeffs in double format
    generate
        for (k = 0; k < N_ILOCK_PARAMS; k = k + 1)
        begin: ilck_par_ar_gen
            assign ilck_par_ar[k] =  ilck_param[(ILCK_WIDTH *(k+1)-1) : (ILCK_WIDTH * k)] ;
        end
    endgenerate

    reg  signed [INTEGRAL_WIDTH-1 :0] adc_integral_ar[0: N_ADC_CHANNELS-1]; // array of N_ADC_CHANNELS 80-bit registers

    reg integral_valid_r; // Integral valid for calculation
    // (* keep = "true" *) 

    wire [N_INT_CHANNELS-1:0] doubleInt64valid;
    wire [N_INT_CHANNELS-1:0] multDoublevalid;

    reg data_valid_c2h_r = 1'b0; // acq_on;

    reg accum_in_vld_0_r = 1'b0, accum_in_vld_1_r = 1'b0; // Accumulator input data valid

    wire accum_out_vld_0, accum_out_vld_1;

    wire  sub_vld_0 = accum_out_vld_0; // Subtract final Accumm  valid
    wire  sub_vld_1 = accum_out_vld_1; // Subtract final Accumm  valid
    //(* keep = "true" *)
    wire  sub_rdy_0, sub_rdy_1;

    reg  [DFLOAT_WIDTH-1:0] accum_inp_0, accum_inp_1; // Input data to Accumulators
    //(* keep = "true" *)
    wire [DFLOAT_WIDTH-1:0] accum_rslt_0, accum_rslt_1;

    reg [DFLOAT_WIDTH-1:0] last_accum_F_r;//, last_accum_dF_r;

    wire [FLOAT_WIDTH-1:0] accum_rslt32_0, accum_rslt32_1;
    wire [FLOAT_WIDTH-1:0] sub_rslt_0, sub_rslt_1;

    reg [C_STREAM_DATA_WIDTH-1 : 0] data_c2h_1_r;


    // Input data for the Diamagnetic algorithm
    //wire signed [INTEGRAL_WIDTH-1 : 0] adc_dia_integral[0: N_INT_CHANNELS-1];
    
    //wire [DFLOAT_WIDTH-1: 0] integral_dia_ar[0: N_INT_CHANNELS-1];

    generate
        for (k = 0; k < N_ADC_CHANNELS; k = k+1)
        begin: FloatIntegrals
            Int64toFloat_lat1 Int64toFloat_inst (
              .aclk(adc_data_clk),
              .s_axis_a_tvalid(integral_valid_r), // i 
              .s_axis_a_tready(),   // o                 Blocking Mode
              .s_axis_a_tdata(integer64_part_f(adc_integral_ar[k])),  // i [63:0], 64-bit value
              .m_axis_result_tvalid(singleInt64valid[k]),  // output
              .m_axis_result_tdata(integral_single_ar[k])    // o [31:0]
            );
        end
    endgenerate
    
    reg accum_resetn_r = 1'b1; // Accumulator reset
    generate
        for (k = 0; k < N_INT_CHANNELS; k = k+1)
        begin: FpTerms
            //assign adc_dia_integral[k] =  adc_integral[INTEGER_CHANNEL_ARRAY[k]];
        
            Int64toDouble_lat1 Int64toDouble_inst (
              .aclk(adc_data_clk),
              .s_axis_a_tvalid(integral_valid_r),
              .s_axis_a_tready(),   // o                 Blocking Mode
              .s_axis_a_tdata(integer64_part_f(adc_integral_ar[INTEGER_CHANNEL_ARRAY[k]])),  // i [63:0], 64-bit value
              .m_axis_result_tvalid(doubleInt64valid[k]),  // o
              .m_axis_result_tdata(integral_double_ar[k])    // o [63:0]
            );
            
            Float2Double_lat0 F2D_inst (
                .s_axis_a_tvalid(doubleInt64valid[0]),            //i
                .s_axis_a_tdata(ilck_par_ar[k]),            // i [31 : 0]
                .m_axis_result_tvalid(),  // o
                .m_axis_result_tdata(ilck_par64_ar[k])   // o [63 : 0]
            );

            DoubleMult_lat1 DoubleMult_lat1_inst (
              .aclk(adc_data_clk),
              .s_axis_a_tvalid(doubleInt64valid[0]), // id   Is Blocking Mode
              .s_axis_a_tdata(integral_double_ar[k]),   // i [63:0]
              .s_axis_a_tready(),                       //o           Blocking Mode
              .s_axis_b_tvalid(doubleInt64valid[0]),            // constant , always valid
              .s_axis_b_tdata(ilck_par64_ar[k]),   // i [63 : 0]
              .s_axis_b_tready(),             //o
              .m_axis_result_tvalid(multDoublevalid[k]),  // o
              .m_axis_result_tdata(integral_double_mult_ar[k])    // o [63 : 0]
            );
        end
    endgenerate

    DoubleAccum_lat2  DoubleAccum_lat2_inst0 (
      .aclk(adc_data_clk),
      .aresetn(accum_resetn_r),                      // input reset accumulator
      .s_axis_a_tvalid(accum_in_vld_0_r),            // i
      .s_axis_a_tready(),                   // o, not used,
      .s_axis_a_tdata(accum_inp_0),              // input wire [63 : 0]
      .s_axis_a_tlast(1'b0),              // i

      .m_axis_result_tvalid(accum_out_vld_0),  // o
      .m_axis_result_tuser(accum_overflow[1:0]),    // o [1 : 0]
      .m_axis_result_tdata(accum_rslt_0),    // output wire [63 : 0]
      .m_axis_result_tlast()    // o
    );

    DoubleAccum_lat2  DoubleAccum_lat2_inst1 (
      .aclk(adc_data_clk),

      .aresetn(accum_resetn_r),                      // input reset accumulator
      .s_axis_a_tvalid(accum_in_vld_1_r),            // i
      .s_axis_a_tready(),                   // o, not used,
      .s_axis_a_tdata(accum_inp_1),              // input wire [63 : 0]
      .s_axis_a_tlast(1'b0),              // i

      .m_axis_result_tvalid(accum_out_vld_1),  // o
      .m_axis_result_tuser(accum_overflow[3:2]),    // o [1 : 0]
      .m_axis_result_tdata(accum_rslt_1),    // output wire [63 : 0]
      .m_axis_result_tlast()    // o
    );

    Double2Single_lat0 Double2Float_lat0_inst0 (
          .s_axis_a_tvalid(accum_out_vld_0),
          .s_axis_a_tdata(accum_rslt_0),
          .m_axis_result_tvalid(d2f_tvalid_0),
          .m_axis_result_tdata(accum_rslt32_0)
    );
    Double2Single_lat0 Double2Float_lat0_inst1 (
          .s_axis_a_tvalid(accum_out_vld_1),
          .s_axis_a_tdata(accum_rslt_1),
          .m_axis_result_tvalid(d2f_tvalid_1),
          .m_axis_result_tdata(accum_rslt32_1)
    );

    FpSingleSub_lat1 FpSingleSub_lat1_0 (
      .aclk(adc_data_clk),                                  // i
      .s_axis_a_tvalid(sub_vld_0),            // i        Blocking Mode
      .s_axis_a_tready(),            // o
      .s_axis_a_tdata(abs_float32_f(accum_rslt32_0)),              // i wire [31 : 0]
      .s_axis_b_tvalid(1'b1),            // i
      .s_axis_b_tready(),            // o
      .s_axis_b_tdata(ilck_par_ar[8]),              // i [31 : 0] subtract term for "F" function
      .m_axis_result_tvalid(sub_rdy_0),  // o
      .m_axis_result_tdata(sub_rslt_0)    // o [31 : 0]
    );

    FpSingleSub_lat1 FpSingleSub_lat1_1 (
      .aclk(adc_data_clk),                                  // i
      .s_axis_a_tvalid(sub_vld_1),            // i     Blocking Mode
      .s_axis_a_tready(),            // o
      .s_axis_a_tdata(abs_float32_f(accum_rslt32_1)),              // input wire [31 : 0]
      .s_axis_b_tvalid(1'b1),            // i
      .s_axis_b_tready(),            // o
      .s_axis_b_tdata(ilck_par_ar[9]),              // input wire [31 : 0] subtract term for "dF" function
      .m_axis_result_tvalid(sub_rdy_1),  // o
      .m_axis_result_tdata(sub_rslt_1)    // o   wire [31 : 0]
    );

    /* Interlock calc */
    always @ (posedge adc_data_clk or negedge acq_on)
        if (!acq_on) begin

            // clear Integral registers
            last_accum_F_r     <= #TCQ  {DFLOAT_WIDTH{1'b0}};// 'h00; // Clear "F" calc
            //last_accum_dF_r    <= #TCQ 'h00; // Clear "diff F" calc
            float_val_0        <= #TCQ {FLOAT_WIDTH{1'b0}};//'h00;
            float_val_1        <= #TCQ {FLOAT_WIDTH{1'b0}};//'h00;
            float_val_2        <= #TCQ {FLOAT_WIDTH{1'b0}};//'h00;
            float_val_3        <= #TCQ {FLOAT_WIDTH{1'b0}};//'h00;
            accum_resetn_r     <= #TCQ 1'b0; // Clear
            for (ch = 0; ch < N_ADC_CHANNELS; ch = ch + 1) begin
                    adc_integral_ar[ch]      <=  #TCQ {INTEGRAL_WIDTH{1'b0}}; //'h00;
                    last_adc_dechopp[ch]  <=  #TCQ {(ADC_DATA_WIDTH+2){1'b0}}; //'h00;
            end
        end
        else
            case (adc_clk_cnt) // one cycle per sample. This case may start at clk_100_cnt != 0!
                5'h00: begin // ADC data Integral Calculation
                        accum_resetn_r      <= #TCQ 1'b1;  // lift reset

                        for (ch = 0; ch < N_ADC_CHANNELS; ch = ch + 1) begin
                             if(adc_data_hold)  // Use last "good" ADC sample before chopp transition (Spike removal)
                                  adc_integral_ar[ch]       <= #TCQ integral_calc_f(adc_integral_ar[ch],
                                      last_adc_dechopp[ch], wo_off_ar[ch]);
                             else begin
                                  adc_integral_ar[ch]       <= #TCQ integral_calc_f(adc_integral_ar[ch],
                                      adc_dechopp[ch], wo_off_ar[ch]);
                                  last_adc_dechopp[ch]   <=  #TCQ adc_dechopp[ch]; // Save for spike corretion
                             end
                        end

                        integral_valid_r  <= #TCQ 1'b1; // enable F Function term calc in Fp** IP cores
                    end
                5'h01:
                    integral_valid_r   <= #TCQ 1'b0;

                5'h02: ;
                        //float_val_2  <= #TCQ accum_rslt_0; // ?

                5'h03: begin
                    if (multDoublevalid[0]) begin
                        accum_in_vld_0_r    <= #TCQ 1'b1; //Start Accumulating F terms
                        accum_in_vld_1_r    <= #TCQ 1'b1; //Start Accumulating F terms
                        accum_inp_0         <= #TCQ integral_double_mult_ar[0];  // First even term
                        accum_inp_1         <= #TCQ integral_double_mult_ar[1];  // First odd term
                    end
                        //float_val_0  <= #TCQ integral_float32_ar[4];  // OK
                        //float_val_0  <= #TCQ accum_rslt_0;
                end 
                5'h04: begin
                        accum_inp_0    <= #TCQ integral_double_mult_ar[2];
                        accum_inp_1    <= #TCQ integral_double_mult_ar[3];
                     end
                5'h05: begin
                        accum_inp_0    <= #TCQ integral_double_mult_ar[4];  // even term
                        accum_inp_1    <= #TCQ integral_double_mult_ar[5];  // odd term
                        //float_val_1  <= #TCQ integral_mult32_ar[4];  // OK
                     end
                5'h06: begin
                        accum_inp_0    <= #TCQ integral_double_mult_ar[6];  // even term
                        accum_inp_1    <= #TCQ integral_double_mult_ar[7];  // odd term
                        //float_val_0  <= #TCQ accum_rslt_0; // 0,0,0
                     end
                5'h07: begin
                    accum_inp_0         <= #TCQ 'h00;  // wait one latency cycle
                    accum_inp_1         <= #TCQ 'h00;
                    accum_in_vld_0_r    <= #TCQ 1'b0;
                    accum_in_vld_1_r    <= #TCQ 1'b0;
                    //float_val_1  <= #TCQ accum_rslt_0; //OK
                end
                5'h08: ;
                5'h09: begin
                    accum_inp_0         <= #TCQ accum_rslt_1;  // Sum two (even/odd) terms
                    accum_inp_1         <= #TCQ accum_rslt_0;  // idem ...
                    accum_in_vld_0_r    <= #TCQ 1'b1;
                    accum_in_vld_1_r    <= #TCQ 1'b1;
                    //accum_rslt_1_8     <= #TCQ accum_rslt_0;  // Sum two (even/odd) terms
                    //accum_rslt_1_8     <= #TCQ accum_rslt_1;  // Sum two (even/odd) terms
                end
                5'h0A: begin
                    accum_inp_0         <= #TCQ 'h00; //
                    accum_in_vld_0_r    <= #TCQ 1'b0;                       //Stop Accumulating
                    accum_inp_1         <= #TCQ float64_negate_f(last_accum_F_r);   // Subtract last "F" value
                end
                5'h0B: accum_in_vld_1_r  <= #TCQ 1'b0;                       //Stop Accumulating
                5'h0C: begin
                        //last_accum_F_r  <= #TCQ float32_negate_f(accum_rslt32_0);     // Save for previous case
                        if (accum_out_vld_0)
                            last_accum_F_r  <= #TCQ accum_rslt_0;     // Save for previous case
                        accum_inp_1     <= #TCQ 'h00; //
                        if (d2f_tvalid_0)
                            float_val_0     <= #TCQ accum_rslt32_0;
                        //float_val_1  <= #TCQ accum_rslt_1;  // = 0..
                end
                5'h0D: begin
                        //if (accum_out_vld_1)
                            //last_accum_dF_r <= #TCQ accum_rslt_1;
                        if (d2f_tvalid_1)
                            float_val_1     <= #TCQ accum_rslt32_1;
                        if (sub_rdy_0) begin
                             interlock_F_r  <= #TCQ  !sub_rslt_0[FLOAT_SIGN_BIT];     // Interlock is ON if final DSP result is POSITIVE
                             float_val_2    <= #TCQ sub_rslt_0;
                         end
                       end
                5'h0E: begin
                         if (sub_rdy_1) begin
                             interlock_dF_r <= #TCQ  !sub_rslt_1[FLOAT_SIGN_BIT];
                             float_val_3    <= #TCQ sub_rslt_1;
                         end
                      end

                5'h0F: ; //begin
                //end
                5'h10: ;
                5'h11: ;
                5'h12: ; //cnt_sample_c2h1_r   <= #TCQ cnt_sample_c2h1_r + 1;
                5'h14: accum_resetn_r     <= #TCQ 1'b0; // Reset Accumulator ...
                5'h18: accum_resetn_r     <= #TCQ 1'b1; // ...for at least for two clock Cycles

                default:begin
                    integral_valid_r      <= #TCQ 1'b0;
                    accum_in_vld_0_r      <= #TCQ 1'b0;
                    accum_in_vld_1_r      <= #TCQ 1'b0;
                 end
            endcase
            
            assign m_axis_tvalid_1 = 1'b1;
/*
    // (* keep = "true" *)
    reg data_valid_c2h1_r = 1'b0;

generate
if (C_DATA_WIDTH == 128)

    //--  Decimated (1:200) data mux on C2H_1 DMA channel 
    always @ (posedge adc_data_clk or negedge acq_on)
        if (!acq_on) begin
            data_valid_c2h1_r  <= 1'b0;
            cnt_pckt_c2h1_r    <= 'h00;

            cnt_decim_c2h_1_r  <= 'h00; // counts samples 0 up 199
        end
        else begin
            data_c2h_1_end_r <= #TCQ big_endian_128_f(data_c2h_1_r, big_endian);
            case (adc_clk_cnt) // one cycle per sample. This case may start at clk_100_cnt != 0!
                5'h00: begin
                        data_c2h_1_r      <= #TCQ 'h00;
                    end
                5'h01: begin
                        data_c2h_1_r      <= #TCQ {64'h0001020304050607, cnt_pckt_c2h1_r};
                    end
                5'h02: begin
                    if (cnt_decim_c2h_1_r == 10'd199) begin
                        cnt_decim_c2h_1_r   <= #TCQ 'h00;
                        cnt_pckt_c2h1_r     <= #TCQ (cnt_pckt_c2h1_r + 1);
                        if (!fifo_prog_full_c2h1_i)  begin // Check if there is space for one packet
                            data_valid_c2h1_r   <= #TCQ 1'b1;
                        end
                    end
                    else begin
                        cnt_decim_c2h_1_r <= #TCQ (cnt_decim_c2h_1_r + 1);
                    end

                    data_c2h_1_r   <= #TCQ {adc_dechopp[3], 12'h00, adc_dechopp[2], 12'h00,
                            adc_dechopp[1], 12'h00, adc_dechopp[0], 12'h00};
                end
                5'h03: begin
                    data_c2h_1_r       <= #TCQ {adc_dechopp[7], 12'h00, adc_dechopp[6], 12'h00,
                        adc_dechopp[5], 12'h00, adc_dechopp[4], 12'h00};
                end
                //5'h04: data_c2h_1_r <= #TCQ {integral_double_mult_ar[1], 64'h404B_8000_0000_0000}; // 55.0D
                //5'h04: data_c2h_1_r <= #TCQ {64'h0001020304050607, 32'h08090A0B, cnt_pckt_c2h1_r[31:0]};
                5'h04: data_c2h_1_r <= #TCQ {integral_double_mult_ar[1], integral_double_mult_ar[0]};
                5'h05: data_c2h_1_r <= #TCQ {integral_double_mult_ar[3], integral_double_mult_ar[2]};
                5'h06: data_c2h_1_r <= #TCQ {integral_double_mult_ar[5], integral_double_mult_ar[4]};
                5'h07: data_c2h_1_r <= #TCQ {integral_double_mult_ar[7], integral_double_mult_ar[6]};
                5'h08: data_c2h_1_r <= #TCQ {float_val_3, float_val_2, float_val_1, float_val_0};
                5'h09: begin
                        if (&cnt_pckt_c2h1_r[7:0] == 1'b1 )      // 32kB ? Check here
                            s_axis_tlast_c2h1_r <= #TCQ 1'b1;
                end
                5'h0A: begin
                    data_valid_c2h1_r   <= #TCQ 1'b0;
                    s_axis_tlast_c2h1_r <= #TCQ 1'b0;
                end

                default: begin
                    s_axis_tlast_c2h1_r <= #TCQ 1'b0;
                    data_valid_c2h1_r   <= #TCQ 1'b0;
                 end
            endcase
        end
endgenerate  //128/64 bit

   wire m_a_tready_1 = 1'b0; // drop c2h 1 ( m_axis_tready_1 || !acq_en); // Flush data fifo on acq disabled

   xpm_fifo_axis #(
      .CDC_SYNC_STAGES(2),            // DECIMAL Range: 2 - 8. Default value = 2.
      .CLOCKING_MODE("independent_clock"), // String
      .ECC_MODE("no_ecc"),            // String
      .FIFO_DEPTH(512),              // DECIMAL
      .FIFO_MEMORY_TYPE("auto"),      // String
      .PACKET_FIFO("false"),          // String
      .PROG_EMPTY_THRESH(20),         // DECIMAL
      .PROG_FULL_THRESH(480),       // DECIMAL 8- 32763
      //.RD_DATA_COUNT_WIDTH(6),        // DECIMAL
      .RELATED_CLOCKS(0),             // DECIMAL
      .SIM_ASSERT_CHK(0),             // DECIMAL; 0=disable simulation messages, 1=enable simulation messages
      .TDATA_WIDTH(C_DATA_WIDTH),               // DECIMAL Defines the width of the TDATA port, s_axis_tdata and m_axis_tdata
      .TDEST_WIDTH(1),                // DECIMAL
      .TID_WIDTH(1),                  // DECIMAL
      .TUSER_WIDTH(1),                // DECIMAL
      .USE_ADV_FEATURES("0106")      // String // USE_ADV_FEATURES[1] to 1 enables prog_full flag;    Default value of this bit is 0                        |
      //.WR_DATA_COUNT_WIDTH(8)         // DECIMAL
   )
   xpm_fifo_axis_c2h_1_i (
      .almost_empty_axis(),
      .almost_full_axis(),
      .dbiterr_axis(),
      .m_axis_tdata(m_axis_tdata_1),
      .m_axis_tdest(),
      .m_axis_tid(),
      .m_axis_tkeep(m_axis_tkeep_1),
      .m_axis_tlast(m_axis_tlast_1),             // 1-bit output: TLAST: Indicates the boundary of a packet.
      .m_axis_tstrb(),
      .m_axis_tuser(),
      .m_axis_tvalid(m_axis_tvalid_1),
      .prog_empty_axis(),
      .prog_full_axis(fifo_prog_full_c2h1_i),
      .rd_data_count_axis(),
      .sbiterr_axis(),
      .wr_data_count_axis(),
      .injectdbiterr_axis(1'b0),
      .injectsbiterr_axis(1'b0),
      .m_aclk(axi_aclk),
      .m_axis_tready(m_a_tready_1),

      .s_axis_tdest(1'b0),
      .s_axis_tid(1'b0),
      .s_axis_tkeep(s_axis_tkeep_c),
      .s_axis_tstrb(s_axis_tstrb_c),
      .s_axis_tuser(1'b0),

      .s_aresetn(axi_aresetn),                   // 1-bit input: Acts_axis_tlast_c2h1_iive low asynchronous reset.

      .s_axis_tdata(data_c2h_1_end_r),
      .s_axis_tready(fifo_ready_c2h_1),
      .s_axis_tlast(s_axis_tlast_c2h1_r),             // 1-bit input: TLAST: Indicates the boundary of a packet.
      .s_axis_tvalid(data_valid_c2h1_r),

      .s_aclk(adc_data_clk)
   );
*/
endmodule // adc_data_producer.sv