//////////////////////////////////////////////////////////////////////
////                                                              ////
//// registerInterface.v                                          ////
////                                                              ////
//// This file is part of the i2cSlave opencores effort.
//// <http://www.opencores.org/cores//>                           ////
////                                                              ////
//// Module Description:                                          ////
//// You will need to modify this file to implement your 
//// interface.
//// Add your control and status bytes/bits to module inputs and outputs,
//// and also to the I2C read and write process blocks  
////                                                              ////
//// To Do:                                                       ////
////
////                                                              ////
//// Author(s):                                                   ////
//// - Steve Fielding, sfielding@base2designs.com                 ////
////                                                              ////
//////////////////////////////////////////////////////////////////////
////                                                              ////
//// Copyright (C) 2008 Steve Fielding and OPENCORES.ORG          ////
////                                                              ////
//// This source file may be used and distributed without         ////
//// restriction provided that this copyright statement is not    ////
//// removed from the file and that any derivative work contains  ////
//// the original copyright notice and the associated disclaimer. ////
////                                                              ////
//// This source file is free software; you can redistribute it   ////
//// and/or modify it under the terms of the GNU Lesser General   ////
//// Public License as published by the Free Software Foundation; ////
//// either version 2.1 of the License, or (at your option) any   ////
//// later version.                                               ////
////                                                              ////
//// This source is distributed in the hope that it will be       ////
//// useful, but WITHOUT ANY WARRANTY; without even the implied   ////
//// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      ////
//// PURPOSE. See the GNU Lesser General Public License for more  ////
//// details.                                                     ////
////                                                              ////
//// You should have received a copy of the GNU Lesser General    ////
//// Public License along with this source; if not, download it   ////
//// from <http://www.opencores.org/lgpl.shtml>                   ////
////                                                              ////
//////////////////////////////////////////////////////////////////////
//
`timescale 1ns / 1ps
`include "i2cSlave_define.vh"

module registerInterface (

    input clk,
    input [7:0] addr,
    input [7:0] dataIn,
    input writeEn,
    output [7:0] dataOut,

    output [7:0] myReg0,
    output [7:0] myReg1,
    output [7:0] myReg2,
    output [7:0] myReg3,

    output [15:0] tmp101_0,
    output [15:0] tmp101_1,

    output [15:0] mcp9808_0,
    output [15:0] mcp9808_1,
    output [15:0] mcp9808_2,
    output [15:0] mcp9808_3,
    output [15:0] mcp9808_4,
    output [15:0] mcp9808_5,
    output [15:0] mcp9808_6,
    output [15:0] mcp9808_7,

    input [7:0] myReg20,
    input [7:0] myReg21,
    input [7:0] myReg22,
    input [7:0] myReg23
);

reg [7:0] dataOut_r = 8'h00;
reg [7:0] myReg0_r = 8'hFF;
reg [7:0] myReg1_r = 8'hFA;  // Make sure ATCA is slave at beggining
reg [7:0] myReg2_r = 8'hFF;
reg [7:0] myReg3_r = 8'hFF;

reg [15:0] tmp101_0_r = 16'h00;   // TMP1010 temperature sensors
reg [15:0] tmp101_1_r = 16'h00;

reg [15:0] mcp9808_0_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_1_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_2_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_3_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_4_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_5_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_6_r = 16'h00;   // MPC9808 temperature sensors
reg [15:0] mcp9808_7_r = 16'h00;   // MPC9808 temperature sensors

assign dataOut = dataOut_r;
assign myReg0 = myReg0_r;
assign myReg1 = myReg1_r;
assign myReg2 = myReg2_r;
assign myReg3 = myReg3_r;

assign tmp101_0 = tmp101_0_r;
assign tmp101_1 = tmp101_1_r;

assign mcp9808_0 = mcp9808_0_r;
assign mcp9808_1 = mcp9808_1_r;
assign mcp9808_2 = mcp9808_2_r;
assign mcp9808_3 = mcp9808_3_r;
assign mcp9808_4 = mcp9808_4_r;
assign mcp9808_5 = mcp9808_5_r;
assign mcp9808_6 = mcp9808_6_r;
assign mcp9808_7 = mcp9808_7_r;

// --- I2C Read
always @(posedge clk) begin
  case (addr)
    8'h00: dataOut_r <= myReg0_r;
    8'h01: dataOut_r <= myReg1_r;
    8'h02: dataOut_r <= myReg2_r;
    8'h03: dataOut_r <= myReg3_r;

    8'h04: dataOut_r <= tmp101_0_r[7:0]; //
    8'h05: dataOut_r <= tmp101_0_r[15:8]; //
    8'h06: dataOut_r <= tmp101_1_r[7:0]; //
    8'h07: dataOut_r <= tmp101_1_r[15:8]; //

    8'h08: dataOut_r <= mcp9808_0_r[7:0]; //
    8'h09: dataOut_r <= mcp9808_0_r[15:8]; //
    8'h0A: dataOut_r <= mcp9808_1_r[7:0]; //
    8'h0B: dataOut_r <= mcp9808_1_r[15:8]; //
    8'h0C: dataOut_r <= mcp9808_2_r[7:0]; //
    8'h0D: dataOut_r <= mcp9808_2_r[15:8]; //
    8'h0E: dataOut_r <= mcp9808_3_r[7:0]; //
    8'h0F: dataOut_r <= mcp9808_3_r[15:8]; //
    8'h10: dataOut_r <= mcp9808_4_r[7:0]; //
    8'h11: dataOut_r <= mcp9808_4_r[15:8]; //
    8'h12: dataOut_r <= mcp9808_5_r[7:0]; //
    8'h13: dataOut_r <= mcp9808_5_r[15:8]; //
    8'h14: dataOut_r <= mcp9808_6_r[7:0]; //
    8'h15: dataOut_r <= mcp9808_6_r[15:8]; //
    8'h16: dataOut_r <= mcp9808_7_r[7:0]; //
    8'h17: dataOut_r <= mcp9808_7_r[15:8]; //

    8'h20: dataOut_r <= myReg20; // ro
    8'h21: dataOut_r <= myReg21;
    8'h22: dataOut_r <= myReg22;
    8'h23: dataOut_r <= myReg23;

    default: dataOut_r <= 8'hFF;
  endcase
end

// --- I2C Write
always @(posedge clk) begin
  if (writeEn == 1'b1) begin
    case (addr)
      8'h00: myReg0_r <= dataIn;
      8'h01: myReg1_r <= dataIn;
      8'h02: myReg2_r <= dataIn;
      8'h03: myReg3_r <= dataIn;

      8'h04: tmp101_0_r[7:0] <= dataIn;
      8'h05: tmp101_0_r[15:8] <= dataIn;
      8'h06: tmp101_1_r[7:0] <= dataIn;
      8'h07: tmp101_1_r[15:8] <= dataIn;

      8'h08: mcp9808_0_r[7:0] <= dataIn;
      8'h09: mcp9808_0_r[15:8] <= dataIn;
      8'h0A: mcp9808_1_r[7:0] <= dataIn;
      8'h0B: mcp9808_1_r[15:8] <= dataIn;
      8'h0C: mcp9808_2_r[7:0] <= dataIn;
      8'h0D: mcp9808_2_r[15:8] <= dataIn;
      8'h0E: mcp9808_3_r[7:0] <= dataIn;
      8'h0F: mcp9808_3_r[15:8] <= dataIn;
      8'h10: mcp9808_4_r[7:0] <= dataIn;
      8'h11: mcp9808_4_r[15:8] <= dataIn;
      8'h12: mcp9808_5_r[7:0] <= dataIn;
      8'h13: mcp9808_5_r[15:8] <= dataIn;
      8'h14: mcp9808_6_r[7:0] <= dataIn;
      8'h15: mcp9808_6_r[15:8] <= dataIn;
      8'h16: mcp9808_7_r[7:0] <= dataIn;
      8'h17: mcp9808_7_r[15:8] <= dataIn;
      default: ;
    endcase
  end
end

endmodule


 
