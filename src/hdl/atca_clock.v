/* vim: set ts=8 sw=4 tw=0 noet : */
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Company: INSTITUTO DOS PLASMAS E FUSAO NUCLEAR
// Engineer:  BBC
//
// Project Name:   W7X ATCA V2 DAQ
// Design Name:    ATCA V2_DAQ FIRMWARE
// Module Name:    atca_clock
// Target Devices: kintex-7
// Tool Versions:  Vivado 2023.2
//
// Description:
// MCMM for the 10 MHz received from  ATCA bus 
//
// Copyright 2015 - 2021 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date   Wed May 22 17:21:15 WEST 2019
//
// Licensed under the EUPL, Version 1.1 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");

// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// http://ec.europa.eu/idabc/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//////////////////////////////////////////////////////////////////////////////////////
//
`timescale 1ns/1ps
module atca_clock (
    input  clk_10_atca,
    input reset,
    
    output mmcm_locked,  
    output clk_50
);

    wire  clk_600_fb, clk_50_mmcm;

    MMCME2_BASE #(
        .BANDWIDTH("OPTIMIZED"),   // Jitter programming (OPTIMIZED, HIGH, LOW)
        //.CLKFBOUT_MULT_F(8.0),     // Multiply value for all CLKOUT (2.000-64.000).
        .CLKFBOUT_PHASE(0.0),      // Phase offset in degrees of CLKFB (-360.000-360.000).
        .CLKFBOUT_MULT_F(60.0),     // Multiply value for all CLKOUT (2.000-64.000).
        .CLKIN1_PERIOD(100.0),       // Input clock period in ns to ps resolution,
        // CLKOUT0_DIVIDE - CLKOUT0_DIVIDE: Divide amount for each CLKOUT (1-128)
        .CLKOUT0_DIVIDE_F(6.0),    // Divide amount for CLKOUT0 (1.000-128.000). 100 Mhz
        //      .CLKOUT0_DIVIDE(8),   //100 Mhz
        .CLKOUT1_DIVIDE(12),  // 50 MHz
        .CLKOUT2_DIVIDE(12), // 50 MHz
        .CLKOUT3_DIVIDE(1),  
        .CLKOUT4_DIVIDE(1), 
        .CLKOUT5_DIVIDE(1),
        .CLKOUT6_DIVIDE(1),
        // CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.01-0.99).
        .CLKOUT0_DUTY_CYCLE(0.5),
        .CLKOUT1_DUTY_CYCLE(0.5),
        .CLKOUT2_DUTY_CYCLE(0.5),
        .CLKOUT3_DUTY_CYCLE(0.5),
        .CLKOUT4_DUTY_CYCLE(0.5),
        .CLKOUT5_DUTY_CYCLE(0.5),
        .CLKOUT6_DUTY_CYCLE(0.5),
        // CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
        .CLKOUT0_PHASE(0.0),
        .CLKOUT1_PHASE(0.0),
        .CLKOUT2_PHASE(0.0),
        .CLKOUT3_PHASE(0.0),
        .CLKOUT4_PHASE(0.0),
        .CLKOUT5_PHASE(0.0),
        .CLKOUT6_PHASE(0.0),
        .CLKOUT4_CASCADE("FALSE"), // Cascade CLKOUT4  with CLKOUT6 (FALSE, TRUE)
        .DIVCLK_DIVIDE(1),         // Master division value (1-106)
        .REF_JITTER1(0.0),         // Reference input jitter in UI (0.000-0.999).
        .STARTUP_WAIT("FALSE")     // Delays DONE until MMCM is locked (FALSE, TRUE)
    )
    mmcm_sc_100_inst (
        // Clock Outputs: 1-bit (each) output: User configurable clock outputs
        .CLKOUT0(),     // 1-bit output: CLKOUT0 clk_100_mmcm
        .CLKOUT0B(),              // 1-bit output: Inverted CLKOUT0
        .CLKOUT1(clk_50),     // 1-bit output: CLKOUT1
        .CLKOUT1B(),             // 1-bit output: Inverted CLKOUT1
        .CLKOUT2(),     // 1-bit output: CLKOUT2
        //.CLKOUT2(clk_50_mmcm),     // 1-bit output: CLKOUT2
        .CLKOUT2B(),   // 1-bit output: Inverted CLKOUT2
        .CLKOUT3(),     // 1-bit output: CLKOUT3
        .CLKOUT3B(),   // 1-bit output: Inverted CLKOUT3
        .CLKOUT4(),     // 1-bit output: CLKOUT4
        .CLKOUT5(),     // 1-bit output: CLKOUT5
        .CLKOUT6(),     // 1-bit output: CLKOUT6
        // Feedback Clocks: 1-bit (each) output: Clock feedback ports
        .CLKFBOUT(clk_600_fb),   // 1-bit output: Feedback clock
        .CLKFBOUTB(), // 1-bit output: Inverted CLKFBOUT
        // Status Ports: 1-bit (each) output: MMCM status ports
        .LOCKED(mmcm_locked),       // 1-bit output: LOCK
        // Clock Inputs: 1-bit (each) input: Clock input
        .CLKIN1(clk_10_atca),       // 1-bit input: Clock
        // Control Ports: 1-bit (each) input: MMCM control ports
        .PWRDWN(1'b0),       // 1-bit input: Power-down
        .RST(reset),             // 1-bit input: Reset
        // Feedback Clocks: 1-bit (each) input: Clock feedback ports
        .CLKFBIN(clk_600_fb)      // 1-bit input: Feedback clock
    );

    //BUFG bufg_100 (.O(clk_100), .I(clk_100_mmcm) );
    //BUFG bufg_50 (.O(clk_50), .I(clk_50_mmcm) );


endmodule // atca_clock
