//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Company: IPFN-IST
// Engineer:  B. Carvalho & AJNB
//
// Create Date:    10:15:58 12/01/2018
// Design Name:    atca_mimo_v2_adc
// Module Name:    adc_chan_slr16
// Project Name:   ATCA v2 Board
// Target Devices: Kintex xc7kxxx  ADC  device AD7641
// https://www.analog.com/media/en/technical-documentation/data-sheets/AD7641.pdf
// MODE 0, MODE 1, WARP, RDC High, NORMAL : Low
// Warp Mode (Read Previous Conversion During Conversion)
// Tool versions:  Vivado 2019.2
// Description:  IDDR deserialzer for the IPFN ADC modules
//
// Dependencies:
//
// Copyright 2015 - 2019 IPFN-Instituto Superior Tecnico, Portugal
//
// Licensed under the EUPL, Version 1.2 only (the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence, available in 23 official languages of the European Union, at:
// https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//////////////////////////////////////////////////////////////////////////////////

`timescale 1ns / 1ps

module adc_chan_ddr #( IDELAY_VAL = 10 )
    (
        input adc_data_clk,
        input word_sync_n,
        //input [5:0] clk_100_cnt, // counts 0-49 in each adc period
        input serial_clock_p,
        input serial_clock_n,
        input serial_data_p,
        input serial_data_n,
        output [17:0] parallel_data

    );

    wire serial_data_i, serial_data_ibuf, serial_clock_i;
    IBUFGDS #(
        .DIFF_TERM("TRUE"),       // Differential Termination
        .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
    ) BUF_adc_clk (
        .O(serial_clock_i),  // Buffer output
        .I(serial_clock_p),  // Diff_p buffer input (connect directly to top-level port)
        .IB(serial_clock_n) // Diff_n buffer input (connect directly to top-level port)
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"),       // Differential Termination
        .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25")     // Specify the input I/O standard
    ) IBUFDS_adc_data (
        .O(serial_data_ibuf),  // Buffer output adc_data_i
        .I(serial_data_p),  // Diff_p buffer input (connect directly to top-level port)
        .IB(serial_data_n) // Diff_n buffer input (connect directly to top-level port)
    );

    // IDELAYE2: Input Fixed or Variable Delay Element
    //  Kintex-7
    // Xilinx HDL Language Template, version 2017.4
    //Average Tap Delay at 200 MHz = 78 ps, at 300 MHz = 52 ps, and at 400 MHz = 39 ps.
    //   (* IODELAY_GROUP = iodelay_group_adc *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL

    IDELAYE2 #(
        .CINVCTRL_SEL("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
        .DELAY_SRC("IDATAIN"),           // Delay input (IDATAIN, DATAIN)
        .HIGH_PERFORMANCE_MODE("FALSE"), // Reduced jitter ("TRUE"), Reduced power ("FALSE")
        .IDELAY_TYPE("FIXED"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
        .IDELAY_VALUE(IDELAY_VAL),        // Input delay tap setting (0-31) 15, 20 gives error
        .PIPE_SEL("FALSE"),              // Select pipelined mode, FALSE, TRUE
        .REFCLK_FREQUENCY(200.0),        // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
        .SIGNAL_PATTERN("DATA")          // DATA, CLOCK input signal
    )
    IDELAYE2_inst (
        .CNTVALUEOUT(), // 5-bit output: Counter value output
        .DATAOUT(serial_data_i),         // 1-bit output: Delayed data output
        .C(1'b0),                     // 1-bit input: Clock input
        .CE(1'b0),                   // 1-bit input: Active high enable increment/decrement input
        .CINVCTRL(),       // 1-bit input: Dynamic clock inversion input
        .CNTVALUEIN(),   // 5-bit input: Counter value input
        .DATAIN(),           // 1-bit input: Internal delay data input
        .IDATAIN(serial_data_ibuf),         // 1-bit input: Data input from the I/O
        .INC(1'b0),                 // 1-bit input: Increment / Decrement tap delay input
        .LD(),                   // 1-bit input: Load IDELAY_VALUE input
        .LDPIPEEN(),       // 1-bit input: Enable PIPELINE register to load data input
        .REGRST(1'b0)            // 1-bit input: Active-high reset tap-delay input
    );


    wire Qp, Qn;

    // IDDR: Input Double Data Rate Input Register with Set, Reset
    //       and Clock Enable.
    //       Kintex-7
    // Xilinx HDL Language Template, version 2015.4
    IDDR #(
        .DDR_CLK_EDGE("OPPOSITE_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE"
        //    or "SAME_EDGE_PIPELINED"
        .INIT_Q1(1'b0), // Initial value of Q1: 1'b0 or 1'b1
        .INIT_Q2(1'b0), // Initial value of Q2: 1'b0 or 1'b1
        .SRTYPE("SYNC") // Set/Reset type: "SYNC" or "ASYNC"
    ) IDDR_inst (
        .Q1(Qp), // 1-bit output for positive edge of clock
        .Q2(Qn), // 1-bit output for negative edge of clock
        .C(serial_clock_i),   // 1-bit clock input
        .CE(1'b1), // 1-bit clock enable input
        .D(serial_data_i),   // 1-bit DDR data input
        .R(1'b0),   // 1-bit reset
        .S(1'b0)    // 1-bit set
    );


    // ADC  IDDR deserialzer

    reg [17:0] word_reg;
    reg [8:1] data_p, data_n;

    reg [3:0] count100 = 4'h0;
    always @ (posedge serial_clock_i  or negedge word_sync_n ) //begin or negedge word_sync_n
        if (!word_sync_n)
            count100  <=  4'h0;
        else
            count100  <=  count100 + 1'b1; // TODO change to Gray Count

    always @(negedge serial_clock_i) // "OPPOSITE_EDGE"
        case (count100)
            4'd0   : data_p <= 7'd0;
            4'd1   : data_p[8] <= Qp;
            4'd2   : data_p[7] <= Qp;
            4'd3   : data_p[6] <= Qp;
            4'd4   : data_p[5] <= Qp;
            4'd5   : data_p[4] <= Qp;
            4'd6   : data_p[3] <= Qp;
            4'd7   : data_p[2] <= Qp;
            4'd8   : data_p[1] <= Qp;
            default: ;
        endcase

    always @(posedge serial_clock_i)
        case (count100)
            //            4'd0   : data_p[8] <= serial_data;
            4'd0   : data_n <= 7'd0;
            4'd1   : data_n[8] <= Qn;
            4'd2   : data_n[7] <= Qn;
            4'd3   : data_n[6] <= Qn;
            4'd4   : data_n[5] <= Qn;
            4'd5   : data_n[4] <= Qn;
            4'd6   : data_n[3] <= Qn;
            4'd7   : data_n[2] <= Qn;
            4'd8   : data_n[1] <= Qn;
            default : ;  //data_n <= 8'd0;
        endcase

    always @ (posedge adc_data_clk )
        //always @ (negedge word_sync_n)
        if (!word_sync_n) // Synch write
            word_reg <=        {data_p[8], data_n[8],
                data_p[7], data_n[7],
                data_p[6], data_n[6],
                data_p[5], data_n[5],
                data_p[4], data_n[4],
                data_p[3], data_n[3],
                data_p[2], data_n[2],
                data_p[1], data_n[1],
                Qp,        Qn};

    assign parallel_data =  word_reg ;

endmodule
