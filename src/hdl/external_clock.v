//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Company: INSTITUTO DOS PLASMAS E FUSAO NUCLEAR
// Engineer:  BBC
//
// Project Name:   W7X ATCA DAQ
// Design Name:    ATCA V2_DAQ FIRMWARE
// Module Name:    external_clocks
// Target Devices:
//
//Description: 10 Mhz External clock manager
//
// Copyright 2015 - 2019 IPFN-Instituto Superior Tecnico, Portugal
// Creation Date   Wed May 22 17:21:15 WEST 2019

//
// Licensed under the EUPL, Version 1.1 or - as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");

// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
//
// http://ec.europa.eu/idabc/eupl
//
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
//
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
`timescale 1ns/1ps
module external_clock (
    input  clk_10_ext,
	input  reset,
	output mmcm_locked,
    output clk_10
);
    wire  clk_600_fb, clk_10_mmcm;
    /* v7v8_mmcm_fvco_rule1: 
        The valid FVCO range for speed grade -2 is 600MHz to 1440MHz. The cell attribute values used to compute FVCO are CLKFBOUT_MULT_F = 50.000, 
        CLKIN1_PERIOD = 100.00000, and DIVCLK_DIVIDE = 1 (FVCO = 1000 * CLKFBOUT_MULT_F/(CLKIN1_PERIOD * DIVCLK_DIVIDE)).
        */
        MMCME2_BASE #(
            .BANDWIDTH("OPTIMIZED"),   // Jitter programming (OPTIMIZED, HIGH, LOW)
            //.CLKFBOUT_MULT_F(8.0),     // Multiply value for all CLKOUT (2.000-64.000).
            .CLKFBOUT_PHASE(0.0),      // Phase offset in degrees of CLKFB (-360.000-360.000).
            .CLKFBOUT_MULT_F(60.0),     // Multiply value for all CLKOUT (2.000-64.000).
            .CLKIN1_PERIOD(100.0),       // Input clock period in ns to ps resolution  ext_clk :10 MHz).
            // CLKOUT0_DIVIDE - CLKOUT6_DIVIDE: Divide amount for each CLKOUT (1-128)
            .CLKOUT0_DIVIDE_F(60.0),    // Divide amount for CLKOUT0 (1.000-128.000). 10 Mhz
            //      .CLKOUT0_DIVIDE(8),   //10 Mhz
            .CLKOUT1_DIVIDE(1),  
            .CLKOUT2_DIVIDE(1), 
            .CLKOUT3_DIVIDE(1),  
            .CLKOUT4_DIVIDE(1), 
            .CLKOUT5_DIVIDE(1),
            .CLKOUT6_DIVIDE(1),
            // CLKOUT0_DUTY_CYCLE - CLKOUT6_DUTY_CYCLE: Duty cycle for each CLKOUT (0.01-0.99).
            .CLKOUT0_DUTY_CYCLE(0.5),
            .CLKOUT1_DUTY_CYCLE(0.5),
            .CLKOUT2_DUTY_CYCLE(0.5),
            .CLKOUT3_DUTY_CYCLE(0.5),
            .CLKOUT4_DUTY_CYCLE(0.5),
            .CLKOUT5_DUTY_CYCLE(0.5),
            .CLKOUT6_DUTY_CYCLE(0.5),
            // CLKOUT0_PHASE - CLKOUT6_PHASE: Phase offset for each CLKOUT (-360.000-360.000).
            .CLKOUT0_PHASE(0.0),
            .CLKOUT1_PHASE(0.0),
            .CLKOUT2_PHASE(0.0),
            .CLKOUT3_PHASE(0.0),
            .CLKOUT4_PHASE(0.0),
            .CLKOUT5_PHASE(0.0),
            .CLKOUT6_PHASE(0.0),
            .CLKOUT4_CASCADE("FALSE"), // Cascade CLKOUT4 cnt_100_r with CLKOUT6 (FALSE, TRUE)
            .DIVCLK_DIVIDE(1),         // Master division value (1-106)
            .REF_JITTER1(0.0),         // Reference input jitter in UI (0.000-0.999).
            .STARTUP_WAIT("FALSE")     // Delays DONE until MMCM is locked (FALSE, TRUE)
        )
    MMCME2_BASE_10_osc (
        // Clock Outputs: 1-bit (each) output: User configurable clock outputs
        .CLKOUT0(clk_10_mmcm),     // 1-bit output: CLKOUT0
        .CLKOUT0B(),              // 1-bit output: Inverted CLKOUT0
        .CLKOUT1(),     // 1-bit output: CLKOUT1
        .CLKOUT1B(),             // 1-bit output: Inverted CLKOUT1
        .CLKOUT2(),     // 1-bit output: CLKOUT2
        .CLKOUT2B(),   // 1-bit output: Inverted CLKOUT2
        .CLKOUT3(),     // 1-bit output: CLKOUT3
        .CLKOUT3B(),   // 1-bit output: Inverted CLKOUT3
        .CLKOUT4(),     // 1-bit output: CLKOUT4
        .CLKOUT5(),     // 1-bit output: CLKOUT5
        .CLKOUT6(),     // 1-bit output: CLKOUT6
        // Feedback Clocks: 1-bit (each) output: Clock feedback ports
        .CLKFBOUT(clk_600_fb),   // 1-bit output: Feedback clock
        .CLKFBOUTB(), // 1-bit output: Inverted CLKFBOUT
        // Status Ports: 1-bit (each) output: MMCM status ports
        .LOCKED(mmcm_locked),       // 1-bit output: LOCK
        // Clock Inputs: 1-bit (each) input: Clock input
        .CLKIN1(clk_10_ext),       // 1-bit input: Clock
        // Control Ports: 1-bit (each) input: MMCM control ports
        .PWRDWN(1'b0),       // 1-bit input: Power-down
        .RST(reset),         // 1-bit input: Reset
        // Feedback Clocks: 1-bit (each) input: Clock feedback ports
        .CLKFBIN(clk_600_fb)      // 1-bit input: Feedback clock
    );

    BUFG bufg_10 ( .O(clk_10), .I(clk_10_mmcm) );

endmodule // 

