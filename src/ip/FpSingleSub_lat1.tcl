#https://grittyengineer.com/creating-vivado-ip-the-smart-tcl-way/
#
set path_ip [file dirname [info script]]

source $path_ip/../../scripts/part.tcl

## Create project
create_project -in_memory -part $part

set ip_name FpSingleSub_lat1

# Just in case
if { [file exists $path_ip/$ip_name/$ip_name.dcp]} {
    puts "file exist: $path_ip/$ip_name.dcp, delete it."
    file delete -force $path_ip/$ip_name/$ip_name.dcp
}

#create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 
create_ip -vlnv xilinx.com:ip:floating_point:7.1 \
    -module_name $ip_name -dir $path_ip -force

set_property -dict [list CONFIG.Add_Sub_Value {Subtract} \
    CONFIG.Flow_Control {Blocking} CONFIG.Axi_Optimize_Goal {Performance} \
    CONFIG.Has_RESULT_TREADY {false} CONFIG.Maximum_Latency {false} \
    CONFIG.C_Latency {1}] [get_ips $ip_name]

generate_target all [get_ips]

# Synthesize all the IP
synth_ip -force [get_ips]
