#https://grittyengineer.com/creating-vivado-ip-the-smart-tcl-way/
#
# set the reference directory to where the script is
#
set path_ip [file dirname [info script]]

source $path_ip/../../scripts/part.tcl
## Create project
create_project -in_memory -part $part

set ip_name DoubleAccum_lat2

# Just in case
if { [file exists $path_ip/$ip_name/$ip_name.dcp]} {
	puts "file exist: $path_ip/$ip_name.dcp, delete it."
	file delete -force $path_ip/$ip_name/$ip_name.dcp
}
#if { [file isdirectory ./$ip_name] } {
        ## if the IP files exist, remove files. (bug: synth_ip cannot overwrite .cp file
    #file delete -force -- {*}[glob $ip_name/*]
#}

#create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1
create_ip -vlnv xilinx.com:ip:floating_point:7.1 \
    -module_name $ip_name -dir $path_ip -force

set_property -dict [list CONFIG.Operation_Type {Accumulator} CONFIG.Add_Sub_Value {Add} \
    CONFIG.A_Precision_Type {Double} CONFIG.C_Accum_Msb {128} CONFIG.C_Accum_Lsb {-63} \
    CONFIG.C_Optimization {Low_Latency} CONFIG.C_Mult_Usage {Full_Usage} \
    CONFIG.Axi_Optimize_Goal {Performance} CONFIG.Has_RESULT_TREADY {false} \
    CONFIG.Maximum_Latency {false} CONFIG.C_Latency {2} CONFIG.Has_ARESETn {true} \
    CONFIG.C_Has_ACCUM_OVERFLOW {true} CONFIG.C_Has_ACCUM_INPUT_OVERFLOW {true} \
    CONFIG.Has_A_TUSER {false} CONFIG.C_A_Exponent_Width {11} \
    CONFIG.C_A_Fraction_Width {53} CONFIG.Result_Precision_Type {Double} \
    CONFIG.C_Result_Exponent_Width {11} CONFIG.C_Result_Fraction_Width {53} \
    CONFIG.C_Accum_Lsb {-63} CONFIG.C_Accum_Input_Msb {32} CONFIG.C_Rate {1} \
    CONFIG.Has_A_TLAST {true} CONFIG.RESULT_TLAST_Behv {Pass_A_TLAST}] [get_ips $ip_name]


generate_target all [get_ips]

# Synthesize the IP
synth_ip [get_ips]
