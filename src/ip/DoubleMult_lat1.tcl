#https://grittyengineer.com/creating-vivado-ip-the-smart-tcl-way/
#
set path_ip [file dirname [info script]]

source $path_ip/../../scripts/part.tcl

## Create project
create_project -in_memory -part $part

set ip_name DoubleMult_lat1

# Just in case
if { [file exists $path_ip/$ip_name/$ip_name.dcp]} {
    puts "file exist: $path_ip/$ip_name.dcp, delete it."
    file delete -force $path_ip/$ip_name/$ip_name.dcp
}

#create_ip -name floating_point -vendor xilinx.com -library ip -version 7.1 
create_ip -vlnv xilinx.com:ip:floating_point:7.1 \
    -module_name $ip_name -dir $path_ip -force

set_property -dict [list CONFIG.Component_Name {$ip_name} \
    CONFIG.Operation_Type {Multiply} \
    CONFIG.A_Precision_Type {Double} CONFIG.C_Optimization {Low_Latency} \
    CONFIG.Has_RESULT_TREADY {false} CONFIG.Maximum_Latency {false} \
    CONFIG.C_Latency {1} CONFIG.C_A_Exponent_Width {11} \
    CONFIG.C_A_Fraction_Width {53} CONFIG.Result_Precision_Type {Double} \
    CONFIG.C_Result_Exponent_Width {11} CONFIG.C_Result_Fraction_Width {53} \
    CONFIG.C_Accum_Msb {32} CONFIG.C_Accum_Lsb {-31} \
    CONFIG.C_Accum_Input_Msb {32} CONFIG.C_Mult_Usage {Max_Usage} \
    CONFIG.C_Rate {1}] [get_ips $ip_name]

generate_target all [get_ips]

# Synthesize all the IP
synth_ip -force [get_ips]
