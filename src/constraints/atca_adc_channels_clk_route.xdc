#  Poor placement for routing between an IO pin and BUFG. If this sub optimal condition is acceptable for this design, you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. These examples can be used directly in the .xdc file to override this clock rule.
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[3].channel_inst/BUF_adc_clk_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[8].channel_inst/BUF_adc_clk_0]

#	adc_data_producer_inst/adc_chan_gen[3].channel_inst/BUF_adc_clk (IBUFDS.O) is locked to IOB_X0Y62
#	 and IDDR_inst_i_1__2 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y4

# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[3].channel_inst/adc_clk_p[3]]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[8].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[9].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[10].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[11].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[13].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[14].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[15].channel_inst/adc_clk_p[15]]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[16].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[17].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[18].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[20].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[21].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[24].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[25].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[26].channel_inst/count100_reg[3]_0]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_data_producer_inst/adc_chan_gen[29].channel_inst/count100_reg[3]_0]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rtm_r1_i]
#[Place 30-575] Sub-optimal placement for a clock-capable IO pin and MMCM pair. If this sub optimal condition is acceptable for this design, you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. These examples can be used directly in the .xdc file to override this clock rule.
#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets te0741_clk_100_i]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk_10_atca_recv]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets atca_1a_i]

#[Place 30-120] Sub-optimal placement for a BUFG-BUFG cascade pair. If this sub optimal condition is acceptable for this design, 
# you may use the CLOCK_DEDICATED_ROUTE constraint in the .xdc file to demote this message to a WARNING. However, the use of this override is highly discouraged. 
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets atca_clock_inst/clk_50]
