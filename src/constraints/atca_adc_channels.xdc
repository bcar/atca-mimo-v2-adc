# Copyright 2019 - 2023 IPFN-Instituto Superior Tecnico, Portugal
#
# Licensed under the EUPL, Version 1.2 only (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence, available in 23 official languages of the European Union, at:
# https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

##-----------------------------------------------------------------------------
## Project    :  ATCA MIMO V2
## File       : atca_adc_channels.xdc
## Version    : 
#

###############################################################################
##### ADC MODULES DATA
###############################################################################

# Common rules
set_property IOSTANDARD LVDS_25 [get_ports adc_data_p[*]]
set_property IOSTANDARD LVDS_25 [get_ports adc_data_n[*]]
set_property IOSTANDARD LVDS_25 [get_ports adc_clk_p[*]]
set_property IOSTANDARD LVDS_25 [get_ports adc_clk_n[*]]
set_property PULLUP TRUE [get_ports adc_data_p[*]]
#set_property PULLUP TRUE [get_ports adc_clk_p[*]]
set_property PULLDOWN TRUE [get_ports adc_clk_n[*]]

#set_property PULLDOWN TRUE [get_ports adc_data_n[0]]

## New pins on ATCA v2.1
#channel 0
#B16_L17
set_property PACKAGE_PIN D14 [get_ports adc_data_p[0]]
set_property PACKAGE_PIN D13 [get_ports adc_data_n[0]]
# B16_L14
set_property PACKAGE_PIN E11 [get_ports adc_clk_p[0]]
set_property PACKAGE_PIN D11 [get_ports adc_clk_n[0]]
create_clock -name adc_s_clk0 -period 10 [get_ports adc_clk_p[0]]
#set_input_delay -clock adc_s_clk0 -max 3.5 [get_ports adc_data_p[0]] (had Setup -0.194ns) 
set_input_delay -clock adc_s_clk0 -max 3.3 [get_ports adc_data_p[0]]
set_input_delay -clock adc_s_clk0 -min 2.0 [get_ports adc_data_p[0]]
#set_input_delay -clock adc_s_clk0 -min 2.4 [get_ports adc_data_p[0]]

#channel 1
#bank 16 B16_L11
set_property PACKAGE_PIN G11 [get_ports adc_clk_p[1]]
set_property PACKAGE_PIN F10 [get_ports adc_clk_n[1]]
# B16_L18
set_property PACKAGE_PIN E13 [get_ports adc_data_p[1]]
set_property PACKAGE_PIN E12 [get_ports adc_data_n[1]]
create_clock -name adc_s_clk1 -period 10 [get_ports adc_clk_p[1]]
set_input_delay -clock adc_s_clk1 -max 3.5 [get_ports adc_data_p[1]]
set_input_delay -clock adc_s_clk1 -min 2.2 [get_ports adc_data_p[1]]

#channel 2 B16_L21dma_ip_drivers/-/blob/master/XDMA/linux-kernel/tools/dma_utils.c
set_property PACKAGE_PIN B14 [get_ports {adc_data_p[2]}]
set_property PACKAGE_PIN A14 [get_ports {adc_data_n[2]}]
#B16_L12
set_property PACKAGE_PIN E10 [get_ports {adc_clk_p[2]}]
set_property PACKAGE_PIN D10 [get_ports {adc_clk_n[2]}]
create_clock -name adc_s_clk2 -period 10 [get_ports adc_clk_p[2]]
# Setup - 0.539. set_input_delay -clock adc_s_clk2 -max 4.0 [get_ports adc_data_p[2]]
set_input_delay -clock adc_s_clk2 -max 3.4 [get_ports adc_data_p[2]]
set_input_delay -clock adc_s_clk2 -min 2.0 [get_ports adc_data_p[2]]


#channel 3  B15_L5 // getting data
set_property PACKAGE_PIN C17 [get_ports {adc_data_p[3]}]
set_property PACKAGE_PIN C18 [get_ports {adc_data_n[3]}]
###B13_L19
set_property PACKAGE_PIN T18 [get_ports {adc_clk_p[3]}]
set_property PACKAGE_PIN T19 [get_ports {adc_clk_n[3]}]
create_clock -name adc_s_clk3 -period 10 [get_ports adc_clk_p[3]]
# Setup - 1.990. set_input_delay -clock adc_s_clk3 -max 3.0 [get_ports adc_data_p[3]]
# Setup - 0.473. set_input_delay -clock adc_s_clk3 -max 3.0 [get_ports adc_data_p[3]]
set_input_delay -clock adc_s_clk3 -max 4.0 [get_ports adc_data_p[3]]
# Hold - 0.143. set_input_delay -clock adc_s_clk3 -min 3.3 [get_ports adc_data_p[3]]
set_input_delay -clock adc_s_clk3 -min 3.5 [get_ports adc_data_p[3]]

#channel 4
#B16_L23
set_property PACKAGE_PIN B15 [get_ports {adc_data_p[4]}]
set_property PACKAGE_PIN A15 [get_ports {adc_data_n[4]}]

#B16_L13
set_property PACKAGE_PIN C12 [get_ports {adc_clk_p[4]}]
set_property PACKAGE_PIN C11 [get_ports {adc_clk_n[4]}]
create_clock -name adc_s_clk4 -period 10 [get_ports adc_clk_p[4]]
set_input_delay -clock adc_s_clk4 -max 3.5 [get_ports adc_data_p[4]]
set_input_delay -clock adc_s_clk4 -min 2.2 [get_ports adc_data_p[4]]

# channel 5 B13_L21
set_property PACKAGE_PIN R16 [get_ports adc_data_p[5]]
set_property PACKAGE_PIN R17 [get_ports adc_data_n[5]]
#B13_L13
set_property PACKAGE_PIN R21 [get_ports adc_clk_p[5]]
set_property PACKAGE_PIN P21 [get_ports adc_clk_n[5]]
create_clock -name adc_s_clk5 -period 20.0 [get_ports adc_clk_p[5]]
# Setup - 0.635 set_input_delay -clock adc_s_clk5 -max 4.5 [get_ports adc_data_p[5]]
set_input_delay -clock adc_s_clk5 -max 5.2 [get_ports adc_data_p[5]]
set_input_delay -clock adc_s_clk5 -min 3.1 [get_ports adc_data_p[5]]

#channel 6
#B13_L15
set_property PACKAGE_PIN T24 [get_ports adc_data_p[6]]
set_property PACKAGE_PIN T25 [get_ports adc_data_n[6]]
# B13_L11
set_property PACKAGE_PIN P23 [get_ports adc_clk_p[6]]
set_property PACKAGE_PIN N23 [get_ports adc_clk_n[6]]
create_clock -name adc_s_clk6 -period 20.0 [get_ports adc_clk_p[6]]
set_input_delay -clock adc_s_clk6 -max 5.2 [get_ports adc_data_p[6]]
set_input_delay -clock adc_s_clk6 -min 3.3 [get_ports adc_data_p[6]]

#channel 7 B13_L4
set_property PACKAGE_PIN P24 [get_ports adc_data_p[7]]
set_property PACKAGE_PIN N24 [get_ports adc_data_n[7]]
#B13_L12
set_property PACKAGE_PIN N21  [get_ports adc_clk_p[7]]
set_property PACKAGE_PIN N22 [get_ports adc_clk_n[7]]
create_clock -name adc_s_clk7 -period 20.0 [get_ports adc_clk_p[7]]
set_input_delay -clock adc_s_clk7 -max 4.5 [get_ports adc_data_p[7]]
#exception
set_input_delay -clock adc_s_clk7 -min 3.2 [get_ports adc_data_p[7]]

#channel 8 B13_L3
set_property PACKAGE_PIN M25 [get_ports adc_data_p[8]]
set_property PACKAGE_PIN L25 [get_ports adc_data_n[8]]
# B13_L2
set_property PACKAGE_PIN R26 [get_ports adc_clk_p[8]]
set_property PACKAGE_PIN P26 [get_ports adc_clk_n[8]]
#channel_8_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y96
# and IDDR_inst_i_1__0 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y0
create_clock -name adc_s_clk8 -period 10.0 [get_ports adc_clk_p[8]]
#https://forums.xilinx.com/t5/Timing-Analysis/Off2set-In-and-offset-out-constraints-for-the-xdc/td-p/651407
set_input_delay -clock adc_s_clk8 -max 2.5 [get_ports adc_data_p[8]]
set_input_delay -clock adc_s_clk8 -min 0.2 [get_ports adc_data_p[8]]

#channel 9 B13_L1
set_property PACKAGE_PIN K25 [get_ports adc_data_p[9]]
set_property PACKAGE_PIN K26 [get_ports adc_data_n[9]]
# B13_L5
set_property PACKAGE_PIN N26 [get_ports adc_clk_p[9]]
set_property PACKAGE_PIN M26 [get_ports adc_clk_n[9]]
create_clock -name adc_s_clk9 -period 10.0 [get_ports adc_clk_p[9]]
#channel_9_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y90
set_input_delay -clock adc_s_clk9 -max 3.0 [get_ports adc_data_p[9]]
set_input_delay -clock adc_s_clk9 -min 0.3 [get_ports adc_data_p[9]]

#channel 10 B13_L17
set_property PACKAGE_PIN T22 [get_ports adc_data_p[10]]
set_property PACKAGE_PIN T23 [get_ports adc_data_n[10]]
# B13_L6
set_property PACKAGE_PIN R25 [get_ports adc_clk_p[10]]
set_property PACKAGE_PIN P25 [get_ports adc_clk_n[10]]
create_clock -name adc_s_clk10 -period 10.0 [get_ports adc_clk_p[10]]
#	channel_10_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y88
#	 and IDDR_inst_i_1__2 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y3
set_input_delay -clock adc_s_clk10 -max 2.0 [get_ports adc_data_p[10]]
set_input_delay -clock adc_s_clk10 -min 0.7 [get_ports adc_data_p[10]]

#channel 11 B13_L18 Getting Signal
set_property PACKAGE_PIN U19 [get_ports adc_data_p[11]]
set_property PACKAGE_PIN U20 [get_ports adc_data_n[11]]
# B13_L9
set_property PACKAGE_PIN P19 [get_ports adc_clk_p[11]]
set_property PACKAGE_PIN P20 [get_ports adc_clk_n[11]]
create_clock -name adc_s_clk11 -period 10.0 [get_ports adc_clk_p[11]]
#	 and IDDR_inst_i_1__1 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y2
set_input_delay -clock adc_s_clk11 -max 3.5 [get_ports adc_data_p[11]]
set_input_delay -clock adc_s_clk11 -min 1.5 [get_ports adc_data_p[11]]

#channel 12
# B13_L16
set_property PACKAGE_PIN T20 [get_ports adc_data_p[12]]
set_property PACKAGE_PIN R20 [get_ports adc_data_n[12]]
#B13_L14
set_property PACKAGE_PIN R22 [get_ports adc_clk_p[12]]
set_property PACKAGE_PIN R23 [get_ports adc_clk_n[12]]
create_clock -name adc_s_clk12 -period 10.0 [get_ports adc_clk_p[12]]
set_input_delay -clock adc_s_clk12 -max 2.5 [get_ports adc_data_p[12]]
set_input_delay -clock adc_s_clk12 -min 1.4 [get_ports adc_data_p[12]]

#channel 13 B13_L24
set_property PACKAGE_PIN R18 [get_ports adc_data_p[13]]
set_property PACKAGE_PIN P18 [get_ports adc_data_n[13]]
# B13_L22
set_property PACKAGE_PIN N18 [get_ports adc_clk_p[13]]
set_property PACKAGE_PIN M19 [get_ports adc_clk_n[13]]
create_clock -name adc_s_clk13 -period 10.0 [get_ports adc_clk_p[13]]
#	channel_13_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y56
#	 and IDDR_inst_i_1 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y0
set_input_delay -clock adc_s_clk13 -max 2.0 [get_ports adc_data_p[13]]
set_input_delay -clock adc_s_clk13 -min 0.4 [get_ports adc_data_p[13]]

#channel 14 B13_L7
set_property PACKAGE_PIN N19 [get_ports adc_data_p[14]]
set_property PACKAGE_PIN M20 [get_ports adc_data_n[14]]
# B13_L10
set_property PACKAGE_PIN M21 [get_ports adc_clk_p[14]]
set_property PACKAGE_PIN M22 [get_ports adc_clk_n[14]]
create_clock -name adc_s_clk14 -period 10.0 [get_ports adc_clk_p[14]]
#	channel_14_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y80
#	 and IDDR_inst_i_1__2 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y3
set_input_delay -clock adc_s_clk14 -max 3.0 [get_ports adc_data_p[14]]
set_input_delay -clock adc_s_clk14 -min 0.4 [get_ports adc_data_p[14]]

#channel 15 B13_L20 ,  getting  chopped signal
set_property PACKAGE_PIN P16 [get_ports adc_data_p[15]]
set_property PACKAGE_PIN N17 [get_ports adc_data_n[15]]
# B13_L8
set_property PACKAGE_PIN M24 [get_ports adc_clk_p[15]]
set_property PACKAGE_PIN L24 [get_ports adc_clk_n[15]]
create_clock -name adc_s_clk15 -period 10.0 [get_ports adc_clk_p[15]]
#	channel_15_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y84
#	 and IDDR_inst_i_1__1 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y2
set_input_delay -clock adc_s_clk15 -max 3.0 [get_ports adc_data_p[15]]
set_input_delay -clock adc_s_clk15 -min 0.7 [get_ports adc_data_p[15]]

##channel 16 B12_L21 , not getting data line
set_property PACKAGE_PIN AD26 [get_ports adc_data_p[16]]
set_property PACKAGE_PIN AE26 [get_ports adc_data_n[16]]
## B12_L16
set_property PACKAGE_PIN AD23 [get_ports adc_clk_p[16]]
set_property PACKAGE_PIN AD24 [get_ports adc_clk_n[16]]
create_clock -name adc_s_clk16 -period 10.0 [get_ports adc_clk_p[16]]
##	channel_16_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y18
##	 and IDDR_inst_i_1__0 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y1
set_input_delay -clock adc_s_clk16 -max 3.0 [get_ports adc_data_p[16]]
set_input_delay -clock adc_s_clk16 -min 0.4 [get_ports adc_data_p[16]]

##channel 17 B12_L9 , getting  chopped signal
set_property PACKAGE_PIN AB26 [get_ports adc_data_p[17]]
set_property PACKAGE_PIN AC26 [get_ports adc_data_n[17]]
## B12_L23
set_property PACKAGE_PIN AD25 [get_ports adc_clk_p[17]]
set_property PACKAGE_PIN AE25 [get_ports adc_clk_n[17]]
create_clock -name adc_s_clk17 -period 10.0 [get_ports adc_clk_p[17]]
##	channel_17_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y4
set_input_delay -clock adc_s_clk17 -max 3.5 [get_ports adc_data_p[17]]
set_input_delay -clock adc_s_clk17 -min 1.1 [get_ports adc_data_p[17]]

##channel 18 B12_L5 getting  chopped signal
set_property PACKAGE_PIN W25 [get_ports adc_data_p[18]]
set_property PACKAGE_PIN W26 [get_ports adc_data_n[18]]
## B12_L8
set_property PACKAGE_PIN W23 [get_ports adc_clk_p[18]]
set_property PACKAGE_PIN W24 [get_ports adc_clk_n[18]]
create_clock -name adc_s_clk18 -period 10.0 [get_ports adc_clk_p[18]]
##set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_chan_gen[18].channel_inst/count100_reg[3]_0]
##	channel_18_inst/IBUFGDS_adc_clk (IBUFDS.O) is locked to IOB_X0Y34
##	 and IDDR_inst_i_1__1 (BUFG.I) is provisionally placed by clockplacer on BUFGCTRL_X0Y2
set_input_delay -clock adc_s_clk18 -max 3.0 [get_ports adc_data_p[18]]
set_input_delay -clock adc_s_clk18 -min 0.4 [get_ports adc_data_p[18]]

##channel #19
## B12_L3
set_property PACKAGE_PIN V23 [get_ports adc_data_p[19]]
set_property PACKAGE_PIN V24 [get_ports adc_data_n[19]]
##B12_L11
set_property PACKAGE_PIN AA23  [get_ports adc_clk_p[19]]
set_property PACKAGE_PIN AB24 [get_ports adc_clk_n[19]]
create_clock -name adc_s_clk19 -period 10.0 [get_ports adc_clk_p[19]]
set_input_delay -clock adc_s_clk19 -max 2.0 [get_ports adc_data_p[19]]
set_input_delay -clock adc_s_clk19 -min 1.4 [get_ports adc_data_p[19]]

##channel 20 B12_L1
set_property PACKAGE_PIN U22 [get_ports adc_data_p[20]]
set_property PACKAGE_PIN V22 [get_ports adc_data_n[20]]
## B12_L2
set_property PACKAGE_PIN U24 [get_ports adc_clk_p[20]]
set_property PACKAGE_PIN U25 [get_ports adc_clk_n[20]]
create_clock -name adc_s_clk20 -period 10.0 [get_ports adc_clk_p[20]]
##set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_chan_gen[20].channel_inst/count100_reg[3]_0]
set_input_delay -clock adc_s_clk20 -max 3.0 [get_ports adc_data_p[20]]
set_input_delay -clock adc_s_clk20 -min 0.3 [get_ports adc_data_p[20]]

##channel 21
##B15_L21
set_property PACKAGE_PIN L19 [get_ports adc_data_p[21]]
set_property PACKAGE_PIN L20 [get_ports adc_data_n[21]]
## B15_L23
set_property PACKAGE_PIN M17 [get_ports adc_clk_p[21]]
set_property PACKAGE_PIN L18 [get_ports adc_clk_n[21]]
create_clock -name adc_s_clk21 -period 10.0 [get_ports adc_clk_p[21]]
##set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_chan_gen[21].channel_inst/count100_reg[3]_0]
set_input_delay -clock adc_s_clk21 -max 3.0 [get_ports adc_data_p[21]]
set_input_delay -clock adc_s_clk21 -min 0.5 [get_ports adc_data_p[21]]

##channel 22
## B15_L20
set_property PACKAGE_PIN J18 [get_ports adc_data_p[22]]
set_property PACKAGE_PIN J19 [get_ports adc_data_n[22]]
## B15_L12
set_property PACKAGE_PIN F17 [get_ports adc_clk_p[22]]
set_property PACKAGE_PIN E17 [get_ports adc_clk_n[22]]
create_clock -name adc_s_clk22 -period 10.0 [get_ports adc_clk_p[22]]
set_input_delay -clock adc_s_clk22 -max 3.5 [get_ports adc_data_p[22]]
set_input_delay -clock adc_s_clk22 -min 1.6 [get_ports adc_data_p[22]]

##channel 23
##B15_L7
set_property PACKAGE_PIN H16 [get_ports adc_data_p[23]]
set_property PACKAGE_PIN G16 [get_ports adc_data_n[23]]
##B15_L11
set_property PACKAGE_PIN G17 [get_ports adc_clk_p[23]]
set_property PACKAGE_PIN F18 [get_ports adc_clk_n[23]]
create_clock -name adc_s_clk23 -period 10.0 [get_ports adc_clk_p[23]]
set_input_delay -clock adc_s_clk23 -max 3.5 [get_ports adc_data_p[23]]
set_input_delay -clock adc_s_clk23 -min 1.6 [get_ports adc_data_p[23]]

##channel 24 B12_L19
set_property PACKAGE_PIN AD21 [get_ports adc_data_p[24]]
set_property PACKAGE_PIN AE21 [get_ports adc_data_n[24]]
## B12_L24
set_property PACKAGE_PIN AE22 [get_ports adc_clk_p[24]]
set_property PACKAGE_PIN AF22 [get_ports adc_clk_n[24]]
create_clock -name adc_s_clk24 -period 10.0 [get_ports adc_clk_p[24]]
set_input_delay -clock adc_s_clk24 -max 3.0 [get_ports adc_data_p[24]]
set_input_delay -clock adc_s_clk24 -min 0.5 [get_ports adc_data_p[24]]

##channel 25 B12_L22,
set_property PACKAGE_PIN AE23 [get_ports adc_data_p[25]]
set_property PACKAGE_PIN AF23 [get_ports adc_data_n[25]]
## B12_L20
set_property PACKAGE_PIN AF24 [get_ports adc_clk_p[25]]
set_property PACKAGE_PIN AF25 [get_ports adc_clk_n[25]]
create_clock -name adc_s_clk25 -period 10.0 [get_ports adc_clk_p[25]]
set_input_delay -clock adc_s_clk25 -max 3.0 [get_ports adc_data_p[25]]
set_input_delay -clock adc_s_clk25 -min 0.3 [get_ports adc_data_p[25]]

##channel 26 B12_L17,
set_property PACKAGE_PIN AB22 [get_ports adc_data_p[26]]
set_property PACKAGE_PIN AC22 [get_ports adc_data_n[26]]
## B12_L18
set_property PACKAGE_PIN AB21 [get_ports adc_clk_p[26]]
set_property PACKAGE_PIN AC21 [get_ports adc_clk_n[26]]
create_clock -name adc_s_clk26 -period 10.0 [get_ports adc_clk_p[26]]
set_input_delay -clock adc_s_clk26 -max 3.0 [get_ports adc_data_p[26]]
set_input_delay -clock adc_s_clk26 -min 0.3 [get_ports adc_data_p[26]]

##channel 27
##B12_L10,
set_property PACKAGE_PIN Y25 [get_ports adc_data_p[27]]
set_property PACKAGE_PIN Y26 [get_ports adc_data_n[27]]
##B12_L14
set_property PACKAGE_PIN AC23 [get_ports adc_clk_p[27]]
set_property PACKAGE_PIN AC24 [get_ports adc_clk_n[27]]
create_clock -name adc_s_clk27 -period 10.0 [get_ports adc_clk_p[27]]
set_input_delay -clock adc_s_clk27 -max 3.0 [get_ports adc_data_p[27]]
set_input_delay -clock adc_s_clk27 -min 1.4 [get_ports adc_data_p[27]]

##channel 28
## B12_L7
set_property PACKAGE_PIN AA25 [get_ports adc_data_p[28]]
set_property PACKAGE_PIN AB25 [get_ports adc_data_n[28]]
## B12_L13
set_property PACKAGE_PIN Y22 [get_ports adc_clk_p[28]]
set_property PACKAGE_PIN AA22 [get_ports adc_clk_n[28]]
create_clock -name adc_s_clk28 -period 10.0 [get_ports adc_clk_p[28]]
set_input_delay -clock adc_s_clk28 -max 3.5 [get_ports adc_data_p[28]]
set_input_delay -clock adc_s_clk28 -min 1.4 [get_ports adc_data_p[28]]

##channel 29 B12_L4 , getting chopped signal
set_property PACKAGE_PIN U26 [get_ports adc_data_p[29]]
set_property PACKAGE_PIN V26 [get_ports adc_data_n[29]]
## B12_L12
set_property PACKAGE_PIN Y23 [get_ports adc_clk_p[29]]
set_property PACKAGE_PIN AA24 [get_ports adc_clk_n[29]]
create_clock -name adc_s_clk29 -period 10.0 [get_ports adc_clk_p[29]]
##set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets adc_chan_gen[29].channel_inst/count100_reg[3]_0]
set_input_delay -clock adc_s_clk29 -max 3.5 [get_ports adc_data_p[29]]
set_input_delay -clock adc_s_clk29 -min 1.4 [get_ports adc_data_p[29]]

##channel 30
##B12_L15
set_property PACKAGE_PIN W20 [get_ports adc_data_p[30]]
set_property PACKAGE_PIN Y21 [get_ports adc_data_n[30]]
##B15_L13
set_property PACKAGE_PIN E18 [get_ports adc_clk_p[30]]
set_property PACKAGE_PIN D18 [get_ports adc_clk_n[30]]
create_clock -name adc_s_clk30 -period 10.0 [get_ports adc_clk_p[30]]
set_input_delay -clock adc_s_clk30 -max 3.5 [get_ports adc_data_p[30]]
set_input_delay -clock adc_s_clk30 -min 2.5 [get_ports adc_data_p[30]]

##channel 31
## B12_L6
set_property PACKAGE_PIN V21 [get_ports adc_data_p[31]]
set_property PACKAGE_PIN W21 [get_ports adc_data_n[31]]
##B15_L13
##set_property PACKAGE_PIN E18 [get_ports adc_data_p[31]]
##set_property PACKAGE_PIN D18 [get_ports adc_data_n[31]]
## B15_L14
set_property PACKAGE_PIN H17 [get_ports adc_clk_p[31]]
set_property PACKAGE_PIN H18 [get_ports adc_clk_n[31]]
create_clock -name adc_s_clk31 -period 10.0 [get_ports adc_clk_p[31]]
set_input_delay -clock adc_s_clk31 -max 3.5 [get_ports adc_data_p[31]]
set_input_delay -clock adc_s_clk31 -min 2.5 [get_ports adc_data_p[31]]

# tight_setup_hold_pins
# relax setup and hold requirements
## Relax timing ADC. Register Data are static when sampled by clk 100 Mhz
# set_multicycle_path -setup 2 -from [get_clocks adc_s_clk*] -to [get_clocks main_clk100]
# set_multicycle_path -hold 1  -from [get_clocks adc_s_clk*] -to [get_clocks main_clk100]

# set_clock_groups -asynchronous -group {adc_s_clk*} -group {adc_clk50}
set_false_path -from [get_clocks adc_clk50] -to [get_clocks adc_s_clk*]
set_false_path -from  [get_clocks adc_s_clk*] -to [get_clocks adc_clk50]

set_property IODELAY_GROUP IDELAYCTRLADC [get_cells -hierarchical IDELAY*]
#  set_property IODELAY_GROUP IDELAYCTRL1 [get_cells  ]

#####################################################################
# End
#####################################################################
#  vim: set ts=8 sw=4 tw=0 et :
