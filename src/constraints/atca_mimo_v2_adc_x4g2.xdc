# Copyright 2019 - 2023 IPFN-Instituto Superior Tecnico, Portugal
#
# Licensed under the EUPL, Version 1.2 only (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence, available in 23 official languages of the European Union, at:
# https://joinup.ec.europa.eu/community/eupl/og_page/eupl-text-11-12
# Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

##-----------------------------------------------------------------------------
## Project    :  ATCA MIMO V2
## File       : atca_mimo_v2_adc_x4g2.xdc
## Version    : 
#
###############################################################################
# User Configuration
# Link Width   - x4
# Link Speed   - gen2
# Family       - kintex7
# Part         - xc7k325t
# Package      - fbg676
# Speed grade  - -2
# PCIe Block   - X0Y0
# Tool versions:  Vivado 2022.1
#
# Trenz Module has a SPI Memory type s25fl256sxxxxxx0-spi-x1_x2_x4
###############################################################################
#
###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

###############################################################################
# User Physical Constraints
###############################################################################


###############################################################################
# Pinout and Related I/O Constraints
###############################################################################

#
# SYS reset (input) signal.  The sys_reset_n signal should be
# obtained from the PCI Express interface if possible.  For
# slot based form factors, a system reset signal is usually
# present on the connector.  For cable based form factors, a
# system reset signal may not be available.  In this case, the
# system reset signal must be generated locally by some form of
# supervisory circuit.  You may change the IOSTANDARD and LOC
# to suit your requirements and VCCO voltage banking rules.
# Some 7 series devices do not have 3.3 V I/Os available.
# Therefore the appropriate level shift is required to operate
# with these devices that contain only 1.8 V banks.
#

# PCIe sys Reset is taken form the ATCA RX_3A pin
#set_property IOSTANDARD LVCMOS25 [get_ports sys_rst_n]

# bank 12 ATCA PCIe reset_n signal, not available in AXI crates
set_property IOSTANDARD LVCMOS25 [get_ports atca_3a_r]
set_property LOC U21 [get_ports atca_3a_r]
set_property PULLUP true [get_ports atca_3a_r]
set_false_path -from [get_ports atca_3a_r]

#
# SYS clock 100 MHz (input) signal. The sys_clk_p and sys_clk_n
# signals are the PCI Express reference clock. Virtex-7 GT
# Transceiver architecture requires the use of a dedicated clock
# resources (FPGA input pins) associated with each GT Transceiver.
# To use these pins an IBUFDS primitive (refclk_ibuf) is
# instantiated in user's design.
# Please refer to the Virtex-7 GT Transceiver User Guide
# (UG) for guidelines regarding clock resource selection.
#
# UG476 (v1.12) December 19, 2016
# 7 Series FPGAs GTX/GTH Transceivers User Guide pg. 347
# Figure A-2: Placement Diagram for the FBG676 Package
# D6 / D5
set_property LOC IBUFDS_GTE2_X0Y1 [get_cells refclk_ibuf]

#MGT_REFCLK0_D5_N
set_property PACKAGE_PIN D6 [get_ports sys_clk_p]
set_property PACKAGE_PIN D5 [get_ports sys_clk_n]

###############################################################################
# Timing Constraints
###############################################################################
#
create_clock -name sys_clk -period 10 [get_ports sys_clk_p]

# Replaces userclk2
# https://docs.xilinx.com/r/en-US/ug903-vivado-using-constraints/Limitations
# report_clocks -file /tmp/clocks.txt
create_generated_clock -name axi_clk [get_pins xdma_id0032_i/inst/xdma_id0032_pcie2_to_pcie3_wrapper_i/pcie2_ip_i/inst/inst/gt_top_i/pipe_wrapper_i/pipe_clock_int.pipe_clock_i/mmcm_i/CLKOUT3]
# XDMA main clocks are
#
#  userclk1               {0.000 2.000}        4.000           250.000 MHz
#  userclk2               {0.000 4.000}        8.000           125.000 MHz
# Rename clk_50_mmcm
# create_generated_clock -name adc_dt_clk [get_pins system_clocks_inst/mmcm_sc_100_inst/CLKOUT2]
#  clk_50_atca_i      | clk_50_te741_i
#  https://support.xilinx.com/s/article/53850?language=en_US

#create_generated_clock -name adc_clk50 [get_pins adc_data_clk]
create_clock -name adc_clk50 -period 20 [get_pins bufgmux_50M_inst/O]
#create_clock -name adc_clk50 -period 20 [get_pins bufg_data_clk50/O]
# create_clock -name adc_clk50 -period 20 [get_nets adc_data_clk]

set_clock_groups -physically_exclusive -group clk_50_atca_i -group clk_50_te741_i

set_clock_groups -physically_exclusive -group clk_10_te741 -group clk_10_rtm

# XDMA using the internal constraints from the IP core
#
##############################################################################
# Tandem Configuration Constraints
###############################################################################
set_input_delay -clock axi_clk -min 0.0 [get_ports atca_3a_r]
set_input_delay -clock axi_clk -max 2.0 [get_ports atca_3a_r]
#set_false_path -from [get_ports sys_rst_n]

################################################################################
#### Physical Pins on  Trenz TE0741
################################################################################

#### 100 MHz input clk on  Trenz TE0741
# bank 14
set_property LOC F22 [get_ports te0741_clk_100_p]
set_property LOC E23 [get_ports te0741_clk_100_n]
set_property IOSTANDARD LVDS_25 [get_ports te0741_clk_100_p]
set_property IOSTANDARD LVDS_25 [get_ports te0741_clk_100_n]
create_clock -name te0741_clk100 -period 10.00 [get_ports te0741_clk_100_p]

set_property IOSTANDARD LVCMOS33 [get_ports te0741_en_clk]
set_property LOC C26 [get_ports te0741_en_clk]
set_false_path -to [get_ports te0741_en_clk]

#bank 14, #To enable the voltage supply for the GTX transceivers, namely the Enpirion EP53F8QI voltage regulators U6 and U16, which serve the voltages MGTAVCC (1.0 V) and MGTAVTT (1.2 V), the signal EN_MGT (bank 14, pin H22) have to be set high
set_property LOC H22 [get_ports te0741_en_mgt]
set_property IOSTANDARD LVCMOS33 [get_ports te0741_en_mgt]
set_false_path -to [get_ports te0741_en_mgt]

# bank 14
set_property PACKAGE_PIN D26 [get_ports te0741_led1]
set_property IOSTANDARD LVCMOS33 [get_ports te0741_led1]
set_property PACKAGE_PIN E26 [get_ports te0741_led2]
set_property IOSTANDARD LVCMOS33 [get_ports te0741_led2]
set_false_path -to [get_ports te0741_led1]
# Dont care...
set_output_delay -clock te0741_clk100 -max 2.0 [get_ports te0741_led1]
set_output_delay -clock te0741_clk100 -min 1.0 [get_ports te0741_led1]

create_clock -name atca_clk10 -period 100.0 [get_ports atca_1a_r]
set_false_path -to [get_ports te0741_led2]
set_output_delay -clock atca_clk10 -max 2.0 [get_ports te0741_led2]
set_output_delay -clock atca_clk10 -min 1.0 [get_ports te0741_led2]

# bank 16
set_property PACKAGE_PIN H11 [get_ports atca_led_d7]
set_property IOSTANDARD LVCMOS25 [get_ports atca_led_d7]
set_false_path -to [get_ports atca_led_d7]
set_output_delay -clock atca_clk10  -min 0.0 [get_ports atca_led_d7]
set_output_delay -clock atca_clk10  -max 2.0 [get_ports atca_led_d7]

####################################################################
#   ATCA Clk SIGNALS
####################################################################
create_clock -name rtm_clk10 -period 100.0 [get_ports rtm_io_r1]

#B15_VREF // ATCA Master Shared clock output
set_property IOSTANDARD LVCMOS25 [get_ports atca_1a_d]
set_property PACKAGE_PIN  J20 [get_ports atca_1a_d]
# SLEW =  "SLOW"  is default
set_property SLEW SLOW [get_ports atca_1a_d]
set_property DRIVE 12 [get_ports atca_1a_d]

set_output_delay -clock rtm_clk10  -min 0.0 [get_ports atca_1a_d]
set_output_delay -clock rtm_clk10  -max 2.0 [get_ports atca_1a_d]

#A24    B14_L4_N // ATCA Shared clock input
set_property IOSTANDARD LVCMOS33 [get_ports atca_1a_r]
set_property PACKAGE_PIN  A24 [get_ports atca_1a_r]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets atca_1a_i]

#set_input_delay -clock atca_clk10 -max 5.0 [get_ports atca_1a_r]
#set_input_delay -clock atca_clk10 -min 0.0 [get_ports atca_1a_r]

#  B12_L25
set_property IOSTANDARD LVCMOS25 [get_ports atca_1b_d]
set_property PACKAGE_PIN Y20 [get_ports atca_1b_d]
#set_property SLEW FAST [get_ports atca_1b_d]
set_property SLEW SLOW [get_ports atca_1b_d]
set_property DRIVE 12 [get_ports atca_1b_d]

set_output_delay -clock rtm_clk10 -min 1.0 [get_ports atca_1b_d]
set_output_delay -clock rtm_clk10 -max 2.0 [get_ports atca_1b_d]
#set_output_delay -clock main_clk100 -min 1.0 [get_ports atca_clk1b_d]

# B14_L13_N // ATCA Master Shared clock intput
set_property IOSTANDARD LVCMOS33    [get_ports atca_1b_r]
set_property PACKAGE_PIN  F23       [get_ports atca_1b_r]
set_input_delay -clock atca_clk10 -max 2.0 [get_ports atca_1b_r]
set_input_delay -clock atca_clk10 -min 0.0 [get_ports atca_1b_r]

################################################################################
##### ADC MODULES COMMON CLOCKS
###############################################################################
#bank 14
set_property PACKAGE_PIN K21 [get_ports adcs_reset]
set_property IOSTANDARD LVCMOS33 [get_ports adcs_reset]
#set_property DRIVE 16 [get_ports ADCs_RESET]
set_property PACKAGE_PIN G21 [get_ports adcs_power_down]
set_property IOSTANDARD LVCMOS33 [get_ports adcs_power_down]
# The adc_data_clk is not transmited...
#set_output_delay -clock clk_50_mmcm -max 2.5 [get_ports adcs_power_down]
#set_output_delay -clock clk_50_mmcm -min 0.0 [get_ports adcs_power_down]
set_output_delay -clock adc_clk50 -max 2.5 [get_ports adcs_power_down]
set_output_delay -clock adc_clk50 -min 0.0 [get_ports adcs_power_down]

#bank 13
set_property IOSTANDARD LVDS_25 [get_ports adcs_start_conv_p]
set_property IOSTANDARD LVDS_25 [get_ports adcs_start_conv_n]
set_property PACKAGE_PIN U17 [get_ports adcs_start_conv_p]
set_property PACKAGE_PIN T17 [get_ports adcs_start_conv_n]
set_output_delay -clock adc_clk50  -min 0.0 [get_ports adcs_start_conv_p]
set_output_delay -clock adc_clk50  -max 5.0 [get_ports adcs_start_conv_p]

################################################################################
##### IMPC I2C SLAVE INTERFACE
###############################################################################
#bank 14
set_property PACKAGE_PIN G24 [get_ports fpga_sda]
set_property IOSTANDARD LVCMOS33 [get_ports fpga_sda]
#set_output_delay -clock adc_clk50 -max 2.5 [get_ports fpga_sda]
#set_output_delay -clock adc_clk50 -min 1.5 [get_ports fpga_sda]
set_input_delay -clock axi_clk -max 2.2 [get_ports fpga_sda]
set_input_delay -clock axi_clk -min 1.0 [get_ports fpga_sda]
set_output_delay -clock axi_clk -max 0.5 [get_ports fpga_sda]
set_output_delay -clock axi_clk -min 0.0 [get_ports fpga_sda]
# relax timing analisys 
set_false_path -from [get_ports fpga_sda]
set_false_path -to [get_ports fpga_sda]
set_false_path -from   [get_pins u_i2cSlave/u_serialInterface/sdaOut_reg_replica/C ] 
#set_multicycle_path -setup 2.4 -from [get_pins u_i2cSlave/u_serialInterface/sdaOut_reg_replica/C] -to [get_ports fpga_sda] # 3.0 -> 2.4 (was Setup -0.578) 
#set_multicycle_path -hold 1.4  -from [get_pins u_i2cSlave/u_serialInterface/sdaOut_reg_replica/C] -to [get_ports fpga_sda]

set_property PACKAGE_PIN H21 [get_ports fpga_scl]
set_property IOSTANDARD LVCMOS33 [get_ports fpga_scl]
set_input_delay -clock axi_clk -max 2.2 [get_ports fpga_scl]
set_input_delay -clock axi_clk -min 1.0 [get_ports fpga_scl]
#set_false_path -from [get_ports fpga_scl]

################################################################################
##### RTM Signals
###############################################################################

#bank 14
#MIO10 (BOTTOM SIDE side A32)  RTM ADC CLOCK OUT
set_property PACKAGE_PIN L22 [get_ports rtm_io_d5]
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_d5]
set_output_delay -clock adc_clk50 -max 1.6 [get_ports rtm_io_d5]
set_output_delay -clock adc_clk50 -min 0.0 [get_ports rtm_io_d5]
#set_output_delay -clock clk_50_mmcm -max 2.5 [get_ports rtm_io_d5]
#set_output_delay -clock clk_50_mmcm -min 0.0 [get_ports rtm_io_d5]

#MIO14 (BOTTOM SIDE side A36)   RTM CHOPPER ADC OUT / INTERLOCK "dF" OUT
set_property PACKAGE_PIN J21 [get_ports rtm_io_d6]
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_d6]
set_output_delay -clock adc_clk50 -max 1.0 [get_ports rtm_io_d6]
set_output_delay -clock adc_clk50 -min 0.0 [get_ports rtm_io_d6]
#set_output_delay -clock clk_50_mmcm -max 2.5 [get_ports rtm_io_d6]
#set_output_delay -clock clk_50_mmcm -min 0.0 [get_ports rtm_io_d6]

#fpga MIO13 (BOTTOM SIDE side A19 ) RTM 10 MHz CLOCK IN
set_property PACKAGE_PIN K22 [get_ports rtm_io_r1]
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_r1]

#MIO12 (top side B29)  RTM TRIGGER  IN
set_property PACKAGE_PIN H23 [get_ports rtm_io_r4]
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_r4]

#G22	B14_L13_P      RTM INTERLOCK OUT
set_property PACKAGE_PIN G22 [get_ports rtm_io_d7]
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_d7]
set_output_delay -clock adc_clk50 -max 2.5 [get_ports rtm_io_d7]
set_output_delay -clock adc_clk50 -min 0.0 [get_ports rtm_io_d7]
#A23	B14_L4_P        RTM "free" input
set_property IOSTANDARD LVCMOS33 [get_ports rtm_io_r8]
set_property PACKAGE_PIN A23 [get_ports rtm_io_r8]
set_input_delay -clock axi_clk -min 0.0 [get_ports rtm_io_r8]
set_input_delay -clock axi_clk -max 2.0 [get_ports rtm_io_r8]
# For now...
set_false_path -from [get_ports rtm_io_r8]

#
# Timing ignoring the below pins to avoid CDC analysis, but care has been taken in RTL to sync properly to other clock domain.
#disables the paths from async clocks,
# Avoids tight_setup_hold_pins as
#  userclk1 |       clk_100_mmcm |adc_data_producer_inst/FpTerms[4].FpSingleMult_lat1_inst/U0/i_synth/MULT.OP/i_non_prim.OP/RESULT_REG.NORMAL.exp_op_reg[7]/D|
#set_false_path -from [get_clocks userclk1] -to [get_clocks main_clk100]
#set_false_path -from [get_clocks main_clk100] -to [get_clocks userclk1]
#set_clock_groups  -name  usrclk1_main_clk -asynchronous \
#-group {userclk1} \
#-group {adc_clk50}


#set_false_path -from [get_nets acq_on_r] -to [get_nets acq_on_q]
set_false_path -from [get_cells  acq_on_r_reg] -to [get_cells acq_on_q_reg]
# These regs are stable during Acquisition
set_false_path -from [get_pins {shapi_regs_inst/control_r_reg[*]}]
set_false_path -from [get_pins {shapi_regs_inst/control_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/dev_control_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/ilck_param_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/eo_offset_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/wo_offset_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/chopp_period_r_reg[*]/C}]
set_false_path -from [get_pins {shapi_regs_inst/channel_mask_r_reg[*]/C}]


# UG903 pg 122
#set_multicycle_path -setup 2 -from [get_clocks clk_50_mmcm] -to [get_clocks main_clk100]
    #set_multicycle_path -hold 1  -from [get_clocks clk_50_mmcm] -to [get_clocks main_clk100]
# Intra-Clock Paths Setup violations
#set_multicycle_path -from [get_pins {adc_data_producer_inst/accum_ina_1_reg[*]_C/C}] \
    #-to [get_pins {adc_data_producer_inst/FpSingleAccum_lat1_inst1/U0/i_synth/ACCUM_OP.OP/i_result/i_pipe/opt_has_pipe.first_q_reg[*]/D}] 2

#set_clock_groups -group userclk1 -group clk_100_mmcm
####################################################################
#  Bitstream and SPI mcs generation
####################################################################
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 66 [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.M1PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M2PIN PULLNONE [current_design]
set_property BITSTREAM.CONFIG.M0PIN PULLNONE [current_design]

set_property BITSTREAM.CONFIG.USR_ACCESS TIMESTAMP [current_design]

#####################################################################
# End
#####################################################################
#  vim: set ts=8 sw=4 tw=0 et :