9/9/19
256 samples/pckt
Running in ATCA v2 . Need to run in isolated core (app/irq)
still missing some data on initial packet
emory mapped at address 0x7fd61eac0000.
Err pckt magic: 0, header: 0x0, lastheader: 0x0, footer: 0x0
 Err pckt seq: 0, header: 0x0, lastheader: 0x0, footer: 0x0, magic: 0x0, fmagic: 0x54310100
max_buffer_count: 0,  1 Npackets in 238 microseconds

use report_exceptions

report timing -from [all_inputs]
set my_inout
set my_output fpga_sda 
set_input_delay -clock
set_input_delay -clock adc_s_clk31 -min 0.5 [get_ports adc_data_p[31]]

set_output_delay -clock clk_100_mmcm -max 3.0 [get_ports fpga_sda]
report_timing -from $my_input -setup
report_timing -from adc_data_p[0] -hold

Command      : report_timing -from app/PIO/PIO_EP_inst/pci_dma_engine_inst/dma_fifo_1/gnuram_async_fifo.xpm_fifo_base_inst/gen_pf_ic_rc.gpf_ic.prog_full_i_reg/C
| Design       : atca_mimo_v2_adc
| Device       : 7k325t-fbg676
| Speed File   : -2  PRODUCTION 1.12 2017-02-17
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Timing Report

Slack (VIOLATED) :        -0.201ns  (required time - arrival time)
  Source:                 app/PIO/PIO_EP_inst/pci_dma_engine_inst/dma_fifo_1/gnuram_async_fifo.xpm_fifo_base_inst/gen_pf_ic_rc.gpf_ic.prog_full_i_reg/C
                            (rising edge-triggered cell FDRE clocked by clk_100_mmcm  {rise@0.000ns fall@5.000ns period=10.000ns})
  Destination:            app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata_reg[63]/D
                            (rising edge-triggered cell FDRE clocked by userclk1  {rise@0.000ns fall@2.000ns period=4.000ns})
  Path Group:             userclk1
  Path Type:              Setup (Max at Slow Process Corner)
  Requirement:            2.000ns  (userclk1 rise@12.000ns - clk_100_mmcm rise@10.000ns)
  Data Path Delay:        2.601ns  (logic 0.395ns (15.186%)  route 2.206ns (84.814%))
  Logic Levels:           4  (LUT5=2 LUT6=2)
  Clock Path Skew:        0.647ns (DCD - SCD + CPR)
    Destination Clock Delay (DCD):    5.964ns = ( 17.964 - 12.000 ) 
    Source Clock Delay      (SCD):    5.317ns = ( 15.317 - 10.000 ) 
    Clock Pessimism Removal (CPR):    0.000ns
  Clock Uncertainty:      0.281ns  ((TSJ^2 + DJ^2)^1/2) / 2 + PE
    Total System Jitter     (TSJ):    0.071ns
    Discrete Jitter          (DJ):    0.251ns
    Phase Error              (PE):    0.151ns
  Clock Domain Crossing:  Inter clock paths are considered valid unless explicitly excluded by timing constraints such as set_clock_groups or set_false_path.

    Location             Delay type                Incr(ns)  Path(ns)    Netlist Resource(s)
  -------------------------------------------------------------------    -------------------
                         (clock clk_100_mmcm rise edge)
                                                     10.000    10.000 r  
    F22                                               0.000    10.000 r  te0741_clk_100_p (IN)
                         net (fo=0)                   0.000    10.000    te0741_clk_100_p
    F22                  IBUFDS (Prop_ibufds_I_O)     0.850    10.850 r  IBUFGDS_100_osc/O
                         net (fo=1, routed)           1.081    11.931    system_clocks_inst/clk_100_ext
    MMCME2_ADV_X0Y2      MMCME2_ADV (Prop_mmcme2_adv_CLKIN1_CLKOUT0)
                                                      0.077    12.008 r  system_clocks_inst/MMCME2_BASE_100_osc/CLKOUT0
                         net (fo=1, routed)           1.804    13.812    system_clocks_inst/clk_100_mmcm
    BUFGCTRL_X0Y9        BUFG (Prop_bufg_I_O)         0.093    13.905 r  system_clocks_inst/bufg_100/O
                         net (fo=1639, routed)        1.412    15.317    app/PIO/PIO_EP_inst/pci_dma_engine_inst/dma_fifo_1/gnuram_async_fifo.xpm_fifo_base_inst/wr_clk
    SLICE_X105Y137       FDRE                                         r  app/PIO/PIO_EP_inst/pci_dma_engine_inst/dma_fifo_1/gnuram_async_fifo.xpm_fifo_base_inst/gen_pf_ic_rc.gpf_ic.prog_full_i_reg/C
  -------------------------------------------------------------------    -------------------
    SLICE_X105Y137       FDRE (Prop_fdre_C_Q)         0.223    15.540 r  app/PIO/PIO_EP_inst/pci_dma_engine_inst/dma_fifo_1/gnuram_async_fifo.xpm_fifo_base_inst/gen_pf_ic_rc.gpf_ic.prog_full_i_reg/Q
                         net (fo=4, routed)           0.528    16.068    app/PIO/PIO_EP_inst/EP_REGS_inst/dma_status[7]
    SLICE_X105Y138       LUT5 (Prop_lut5_I0_O)        0.043    16.111 f  app/PIO/PIO_EP_inst/EP_REGS_inst/EP_DMA_TX_inst_i_144/O
                         net (fo=1, routed)           0.606    16.717    app/PIO/PIO_EP_inst/EP_REGS_inst/EP_DMA_TX_inst_i_144_n_0
    SLICE_X114Y139       LUT6 (Prop_lut6_I4_O)        0.043    16.760 f  app/PIO/PIO_EP_inst/EP_REGS_inst/EP_DMA_TX_inst_i_34/O
                         net (fo=1, routed)           0.553    17.313    app/PIO/PIO_EP_inst/EP_REGS_inst/EP_DMA_TX_inst_i_34_n_0
    SLICE_X114Y139       LUT5 (Prop_lut5_I3_O)        0.043    17.356 r  app/PIO/PIO_EP_inst/EP_REGS_inst/EP_DMA_TX_inst_i_1/O
                         net (fo=1, routed)           0.519    17.875    app/PIO/PIO_EP_inst/EP_DMA_TX_inst/rd_data[31]
    SLICE_X117Y140       LUT6 (Prop_lut6_I4_O)        0.043    17.918 r  app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata[63]_i_3/O
                         net (fo=1, routed)           0.000    17.918    app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata[63]_i_3_n_0
    SLICE_X117Y140       FDRE                                         r  app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata_reg[63]/D
  -------------------------------------------------------------------    -------------------

                         (clock userclk1 rise edge)
                                                     12.000    12.000 r  
    GTXE2_CHANNEL_X0Y7   GTXE2_CHANNEL                0.000    12.000 r  pcie_7x_gen2_support_i/pcie_7x_gen2_i/inst/inst/gt_top_i/pipe_wrapper_i/pipe_lane[0].gt_wrapper_i/gtx_channel.gtxe2_channel_i/TXOUTCLK
                         net (fo=1, routed)           0.829    12.829    pcie_7x_gen2_support_i/pipe_clock_i/CLK_TXOUTCLK
    BUFGCTRL_X0Y21       BUFG (Prop_bufg_I_O)         0.083    12.912 r  pcie_7x_gen2_support_i/pipe_clock_i/txoutclk_i.txoutclk_i/O
                         net (fo=1, routed)           1.608    14.520    pcie_7x_gen2_support_i/pipe_clock_i/refclk
    MMCME2_ADV_X1Y1      MMCME2_ADV (Prop_mmcme2_adv_CLKIN1_CLKOUT2)
                                                      0.073    14.593 r  pcie_7x_gen2_support_i/pipe_clock_i/mmcm_i/CLKOUT2
                         net (fo=1, routed)           2.005    16.598    pcie_7x_gen2_support_i/pipe_clock_i/userclk1
    BUFGCTRL_X0Y2        BUFG (Prop_bufg_I_O)         0.083    16.681 r  pcie_7x_gen2_support_i/pipe_clock_i/userclk1_i1.usrclk1_i1/O
                         net (fo=1914, routed)        1.283    17.964    app/PIO/PIO_EP_inst/EP_DMA_TX_inst/clk
    SLICE_X117Y140       FDRE                                         r  app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata_reg[63]/C
                         clock pessimism              0.000    17.964    
                         clock uncertainty           -0.281    17.683    
    SLICE_X117Y140       FDRE (Setup_fdre_C_D)        0.034    17.717    app/PIO/PIO_EP_inst/EP_DMA_TX_inst/gen_cpl_64.s_axis_tx_tdata_reg[63]
  -------------------------------------------------------------------
                         required time                         17.717    
                         arrival time                         -17.918    
  -------------------------------------------------------------------
                         slack                                 -0.201 


### XDMA 
 ../tools/acq_from_device  -f data/out_32.bin -s 0x8000 -c 4
character device /dev/xdma0_user opened.
Memory mapped at address 0x7eff839d0000.
 read 0x800000
/dev/xdma0_c2h_0 ** Average BW = 32768, 151.702652 MB/s

 tests git:(master) ✗ ../tools/acq_from_device  -f data/out_32.bin -s 0x8000 -c 1 -e
character device /dev/xdma0_user opened.
Memory mapped at address 0x7fe4e97a0000.
 read 0x800000
/dev/xdma0_c2h_0 ** Average BW = 32768, 241.228516 MB/s
➜  tests git:(master) ✗ ../tools/acq_from_device  -f data/out_32.bin -s 0x8000 -c 4 -e
character device /dev/xdma0_user opened.
Memory mapped at address 0x7f292b573000.
 read 0x800000
/dev/xdma0_c2h_0 ** Average BW = 32768, 258.775269 MB/s
➜  tests git:(master) ✗ ../tools/acq_from_device  -f data/out_32.bin -s 0x8000 -c 20 -e
character device /dev/xdma0_user opened.
Memory mapped at address 0x7f36050c3000.
 read 0x800000
/dev/xdma0_c2h_0 ** Average BW = 32768, 389.260895 MB/s
➜  tests git:(master) ✗ ../tools/acq_from_device  -f data/out_32.bin -s 0x8000 -c 1000 -e
character device /dev/xdma0_user opened.
Memory mapped at address 0x7f23e4186000.
 read 0x800000
/dev/xdma0_c2h_0 ** Average BW = 32768, 570.527100 MB/s

10/07/2012
us1/dsp.one.dsp48e1_add/DSP: DSP48E1 is not using the Multiplier (USE_MULT = NONE). For improved power characteristics, set DREG and ADREG to '1', tie CED, CEAD, and RSTD to logic '0'.
INFO: [DRC AVAL-5] enum_USE_MULT_NONE_enum_DREG_ADREG_0_connects_CED_CEAD_RSTD_GND: adc_data_producer_inst/FpSingleAccum_lat1_inst1/U0/i_synth/ACCUM_OP.OP/i_sign_mag/dsp.one.dsp48e1_add/DSP: DSP48E1 is not using the Multiplier (USE_MULT = NONE). For improved power characteristics, set DREG and ADREG to '1', tie CED, CEAD, and RSTD to logic '0'.
INFO: [DRC REQP-1725] DSP_Abus_sign_bit_alert: adc_data_producer_inst/FpSingleSub_lat1_0/U0/i_synth/ADDSUB_OP.ADDSUB/DSP.OP/DSP48E1_BODY.ALIGN_ADD/DSP2/DSP: When using th

Remove keeps on adc module

20/11/2021

Typical parameters
eo1 = -99
eo2 = -108
eo3 = -234
eo4 = -133
eo5 = -26

wo1 = -0.5452
wo2 = -0.429
wo3 = -0.0782
wo4 = -0.5993
wo5 = -0.4101

B1 = -8.2965e-05
B2 = -8.2820e-05
B3 = -8.2790e-05
B4 = -8.2823e-05
B5 = 7.5544e-05
B6 = 0

threshold_value = 700

control function:
F = B1*int1 + B2*int2 + B3*int3 + B4*int4 + B5*int5


